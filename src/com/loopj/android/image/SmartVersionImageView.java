package com.loopj.android.image;

import android.content.Context;
import android.util.AttributeSet;

import com.loopj.android.image.SmartImageTask;
import com.loopj.android.image.SmartImageView;

/**
 * 自定义SmartImageView，添加了Version的支持
 * @author xiecheng(sparkrico@yahoo.com.cn)
 *
 */
public class SmartVersionImageView extends SmartImageView{

	public SmartVersionImageView(Context context) {
        super(context);
    }

    public SmartVersionImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SmartVersionImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }
    
    public void setVersionImageUrl(String url, int version){
    	setImageUrl(concatVersion(url, version));
    }

    public void setVersionImageUrl(String url, int version, SmartImageTask.OnCompleteListener completeListener) {
    	setImageUrl(concatVersion(url, version), completeListener);
    }

    public void setVersionImageUrl(String url, int version, final Integer fallbackResource) {
    	setImageUrl(concatVersion(url, version), fallbackResource);
    }

    public void setVersionImageUrl(String url, int version, final Integer fallbackResource, SmartImageTask.OnCompleteListener completeListener) {
    	setImageUrl(concatVersion(url, version), fallbackResource, completeListener);
    }

    public void setVersionImageUrl(String url, int version, final Integer fallbackResource, final Integer loadingResource) {
    	setImageUrl(concatVersion(url, version), fallbackResource, loadingResource);
    }

    public void setVersionImageUrl(String url, int version, final Integer fallbackResource, final Integer loadingResource, SmartImageTask.OnCompleteListener completeListener) {
    	setImageUrl(concatVersion(url, version), fallbackResource, loadingResource, completeListener);
    }
    
    /**
     * 组装image version url
     * @param url
     * @param version
     * @return
     */
    private String concatVersion(String url, int version){
    	return url.concat("?v="+version);
    }
}
