package com.asuspro;

import java.util.Timer;
import java.util.TimerTask;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.asuspro.base.BaseActivity;
import com.asuspro.manager.HttpProxy;
import com.asuspro.manager.HttpProxy.AsyncResponseListener;
import com.asuspro.tools.ToastFactory;

/**
 * 重置密码 step2 larry create on 2015-6-11
 */
public class ForgetPasswordStep2 extends BaseActivity {

	private EditText forget_pwd_step2_new;
	private EditText forget_pwd_step2_code;
	private Button forget_pwd_step2_submit;
	private Button forget_pwd_get_code;
	
	private String email;

	@Override
	protected int getContentView() {
		// TODO Auto-generated method stub
		return R.layout.activity_forget_password_step2;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		email = getIntent().getStringExtra("email");
		
		initView();
		startTimer();
	}

	private void initView() {
		forget_pwd_step2_new = (EditText) findViewById(R.id.forget_pwd_step2_new);
		forget_pwd_step2_code = (EditText) findViewById(R.id.forget_pwd_step2_code);
		forget_pwd_step2_submit = (Button) findViewById(R.id.forget_password_step2_submit);
		forget_pwd_get_code = (Button) findViewById(R.id.forget_pwd_get_code);
		
		forget_pwd_get_code.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				forget_pwd_get_code.setText("正在获取");
				forget_pwd_get_code.setEnabled(false);
				HttpProxy.requestReBackPass(email, new AsyncResponseListener() {
					@Override
					public void onSuccess(String data) {
						// TODO Auto-generated method stub
						startTimer();
					}
					
					@Override
					public void onFailure(String error) {
						// TODO Auto-generated method stub
						forget_pwd_get_code.setEnabled(true);
						forget_pwd_get_code.setText("重新获取");
					}
				});
			}
		});
		
		forget_pwd_step2_submit.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String newPwd = forget_pwd_step2_new.getText().toString();
				String codeString = forget_pwd_step2_code.getText().toString();
				if(TextUtils.isEmpty(newPwd)) {
					ToastFactory.showToast(ForgetPasswordStep2.this, "密码不能为空", Toast.LENGTH_SHORT);
					return ;
				}
				if(TextUtils.isEmpty(codeString)) {
					ToastFactory.showToast(ForgetPasswordStep2.this, "验证码不能为空", Toast.LENGTH_SHORT);
					return ;
				}
				
				showWaitDialog();
				HttpProxy.requestResetPassword(0, "", newPwd, new AsyncResponseListener() {
					@Override
					public void onSuccess(String data) {
						// TODO Auto-generated method stub
						hideWaitDialog();
						
					}
					
					@Override
					public void onFailure(String error) {
						// TODO Auto-generated method stub
						hideWaitDialog();
						ToastFactory.showToast(ForgetPasswordStep2.this, error, Toast.LENGTH_SHORT);
					}
				});
			}
		});
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	protected String getHeaderBarTitle() {
		// TODO Auto-generated method stub
		return "重置密码";
	}

	@Override
	protected boolean onClickLeftEvent() {
		// TODO Auto-generated method stub
		finish();
		return super.onClickLeftEvent();
	}

	private int sceod = 60;
	private Timer timer;

	private void startTimer() {

		stopTimer();
		sceod = 60;
		forget_pwd_get_code.setEnabled(false);
		forget_pwd_get_code.setText((sceod--) + "s");
		timer = new Timer();
		timer.schedule(new TimerTask() {

			@Override
			public void run() {
				runOnUiThread(new Runnable() {
					public void run() {
						if (sceod <= 0) {
							mHandler.sendEmptyMessage(1);
							stopTimer();
						} else {
							mHandler.sendEmptyMessage(0);
						}
					}
				});

			}
		}, 1000, 1000);
	}

	private void stopTimer() {
		if (timer != null) {
			timer.cancel();
			timer.purge();
			timer = null;
		}
	}

	private Handler mHandler = new Handler() {

		@Override
		public void handleMessage(Message msg) {
			// TODO Auto-generated method stub
			super.handleMessage(msg);
			switch (msg.what) {
			case 0:// start
				forget_pwd_get_code.setText((sceod--) + "s");
				forget_pwd_get_code.setEnabled(false);
				break;
			case 1:// stop
				forget_pwd_get_code.setText("重新获取");
				forget_pwd_get_code.setEnabled(true);
				break;

			default:
				break;
			}
		}
	};

}
