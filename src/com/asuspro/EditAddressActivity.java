package com.asuspro;

import android.os.Bundle;
import android.text.TextUtils;
import android.widget.EditText;

import com.asuspro.base.BaseActivity;
import com.asuspro.model.AddressInfo;
import com.asuspro.tools.Tools;

/**
 * 编辑地址
 * larry create on 2015-8-7
 */
public class EditAddressActivity extends BaseActivity {
	
	private String title;
	private AddressInfo addressInfo;
	private int type = 0; // 0 新增， 1 修改
	
	private EditText edit_addresslist_name, edit_addresslist_mobile, edit_addresslist_area, edit_addresslist_address, edit_addresslist_zip;

	@Override
	protected int getContentView() {
		// TODO Auto-generated method stub
		return R.layout.activity_edit_address;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		type = getIntent().getIntExtra("type", 0);
		title = getIntent().getStringExtra("title");
		addressInfo = (AddressInfo) getIntent().getSerializableExtra("address");
		
		initView();
		initData();
		
	}
	
	private void initView() {
		edit_addresslist_name = (EditText) findViewById(R.id.edit_addresslist_name);
		edit_addresslist_mobile = (EditText) findViewById(R.id.edit_addresslist_mobile);
		edit_addresslist_area = (EditText) findViewById(R.id.edit_addresslist_area);
		edit_addresslist_address = (EditText) findViewById(R.id.edit_addresslist_address);
		edit_addresslist_zip = (EditText) findViewById(R.id.edit_addresslist_zip);
		
		headerRight.setTextSize(Tools.dipToPx(6));
	}
	
	private void initData() {
		if(addressInfo != null) {
			edit_addresslist_name.setText(addressInfo.getUsername());
			edit_addresslist_mobile.setText(addressInfo.getMobile());
			edit_addresslist_address.setText(addressInfo.getAddress());
			edit_addresslist_zip.setText(addressInfo.getZip());
			edit_addresslist_area.setText(addressInfo.getProvince() + addressInfo.getCity() + addressInfo.getArea());
			
		}
		if(!TextUtils.isEmpty(title)) {
			headerTitle.setText(title);
		}
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	protected String getHeaderBarTitle() {
		// TODO Auto-generated method stub
		return "编辑地址";
	}

	@Override
	protected String getHeaderBarRightIcon() {
		// TODO Auto-generated method stub
		return "保存";
	}

	@Override
	protected boolean onClickLeftEvent() {
		// TODO Auto-generated method stub
		finish();
		return super.onClickLeftEvent();
	}

	@Override
	protected boolean onClickRightEvent() {
		// TODO Auto-generated method stub
		return super.onClickRightEvent();
	}
	
	
	

}
