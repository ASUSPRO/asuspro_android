package com.asuspro;

import android.os.Bundle;
import android.widget.ListView;

import com.asuspro.adapter.ShoppingCartAdapter;
import com.asuspro.base.BaseActivity;

/**
 * 购物车
 * larry create on 2015-6-19
 */
public class ShoppingCartActivity extends BaseActivity {
	
	private ListView mListView;
	private ShoppingCartAdapter mAdapter;
	
	@Override
	protected int getContentView() {
		// TODO Auto-generated method stub
		return R.layout.activity_shopping_cart;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		initView();
		setListener();
		initData();
	}

	private void initView() {
		mListView = (ListView) findViewById(R.id.mListView);
		
	}
	
	private void setListener() {
		
	}
	
	private void initData() {
		mAdapter = new ShoppingCartAdapter(this);
		mListView.setAdapter(mAdapter);
		
	}
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	protected String getHeaderBarTitle() {
		// TODO Auto-generated method stub
		return "购物车";
	}

	@Override
	protected String getHeaderBarRightIcon() {
		// TODO Auto-generated method stub
		return "编辑";
	}

	@Override
	protected boolean onClickLeftEvent() {
		// TODO Auto-generated method stub
		finish();
		return super.onClickLeftEvent();
	}

	@Override
	protected boolean onClickRightEvent() {
		// TODO Auto-generated method stub
		return super.onClickRightEvent();
	}
	
	

}
