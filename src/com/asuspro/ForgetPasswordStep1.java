package com.asuspro;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.asuspro.base.BaseActivity;
import com.asuspro.manager.HttpProxy;
import com.asuspro.manager.HttpProxy.AsyncResponseListener;
import com.asuspro.tools.StringUtils;
import com.asuspro.tools.ToastFactory;

/**
 * 忘记密码 step1 larry create on 2015-6-10
 */
public class ForgetPasswordStep1 extends BaseActivity implements AsyncResponseListener{
	
	private EditText forget_pwd_step1_edit;
	private Button forget_password_step1_submit;

	@Override
	protected int getContentView() {
		// TODO Auto-generated method stub
		return R.layout.activity_forget_password_step1;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		initView();
	}
	
	private void initView() {
		forget_pwd_step1_edit = (EditText) findViewById(R.id.forget_pwd_step1_edit);
		forget_password_step1_submit = (Button) findViewById(R.id.forget_password_step1_submit);
		
		forget_password_step1_submit.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String email = forget_pwd_step1_edit.getText().toString();
				if(TextUtils.isEmpty(email)) {
					ToastFactory.showToast(ForgetPasswordStep1.this, "邮箱不能为空", Toast.LENGTH_SHORT);
					return;
				}
				if(!StringUtils.isEmail(email)) {
					ToastFactory.showToast(ForgetPasswordStep1.this, "邮箱格式不正确", Toast.LENGTH_SHORT);
					return;
				}
				showWaitDialog();
				HttpProxy.requestReBackPass(email, ForgetPasswordStep1.this);
			}
		});
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	protected String getHeaderBarRightIcon() {
		// TODO Auto-generated method stub
		return "";
	}

	@Override
	protected String getHeaderBarTitle() {
		// TODO Auto-generated method stub
		return "忘记密码";
	}

	@Override
	protected boolean onClickLeftEvent() {
		// TODO Auto-generated method stub
		finish();
		return super.onClickLeftEvent();
	}

	@Override
	public void onSuccess(String data) {
		// TODO Auto-generated method stub
		hideWaitDialog();
		Intent i = new Intent();
		i.setClass(this, ForgetPasswordStep2.class);
		i.putExtra("email", forget_pwd_step1_edit.getText().toString());
		startActivity(i);
	}

	@Override
	public void onFailure(String error) {
		// TODO Auto-generated method stub
		hideWaitDialog();
		ToastFactory.showToast(ForgetPasswordStep1.this, error, Toast.LENGTH_SHORT);
	}

}
