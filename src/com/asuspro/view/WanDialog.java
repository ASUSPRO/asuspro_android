package com.asuspro.view;

import android.app.Dialog;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.text.TextUtils;
import android.text.method.ScrollingMovementMethod;
import android.text.util.Linkify;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.asuspro.R;

public class WanDialog extends Dialog implements View.OnClickListener{

	private RelativeLayout mTitleRoot;
    protected TextView mTitle;
    private   TextView mTitleRight;
    private   TextView mTitleRightUnit;
    
    protected LinearLayout mContent;
    protected Button mBtnOk;
    protected Button mBtnNo;
    
    protected Context context;
    protected int mWidth;
	private View mUpline;
	private View mDownline;
	private View mMiddleline;
    

    public WanDialog(Context context) {
        this(context, null, null);
    }
    
    public WanDialog(Context context,CharSequence title) {
        this(context, title, null);
    }
    private boolean hasTitle = false;
    public WanDialog(Context context,CharSequence title,CharSequence msg) {
        super(context, R.style.gc_kala_dialog);
        this.context = context;
        super.setContentView(R.layout.kalagame_wan_dialog);        
        mWidth = getDensity(context) *3/4;
        setWindowParams(mWidth,LayoutParams.WRAP_CONTENT);
        initView();
        if(!TextUtils.isEmpty(title)){
            setTitle(title);
        }else{
        	goneTileView();
        }
        
        if(!TextUtils.isEmpty(msg)){
            setMsg(msg);
        }
        //setCanceledOnTouchOutside(true);
    }
    
    public void setView(View view){
    	super.setContentView(view); 
    }
    
    private void goneTileView(){
    	hasTitle = false;
    	mTitle.setVisibility(View.GONE);
    	mUpline.setVisibility(View.GONE);
    	mTitleRoot.setVisibility(View.GONE);
    	mContent.setBackgroundResource(R.drawable.kalagame_wan_dialog_title_bg_shape);
    	
    }
    private void visibleTileView(){
    	hasTitle = true;
    	mTitle.setVisibility(View.VISIBLE);
    	mUpline.setVisibility(View.VISIBLE);
    	mTitleRoot.setVisibility(View.VISIBLE);
    	mContent.setBackgroundResource(R.drawable.kalagame_wan_dialog_title_bottom_bg_shape);
    	
    }
    public void setSystemType(){
    	getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
    }
    
    protected void setWindowParams(int width,int height){
        Window window = getWindow();
        WindowManager.LayoutParams params = window.getAttributes();
        params.width = width;
        params.height = height;
        params.gravity = Gravity.CENTER;
        window.setAttributes(params);
    }
    private void setMsg(CharSequence msg){
        if(!TextUtils.isEmpty(msg)){
            TextView tv = new TextView(context);
            tv.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
//            tv.setAutoLinkMask(Linkify.PHONE_NUMBERS);
            tv.setTextColor(0xff666666);
            tv.setBackgroundColor(Color.TRANSPARENT);
            tv.setId(1000);
            tv.setMaxHeight(mWidth*3/4);
            int left = context.getResources().getDimensionPixelSize(R.dimen.wan_dialog_content_padding);
            tv.setPadding(left, left, left, left);
            tv.setMovementMethod(ScrollingMovementMethod.getInstance());
            tv.setText(msg);
            if(mContent != null){
                mContent.removeAllViews();
                mContent.setGravity(Gravity.CENTER);
                mContent.addView(tv,new android.widget.LinearLayout.LayoutParams
                        (android.widget.LinearLayout.LayoutParams.WRAP_CONTENT,
                                android.widget.LinearLayout.LayoutParams.WRAP_CONTENT));
            }
        }
    }
    
    public void setContentMsg(CharSequence msg){
    	if(mContent != null){
    		TextView tv = (TextView) mContent.findViewById(1000);
    		if(tv != null && !TextUtils.isEmpty(msg)){
    			tv.setMovementMethod(ScrollingMovementMethod.getInstance());
    			tv.setTextColor(0x000000);
    			tv.setText(msg);
    		}else if(tv == null){
    			setMsg(msg);
    		}
    	}
    }
    
    public void setContentMsg(CharSequence msg, boolean showLink){
    	if(mContent != null){
    		TextView tv = (TextView) mContent.findViewById(1000);
    		if(tv != null && !TextUtils.isEmpty(msg)){
    			tv.setMovementMethod(ScrollingMovementMethod.getInstance());
    			tv.setText(msg);
    			if (showLink) {
    				tv.setAutoLinkMask(Linkify.PHONE_NUMBERS);
    			}
    					
    		}else if(tv == null){
    			setMsg(msg, showLink);
    		}
    	}
    }
    
    private void setMsg(CharSequence msg, boolean showLink){
        if(!TextUtils.isEmpty(msg)){
            TextView tv = new TextView(context);
            tv.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
            if (showLink) {
            	tv.setAutoLinkMask(Linkify.PHONE_NUMBERS);
            }
//            tv.setAutoLinkMask(Linkify.PHONE_NUMBERS);
            tv.setTextColor(0xff666666);
            tv.setId(1000);
            tv.setMaxHeight(mWidth*3/4);
            tv.setBackgroundColor(Color.TRANSPARENT);
            int left = context.getResources().getDimensionPixelSize(R.dimen.wan_dialog_content_padding);
            tv.setPadding(left, left*2, left, left*2);
            tv.setMovementMethod(ScrollingMovementMethod.getInstance());
            tv.setText(msg);
            if(mContent != null){
                mContent.removeAllViews();
                mContent.setGravity(Gravity.CENTER);
                mContent.addView(tv,new android.widget.LinearLayout.LayoutParams
                        (android.widget.LinearLayout.LayoutParams.WRAP_CONTENT,
                                android.widget.LinearLayout.LayoutParams.WRAP_CONTENT));
            }
        }
    }
    
    public void setContentGravity(int gravity){
        if(mContent != null && mContent.getChildCount() > 0){
            View view = mContent.getChildAt(0);
            if(view instanceof TextView){
                ((TextView)view).setGravity(gravity);
            }
        }
    }
    
    @Override
    public void setContentView(int layoutResID) {
        View view = LayoutInflater.from(context).inflate(layoutResID, null);
        setContentView(view);
    }
   
    @Override
    public void setContentView(View view) {
        setContentView(view,null);
    }

    @Override
    public void setContentView(View view, LayoutParams params) {
        if(mContent != null && view != null){
        	if(!hasBtnNo && !hasBtnOk){
        		mContent.setBackgroundResource(R.drawable.kalagame_wan_dialog_btn_bg_shape);
        	}
            if(params == null){
                mContent.addView(view);
            }else{
                mContent.addView(view,params);
            }
        }
    }

    private void initView() {
    	mTitleRoot = (RelativeLayout) findViewById(R.id.kalagame_id_wan_dialog_title_root);
        mTitle = (TextView)this.findViewById(R.id.kalagame_id_wan_dialog_title);
        mTitleRight = (TextView)this.findViewById(R.id.kalagame_id_wan_dialog_title_right);
        mTitleRightUnit = (TextView)this.findViewById(R.id.kalagame_id_wan_dialog_title_right_unit);
        mContent = (LinearLayout)this.findViewById(R.id.kalagame_wan_dialog_content);
        mContent.setBackgroundResource(R.drawable.kalagame_wan_dialog_btn_bg_shape);
        mBtnOk   = (Button) this.findViewById(R.id.kalagame_id_wan_dialog_ok);
        mBtnNo  = (Button) this.findViewById(R.id.kalagame_id_wan_dialog_no);
        mUpline  = this.findViewById(R.id.kalagame_id_v_up_line);
        mDownline  = this.findViewById(R.id.kalagame_id_v_bottom_line);
        mMiddleline  = this.findViewById(R.id.kalagame_id_v_middle_line);
    }

    private boolean hasBtnOk = false;
    private boolean hasBtnNo = false;
    public void setBtnOk(String text,final OnClickListener listener){
        
        if(mBtnOk != null){
        	hasBtnOk = true;
        	if(hasBtnNo){
        		mMiddleline.setVisibility(View.VISIBLE);
        	}
        	mDownline.setVisibility(View.VISIBLE);
        	if(hasTitle){
        		mContent.setBackgroundColor(Color.WHITE);
        	}else{
        		mContent.setBackgroundResource(R.drawable.kalagame_wan_dialog_title_bg_shape);
        	}
        	
            mBtnOk.setVisibility(View.VISIBLE);
            mBtnOk.requestFocus();
            ((View)mBtnOk.getParent()).setVisibility(View.VISIBLE);
            mBtnOk.setText(text);
            mBtnOk.setOnClickListener(new View.OnClickListener() {
                
                @Override
                public void onClick(View v) {
                    if(listener != null){
                        listener.onClick(WanDialog.this, 0);
                    }else{
                        WanDialog.this.cancel();
                    }
                    //WanDialog.this.cancel();
                }
            });
        }
    }
    
    public void setBtnNo(String text,final OnClickListener listener){
        if(mBtnNo != null){
        	hasBtnNo = true;
        	if(hasBtnOk){
        		mMiddleline.setVisibility(View.VISIBLE);
        	}
        	mDownline.setVisibility(View.VISIBLE);
        	if(hasTitle){
        		mContent.setBackgroundColor(Color.WHITE);
        	}else{
        		mContent.setBackgroundResource(R.drawable.kalagame_wan_dialog_title_bg_shape);
        	}
            mBtnNo.setVisibility(View.VISIBLE);
            mBtnNo.requestFocus();
            ((View)mBtnOk.getParent()).setVisibility(View.VISIBLE);
            mBtnNo.setText(text);
            mBtnNo.setOnClickListener(new View.OnClickListener() {
                
                @Override
                public void onClick(View v) {
                    if(listener != null){
                        listener.onClick(WanDialog.this, 1);
                    }else{
                        WanDialog.this.cancel();
                    }
                    
                    WanDialog.this.cancel();
                }
            });
        }
    }
   
    
    @Override
    public void setTitle(CharSequence title) {
        if(!TextUtils.isEmpty(title) && mTitle != null){
        	visibleTileView();
            mTitle.setText(title);
        }
    }

    @Override
    public void setTitle(int titleId) {
        if(titleId != 0  && mTitle != null){
        	visibleTileView();
            mTitle.setText(titleId);
        }
    }
    
    
    public void setTitleRight(int titleId) {
        if(titleId != 0  && mTitleRight != null){
//        	visibleTileView();
        	mTitleRight.setVisibility(View.VISIBLE);
        	mTitleRightUnit.setVisibility(View.VISIBLE);
        	mTitleRight.setText(titleId);
        }
    }
    
    public void setTitleRight(CharSequence title) {
        if(!TextUtils.isEmpty(title) && mTitleRight != null){
//        	visibleTileView();
        	mTitleRight.setVisibility(View.VISIBLE);
        	mTitleRightUnit.setVisibility(View.VISIBLE);
        	mTitleRight.setText(title);
        }
    }

    private int getDensity(Context context) {
        Resources resources = context.getResources();
        DisplayMetrics dm = resources.getDisplayMetrics();
        int width = dm.widthPixels;
        return width;
    }
    
    
    private OnClickListener onClickListener;
    public void setItemContent(String[] items , OnClickListener onClickListener){
    	mContent.setBackgroundResource(R.drawable.kalagame_wan_dialog_btn_bg_shape);
    	setContentView(getItemView(items));
    	this.onClickListener = onClickListener;
    }
    
    
    private View getItemView(String[] items){
    	LinearLayout itemViews = new LinearLayout(getContext());
    	//itemViews.setBackgroundColor(Color.WHITE);
    	itemViews.setOrientation(LinearLayout.VERTICAL);
    	float density = getContext().getResources().getDisplayMetrics().density;
    	/*int padding = (int) (10* density);
    	itemViews.setPadding(padding, padding, padding, padding);*/
    	itemViews.setDescendantFocusability(LinearLayout.FOCUS_BLOCK_DESCENDANTS);
    	itemViews.setFocusableInTouchMode(true);
    	//itemViews.setBackgroundResource(R.drawable.kalagame_wan_dialog_btn_bg_shape);
    	//ColorStateList csl=(ColorStateList)getContext().getResources().getColorStateList(R.color.kalagame_text_black2white);  
    	LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
    	LinearLayout.LayoutParams lp1 = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, 1);
    	lp.gravity = Gravity.CENTER;
    	if(items != null && items.length > 0){
    		for(int i = 0 ; i < items.length ; i ++){
    			TextView tv = new TextView(getContext());
    			tv.setId(i);
    			tv.setAutoLinkMask(Linkify.PHONE_NUMBERS);
    			tv.setBackgroundResource(R.drawable.kalagame_actionbar_item_bg_status);
    			tv.setOnClickListener(this);
    			tv.setText(items[i]);
    			tv.setLayoutParams(lp);
    			tv.setGravity(Gravity.CENTER_VERTICAL);
    			tv.setPadding((int) (20 *density), 0, 0, 0);
    			tv.setMinHeight(getContext().getResources().getDimensionPixelSize(R.dimen.wan_dialog_item_height));
    			tv.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 18);
    			tv.setTextColor(0xff333333);
    			itemViews.addView(tv);
    			if(i != items.length - 1){
    				View view = new View(getContext());
        			view.setBackgroundColor(0xffe5e5e5);
        			itemViews.addView(view, lp1);
    			}
    		}
    	}
    	return itemViews;
    }

	@Override
	public void onClick(View v) {
		if(onClickListener != null){
			onClickListener.onClick(this, v.getId());
		}
	}
    
    
    
}
