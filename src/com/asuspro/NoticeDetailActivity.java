package com.asuspro;

import android.os.Bundle;
import android.text.Html;
import android.util.Base64;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.asuspro.base.BaseActivity;
import com.asuspro.manager.HttpProxy;
import com.asuspro.manager.HttpProxy.AsyncResponseListener;
import com.asuspro.manager.JsonParser;
import com.asuspro.model.BaseResult;
import com.asuspro.model.NoticeInfo;
import com.asuspro.tools.ToastFactory;
import com.asuspro.tools.Tools;

/**
 * 政策通知详情
 * larry create on 2015-7-20
 */
public class NoticeDetailActivity extends BaseActivity {
	private int noticeId;
	private int cid = 108;
	private NoticeInfo info = new NoticeInfo();;

	private TextView title;
	private TextView desc;
	private TextView time;
	private TextView content;
	
	@Override
	protected int getContentView() {
		// TODO Auto-generated method stub
		return R.layout.activity_notice_detail;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		noticeId = getIntent().getIntExtra("id", 0);
		cid = getIntent().getIntExtra("cid", 108);
		info.setId(noticeId);
		
		initView();
		
		getArticleDetailDataFromNet();
	}
	
	private void initView() {
		title = (TextView) findViewById(R.id.detail_notice_title);
		desc = (TextView) findViewById(R.id.detail_notice_desc);
		time = (TextView) findViewById(R.id.detail_notice_time);
		content = (TextView) findViewById(R.id.notice_detail_webview);
		
		if(cid == 109) {
			headerTitle.setText("技术分享");
		} else {
			headerTitle.setText("政策通知");
		}
	}
	
	private void initArticleData() {
		time.setText(Tools.convertTimeStamp(info.getCreatetime(), "yyyy-MM-dd"));
		title.setText(info.getMaintitle());
		desc.setText(info.getSource());
//		content.loadData(info.getContent(), "text/html; charset=UTF-8", "UTF-8");
		byte[] b = Base64.decode(info.getContent().getBytes(), Base64.DEFAULT);
		content.setText(Html.fromHtml(new String(b)));
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	protected String getHeaderBarRightIcon() {
		// TODO Auto-generated method stub
		return "";
	}
	
	@Override
	protected String getHeaderBarTitle() {
		// TODO Auto-generated method stub
		return "政策通知";
	}

	@Override
	protected boolean onClickLeftEvent() {
		// TODO Auto-generated method stub
		finish();
		return super.onClickLeftEvent();
	}
	
	// 获取通知详情
	private void getArticleDetailDataFromNet() {
		showWaitDialog();
		HttpProxy.requestGetArticleContent(cid, noticeId, new AsyncResponseListener() {
			
			@Override
			public void onSuccess(String data) {
				// TODO Auto-generated method stub
				hideWaitDialog();
				BaseResult result = JsonParser.parserNoticeDetail(data);
				if(result.getResultCode() == 0) {
					try {
						info = (NoticeInfo) result.getResultContent();
						initArticleData();
					} catch (Exception e) {
						e.printStackTrace();
					}
				} else {
					ToastFactory.showToast(NoticeDetailActivity.this, result.getResultMsg(), Toast.LENGTH_SHORT);
				}
				Log.d("larry", "--requestGetArticleContent--" + data);
			}
			
			@Override
			public void onFailure(String error) {
				hideWaitDialog();
				ToastFactory.showToast(NoticeDetailActivity.this, error, Toast.LENGTH_SHORT);
			}
		});
	}
}
