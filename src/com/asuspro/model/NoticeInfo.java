package com.asuspro.model;

/**
 * 政策通知
 * larry create on 2015-7-14
 */
public class NoticeInfo extends BaseResult {

	private static final long serialVersionUID = 335905871278516189L;

	private int id; // 文章的id

	private String maintitle; // 文章标题

	private String source; // 来源

	private long createtime; // 创建时间

	private String content; // 	文章内容（html格式代码）

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getMaintitle() {
		return maintitle;
	}

	public void setMaintitle(String maintitle) {
		this.maintitle = maintitle;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public long getCreatetime() {
		return createtime;
	}

	public void setCreatetime(long createtime) {
		this.createtime = createtime;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

}
