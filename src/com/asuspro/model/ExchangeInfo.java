package com.asuspro.model;
/**
 * 兑换物品
 * larry create on 2015-7-6
 */
public class ExchangeInfo extends BaseResult {

	private static final long serialVersionUID = 1L;
		
	private long id;
	
	private String order_id; // 订单号
	
	private long gift_id; // 礼品id
	
	private String gift_name; // 礼品名
	
	private String gift_pic; // 礼品图片
	
	private long score; // 兑换单个礼品使用积分
	
	private long num; // 兑换数量
	
	private long create_time; // 兑换时间（单位：秒），需要转化
	
	private String express; // 快递公司
	
	private String express_no; // 快递订单号

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getOrder_id() {
		return order_id;
	}

	public void setOrder_id(String order_id) {
		this.order_id = order_id;
	}

	public long getGift_id() {
		return gift_id;
	}

	public void setGift_id(long gift_id) {
		this.gift_id = gift_id;
	}

	public String getGift_name() {
		return gift_name;
	}

	public void setGift_name(String gift_name) {
		this.gift_name = gift_name;
	}

	public String getGift_pic() {
		return gift_pic;
	}

	public void setGift_pic(String gift_pic) {
		this.gift_pic = gift_pic;
	}

	public long getScore() {
		return score;
	}

	public void setScore(long score) {
		this.score = score;
	}

	public long getNum() {
		return num;
	}

	public void setNum(long num) {
		this.num = num;
	}

	public long getCreate_time() {
		return create_time;
	}

	public void setCreate_time(long create_time) {
		this.create_time = create_time;
	}

	public String getExpress() {
		return express;
	}

	public void setExpress(String express) {
		this.express = express;
	}

	public String getExpress_no() {
		return express_no;
	}

	public void setExpress_no(String express_no) {
		this.express_no = express_no;
	}
	
	
}
