package com.asuspro.model;

import java.io.Serializable;

/**
 * 解析基类 larry create on 2015-6-11
 */
public class BaseResult implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private int ResultCode;
	private String ResultMsg;
	private Object ResultContent;

	public int getResultCode() {
		return ResultCode;
	}

	public void setResultCode(int resultCode) {
		ResultCode = resultCode;
	}

	public String getResultMsg() {
		return ResultMsg;
	}

	public void setResultMsg(String resultMsg) {
		ResultMsg = resultMsg;
	}

	public Object getResultContent() {
		return ResultContent;
	}

	public void setResultContent(Object resultContent) {
		ResultContent = resultContent;
	}

}
