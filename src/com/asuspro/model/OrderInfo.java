package com.asuspro.model;
/**
 * 订单信息
 * larry create on 2015-6-8
 */
public class OrderInfo extends BaseResult{

	private static final long serialVersionUID = 1412447618322866197L;

	private int order_id;	// 订单号
	private int use_score;	// 兑换使用总积分
	private int user_id;	// 兑换用户id
	private int receive_id;	// 对应收货表中的id，可以不关注此字段
	private float delivery_fee;	// 邮费，单位分
	private String create_time;	// 订单生成时间（单位：秒），需要转化
	private String send_time;	// 发货时间（单位：秒），需要转化
	private int status;			// 订单状态
	private String express;		// 快递公司
	private String express_no;	// 快递单号
	public int getOrder_id() {
		return order_id;
	}
	public void setOrder_id(int order_id) {
		this.order_id = order_id;
	}
	public int getUse_score() {
		return use_score;
	}
	public void setUse_score(int use_score) {
		this.use_score = use_score;
	}
	public int getUser_id() {
		return user_id;
	}
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}
	public int getReceive_id() {
		return receive_id;
	}
	public void setReceive_id(int receive_id) {
		this.receive_id = receive_id;
	}
	public float getDelivery_fee() {
		return delivery_fee;
	}
	public void setDelivery_fee(float delivery_fee) {
		this.delivery_fee = delivery_fee;
	}
	public String getCreate_time() {
		return create_time;
	}
	public void setCreate_time(String create_time) {
		this.create_time = create_time;
	}
	public String getSend_time() {
		return send_time;
	}
	public void setSend_time(String send_time) {
		this.send_time = send_time;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getExpress() {
		return express;
	}
	public void setExpress(String express) {
		this.express = express;
	}
	public String getExpress_no() {
		return express_no;
	}
	public void setExpress_no(String express_no) {
		this.express_no = express_no;
	}
	
	
}
