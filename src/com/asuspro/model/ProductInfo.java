package com.asuspro.model;

/**
 * 商品 larry create on 2015-6-8
 */
public class ProductInfo extends BaseResult {

	private static final long serialVersionUID = 2553521573007940038L;

	private int product_id;
	private long cat_id;//所属分类的id
	private String part_number;//料号
	private String type;//型号
	private String stock_position;//库位
	private int num;//库存数量
	private long price;

	public int getProduct_id() {
		return product_id;
	}

	public void setProduct_id(int product_id) {
		this.product_id = product_id;
	}

	public int getNum() {
		return num;
	}

	public void setNum(int num) {
		this.num = num;
	}

	public long getCat_id() {
		return cat_id;
	}

	public void setCat_id(long cat_id) {
		this.cat_id = cat_id;
	}

	public String getPart_number() {
		return part_number;
	}

	public void setPart_number(String part_number) {
		this.part_number = part_number;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getStock_position() {
		return stock_position;
	}

	public void setStock_position(String stock_position) {
		this.stock_position = stock_position;
	}

	public long getPrice() {
		return price;
	}

	public void setPrice(long price) {
		this.price = price;
	}

}
