package com.asuspro.model;

import android.os.Parcel;
import android.os.Parcelable;

public class ImageInfo implements Parcelable {
	private String originalURL;
	private String minURL;
	private String middleURL;
	private String maxURL;
	private String localURL;

	public String getOriginalURL() {
		return originalURL;
	}

	public void setOriginalURL(String originalURL) {
		this.originalURL = originalURL;
	}

	public String getMinURL() {
		return minURL;
	}

	public void setMinURL(String minURL) {
		this.minURL = minURL;
	}

	public String getMiddleURL() {
		return middleURL;
	}

	public void setMiddleURL(String middleURL) {
		this.middleURL = middleURL;
	}

	public String getMaxURL() {
		return maxURL;
	}

	public void setMaxURL(String maxURL) {
		this.maxURL = maxURL;
	}

	public String getLocalURL() {
		return localURL;
	}

	public void setLocalURL(String localURL) {
		this.localURL = localURL;
	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel parcel, int flags) {
		// TODO Auto-generated method stub
		parcel.writeString(originalURL);
		parcel.writeString(maxURL);
		parcel.writeString(middleURL);
		parcel.writeString(minURL);
		parcel.writeString(localURL);
	}

	public static final Parcelable.Creator<ImageInfo> CREATOR = new Parcelable.Creator<ImageInfo>() {

		@Override
		public ImageInfo createFromParcel(Parcel source) {
			// TODO Auto-generated method stub
			ImageInfo info = new ImageInfo();
			info.originalURL = source.readString();
			info.maxURL = source.readString();
			info.middleURL = source.readString();
			info.minURL = source.readString();
			info.localURL = source.readString();
			return info;
		}

		@Override
		public ImageInfo[] newArray(int size) {
			// TODO Auto-generated method stub
			return new ImageInfo[size];
		}
		
	};
}
