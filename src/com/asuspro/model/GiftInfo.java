package com.asuspro.model;

/**
 * 礼品信息
 * larry create on 2015-6-8
 */
public class GiftInfo extends BaseResult{
	private static final long serialVersionUID = -3071064254662685893L;

	private int id; // 礼品id
	private String gift_name; // 礼品名称
	private int cat_id; // 礼品所属分类id
	private int inner_score; // 内部人员兑换所需积分
	private int outer_score; // 外部经销商兑换所需积分
	private int num; // 当前库存
	private String gift_pic; // 礼品图片
	private String gift_desc; // 礼品描述
	private String gift_note; // 详细规格
	private int recommend; // 推荐指数，数值越大，排序越往前
	private String recommend_pic; // 推荐图片
	private int show_flag; // 是否显示1-显示，0-不显示
	private String create_time; // 创建时间
	private int is_asus; // 是否是华硕产品0-否，1-是

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getGift_name() {
		return gift_name;
	}

	public void setGift_name(String gift_name) {
		this.gift_name = gift_name;
	}

	public int getCat_id() {
		return cat_id;
	}

	public void setCat_id(int cat_id) {
		this.cat_id = cat_id;
	}

	public int getInner_score() {
		return inner_score;
	}

	public void setInner_score(int inner_score) {
		this.inner_score = inner_score;
	}

	public int getOuter_score() {
		return outer_score;
	}

	public void setOuter_score(int outer_score) {
		this.outer_score = outer_score;
	}

	public int getNum() {
		return num;
	}

	public void setNum(int num) {
		this.num = num;
	}

	public String getGift_pic() {
		return gift_pic;
	}

	public void setGift_pic(String gift_pic) {
		this.gift_pic = gift_pic;
	}

	public String getGift_desc() {
		return gift_desc;
	}

	public void setGift_desc(String gift_desc) {
		this.gift_desc = gift_desc;
	}

	public String getGift_note() {
		return gift_note;
	}

	public void setGift_note(String gift_note) {
		this.gift_note = gift_note;
	}

	public int getRecommend() {
		return recommend;
	}

	public void setRecommend(int recommend) {
		this.recommend = recommend;
	}

	public String getRecommend_pic() {
		return recommend_pic;
	}

	public void setRecommend_pic(String recommend_pic) {
		this.recommend_pic = recommend_pic;
	}

	public int getShow_flag() {
		return show_flag;
	}

	public void setShow_flag(int show_pic) {
		this.show_flag = show_pic;
	}

	public String getCreate_time() {
		return create_time;
	}

	public void setCreate_time(String create_time) {
		this.create_time = create_time;
	}

	public int getIs_asus() {
		return is_asus;
	}

	public void setIs_asus(int is_asus) {
		this.is_asus = is_asus;
	}

}
