package com.asuspro.model;


/**
 * 广告 larry create on 2015-2-14
 */
public class AdvertisementInfo {
	private String link;
	private ImageInfo image;

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public ImageInfo getImage() {
		return image;
	}

	public void setImage(ImageInfo image) {
		this.image = image;
	}

}
