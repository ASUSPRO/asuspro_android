package com.asuspro.model;

import java.io.Serializable;

/**
 * 收货地址 larry create on 2015-8-6
 */
public class AddressInfo implements Serializable {

	private static final long serialVersionUID = -6365118911143406245L;

	private int id;
	private int user_id; // 用户ID
	private String username; // 用户姓名
	private String province; // 省份
	private String city; // 城市
	private String area; // 地区
	private String address; // 收货地址
	private String zip; // 邮编
	private String mobile; // 收货人电话
	private int is_default; // 1为默认收货地址，0为非默认

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getUser_id() {
		return user_id;
	}

	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public int getIs_default() {
		return is_default;
	}

	public void setIs_default(int is_default) {
		this.is_default = is_default;
	}

}
