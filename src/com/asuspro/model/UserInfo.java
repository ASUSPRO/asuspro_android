package com.asuspro.model;

/**
 * 用户信息 larry create on 2015-6-11
 */
public class UserInfo extends BaseResult{

	private static final long serialVersionUID = -8972795277913112989L;

	private int id; 			// 用户的id
	private String username;	// 用户帐号
	private String password;	// 登录密码，md5加密
	private String name;		// 用户姓名
	private int gender;			// 0-男，1-女
	private String birthday;	// 出生日期
	private String address;		// 地址
	private String mobile;		// 手机号
	private String email;		// 电子邮件
	private long score;			// 用户积分
	private int dealer_id;		// 所属经销商id，此字段不用关注
	private int user_type;		// 1-内部人员，2-经销商
	private String pic_url;		// 用户头像
	private String token;		// token
	private String company;		// 公司名称
	private String phone;		// 座机
	
	private int status;			// 登录态
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getGender() {
		return gender;
	}
	public void setGender(int gender) {
		this.gender = gender;
	}
	public String getBirthday() {
		return birthday;
	}
	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public long getScore() {
		return score;
	}
	public void setScore(long score) {
		this.score = score;
	}
	public int getDealer_id() {
		return dealer_id;
	}
	public void setDealer_id(int dealer_id) {
		this.dealer_id = dealer_id;
	}
	public int getUser_type() {
		return user_type;
	}
	public void setUser_type(int user_type) {
		this.user_type = user_type;
	}
	public String getPic_url() {
		return pic_url;
	}
	public void setPic_url(String pic_url) {
		this.pic_url = pic_url;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	
}
