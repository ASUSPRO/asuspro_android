package com.asuspro.model;

/**
 * 列表数据模型 larry create on 2015-5-9
 */
public class ListItemBean {

	private String title;

	private String desc;

	private String time;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

}
