package com.asuspro.model;

/**
 * 分类 larry create on 2015-7-7
 */
public class CategoryBean extends BaseResult {

	private static final long serialVersionUID = -7753070165938727729L;

	private long column_id; // 分类ID

	private String column_name; // 分类名称
	
	private String parent_id; // 父级分类的ID

	public long getColumn_id() {
		return column_id;
	}

	public void setColumn_id(long column_id) {
		this.column_id = column_id;
	}

	public String getColumn_name() {
		return column_name;
	}

	public void setColumn_name(String column_name) {
		this.column_name = column_name;
	}

	public String getParent_id() {
		return parent_id;
	}

	public void setParent_id(String parent_id) {
		this.parent_id = parent_id;
	}

	
}
