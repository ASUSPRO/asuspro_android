package com.asuspro;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Selection;
import android.text.Spannable;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.asuspro.base.BaseActivity;
import com.asuspro.tools.Tools;

/**
 * 单字段编辑
 * larry create on 2015-8-5
 */
public class UserFieldEditActivity extends BaseActivity {
	
	private EditText editText;
	
	private String editContent;
	private String tag;

	@Override
	protected int getContentView() {
		// TODO Auto-generated method stub
		return R.layout.activity_user_field_edit;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		tag = getIntent().getStringExtra("tag");
		
		editContent = getIntent().getStringExtra("content");
		
		editText = (EditText) findViewById(R.id.user_field_edit);
		
		editText.setText(editContent);
		
		editText.setSelection(editContent.length());	// 光标设定在末尾
		
		InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.showSoftInput(editText, InputMethodManager.SHOW_FORCED);
		
		headerRight.setTextSize(Tools.dipToPx(6));
		
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	protected String getHeaderBarTitle() {
		// TODO Auto-generated method stub
		return "编辑";
	}

	@Override
	protected String getHeaderBarRightIcon() {
		// TODO Auto-generated method stub
		return "保存";
	}

	@Override
	protected boolean onClickLeftEvent() {
		// TODO Auto-generated method stub
		finish();
		return super.onClickLeftEvent();
	}

	@Override
	protected boolean onClickRightEvent() {
		// TODO Auto-generated method stub
		Intent data = new Intent();
		data.putExtra("tag", tag);
		data.putExtra("content", editText.getText().toString());
		setResult(RESULT_OK, data);
		finish();
		return super.onClickRightEvent();
	}
	
	

}
