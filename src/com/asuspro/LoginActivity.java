package com.asuspro;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.asuspro.base.App;
import com.asuspro.base.BaseActivity;
import com.asuspro.config.AppConstants;
import com.asuspro.dbhelper.DBManager;
import com.asuspro.manager.HttpProxy;
import com.asuspro.manager.HttpProxy.AsyncResponseListener;
import com.asuspro.manager.JsonParser;
import com.asuspro.model.BaseResult;
import com.asuspro.model.UserInfo;
import com.asuspro.tools.ToastFactory;

/**
 * 登录页 larry create on 2015-5-5
 */
public class LoginActivity extends BaseActivity implements AsyncResponseListener{
	private Button login_button;
	private TextView login_forget_password;
	
	private TextView username_tag, pwd_tag;
	
	private TextView clear_username, clear_password;
	
	private EditText username, pwd;
	
	private UserInfo mCurrentUser;
	

	@Override
	protected int getContentView() {
		// TODO Auto-generated method stub
		return R.layout.activity_login;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		initView();
		setListener();
		mCurrentUser = DBManager.getUserDBHandler().getLoginUser();
		if(mCurrentUser != null && mCurrentUser.getStatus() == 1) {
			Intent intent = new Intent();
			intent.setClass(LoginActivity.this, MainActivity.class);
			startActivityForResult(intent, 0);
		}
	}

	private void initView() {
		login_button = (Button) findViewById(R.id.login_button);
		login_forget_password = (TextView) findViewById(R.id.login_forget_password);
		username_tag = (TextView) findViewById(R.id.username_tag);
		pwd_tag = (TextView) findViewById(R.id.pwd_tag);
		clear_username = (TextView) findViewById(R.id.clear_username);
		clear_password = (TextView) findViewById(R.id.clear_password);
		username = (EditText) findViewById(R.id.username);
		pwd = (EditText) findViewById(R.id.pwd);
		
		clear_username.setTypeface(App.getInstance().getCommnonTypeface());
		clear_username.setText(Html.fromHtml("&#xe607"));
		username_tag.setTypeface(App.getInstance().getCommnonTypeface());
		username_tag.setText(Html.fromHtml("&#xe60b"));
		pwd_tag.setTypeface(App.getInstance().getCommnonTypeface());
		pwd_tag.setText(Html.fromHtml("&#xe609"));
	}

	private void setListener() {
		login_button.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String account = username.getText().toString();
				String password = pwd.getText().toString();
				if (TextUtils.isEmpty(account)) {
					ToastFactory.showToast(LoginActivity.this, "帐号不能为空", Toast.LENGTH_SHORT);
					return;
				}
				if (TextUtils.isEmpty(password)) {
					ToastFactory.showToast(LoginActivity.this, "密码不能为空", Toast.LENGTH_SHORT);
					return;
				}
				
				HttpProxy.requestUserLogin(account, password, LoginActivity.this);
				
				showWaitDialog("登录中");

			}
		});
		login_forget_password.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent();
				i.setClass(LoginActivity.this, ForgetPasswordStep1.class);
				startActivity(i);
			}
		});
		clear_username.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				username.setText("");
				pwd.setText("");
			}
		});
		username.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				if(s == username.getText()) {
					if(!TextUtils.isEmpty(s)) {
						clear_username.setVisibility(View.VISIBLE);
					} else {
						clear_username.setVisibility(View.GONE);
					}
				}
			}
		});
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	//06-09 01:17:15.715: D/larry(26824): content {"ResultCode":"0","ResultMsg":"登陆成功","ResultContent":{"id":"1","username":"dpdzq","password":"e10adc3949ba59abbe56e057f20f883e","name":"eric","gender":"0","birthday":"2015-06-05","address":"北京市","mobile":"13999999999","email":"eric@server.com","score":"53700","dealer_id":"0","user_type":"1","pic_url":"http://www.ahaokang.com/Public/Uploads/2015/06/07/5573afe430b76.jpg","token":"MXxkcGR6cXwxNDMzNzgzODM2fDF8NjI3QzcyN0Q3MDcwNzI3MTdDN0Q2NTBBMEEwNDA3MEQwMTBGMEUxQzAzMDAxNzA1MEQwODAzMDYwNTA2N0Q3Mw==","company":"","phone":""}}

	@Override
	public void onSuccess(String data) {
		// TODO Auto-generated method stub
		Log.d("larry", "content " + data );
		hideWaitDialog();
		BaseResult result = JsonParser.parserLogin(data);
		if(0 == result.getResultCode()) {
			Intent intent = new Intent();
			intent.setClass(LoginActivity.this, MainActivity.class);
			startActivityForResult(intent, 0);
			
			UserInfo res = (UserInfo) result.getResultContent();
			boolean exit = true;
			mCurrentUser = DBManager.getUserDBHandler().getUserById(res.getId());
			if (mCurrentUser == null) {
				mCurrentUser = new UserInfo();
				exit = false;
			}
			mCurrentUser = res;
			AppConstants.CURRENT_USER_ID = mCurrentUser.getId();
			mCurrentUser.setStatus(1);
			if (exit) {
				DBManager.getUserDBHandler().updateUserInfo(mCurrentUser);
			} else {
				DBManager.getUserDBHandler().addUserInfo(mCurrentUser);
			}
		} else {
			ToastFactory.showToast(LoginActivity.this, result.getResultMsg(), Toast.LENGTH_SHORT);
		}
	}

	@Override
	public void onFailure(String error) {
		// TODO Auto-generated method stub
		Log.e("larry", "onFailure " + error);
		hideWaitDialog();
		ToastFactory.showToast(LoginActivity.this, error, Toast.LENGTH_SHORT);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if(resultCode == Activity.RESULT_OK) {
			if(requestCode == 0) {
				finish();	//登录页面不要在其他地方出现finish();
			}
		}
	}

	
}
