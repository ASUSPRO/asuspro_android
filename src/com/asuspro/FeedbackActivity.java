package com.asuspro;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.asuspro.base.BaseActivity;

/**
 * 意见反馈
 * larry create on 2015-5-30
 */
public class FeedbackActivity extends BaseActivity{ 
	
	private EditText feedback_content;
	private Button feedback_sumbit;

	@Override
	protected int getContentView() {
		// TODO Auto-generated method stub
		return R.layout.activity_feedback;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		initView();
	}
	
	private void initView() {
		feedback_content = (EditText) findViewById(R.id.feedback_content);
		feedback_sumbit = (Button) findViewById(R.id.feedback_sumbit);
		
		
		feedback_sumbit.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String s = feedback_content.getText().toString();
				if(TextUtils.isEmpty(s)) {
					Toast.makeText(FeedbackActivity.this, "" + s, Toast.LENGTH_SHORT).show();
				}

			}
		});
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	protected String getHeaderBarTitle() {
		// TODO Auto-generated method stub
		return "反馈建议";
	}

	@Override
	protected boolean onClickLeftEvent() {
		// TODO Auto-generated method stub
		finish();
		return super.onClickLeftEvent();
	}

}
