package com.asuspro;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.asuspro.base.App;
import com.asuspro.base.BaseActivity;
import com.asuspro.dbhelper.DBManager;
import com.asuspro.model.UserInfo;

/**
 * 设置
 * larry create on 2015-5-25
 */
public class SettingActivity extends BaseActivity {
	private RelativeLayout setting_checkup_layout, setting_cache_layout, setting_feedback_layout, setting_logout_layout;
	private TextView setting_feedback_right, setting_cache;
	
	private UserInfo mCurrentUser;

	@Override
	protected int getContentView() {
		// TODO Auto-generated method stub
		return R.layout.activity_setting;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		initView();
		setListener();
	}

	private void initView() {
		setting_checkup_layout = (RelativeLayout) findViewById(R.id.setting_checkup_layout);
		setting_cache_layout = (RelativeLayout) findViewById(R.id.setting_cache_layout);
		setting_feedback_layout = (RelativeLayout) findViewById(R.id.setting_feedback_layout);
		setting_logout_layout = (RelativeLayout) findViewById(R.id.setting_logout_layout);
		setting_cache = (TextView) findViewById(R.id.setting_cache);
		
		setting_feedback_right = (TextView) findViewById(R.id.setting_feedback_right);
		setting_feedback_right.setTypeface(App.getInstance().getCommnonTypeface());
		setting_feedback_right.setText(Html.fromHtml("&#xe60d"));
		
	}
	
	private void setListener() {
		setting_checkup_layout.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
			}
		});
		setting_cache_layout.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
			}
		});
		setting_feedback_layout.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent feedbackIntent = new Intent();
				feedbackIntent.setClass(SettingActivity.this, FeedbackActivity.class);
				startActivity(feedbackIntent);
			}
		});
		setting_logout_layout.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent();
				i.setClass(SettingActivity.this, LoginActivity.class);
				i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(i);
				
				mCurrentUser = DBManager.getUserDBHandler().getLoginUser();
				mCurrentUser.setStatus(0);
				DBManager.getUserDBHandler().updateUserInfo(mCurrentUser);
			}
		});
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	protected String getHeaderBarTitle() {
		// TODO Auto-generated method stub
		return "设置";
	}

	@Override
	protected boolean onClickLeftEvent() {
		// TODO Auto-generated method stub
		finish();
		return super.onClickLeftEvent();
	}

}
