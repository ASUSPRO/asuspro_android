package com.asuspro;

import java.io.File;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.asuspro.base.App;
import com.asuspro.base.BaseActivity;
import com.asuspro.config.AppConstants;
import com.asuspro.dbhelper.DBManager;
import com.asuspro.manager.HttpProxy;
import com.asuspro.manager.HttpProxy.AsyncResponseListener;
import com.asuspro.model.UserInfo;
import com.asuspro.tools.BitmapTool;
import com.asuspro.tools.FileTools;
import com.asuspro.tools.ToastFactory;
import com.asuspro.tools.Tools;
import com.asuspro.view.CircleImageView;
import com.asuspro.view.WanDialog;

/**
 * 我的信息 larry create on 2015-5-9
 */
public class MineActivity extends BaseActivity {
	
	private static final int PHOTO_REQUEST_CODE = 109;
	private static final int CAMERA_REQUEST_CODE = 110;
	private static final int SELECT_IMAGE_REQUEST = 111;
	private final int UPDATE_USER_ICON = 0xFF110;
	
	private RelativeLayout mine_information, mine_record, mine_integral,
			mine_address, mine_setting;
	private TextView mine_information_right, mine_record_right, mine_integral_right, mine_address_right, mine_setting_right;
	private TextView mine_information_icon, mine_record_icon, mine_integral_icon, mine_address_icon, mine_setting_icon;
	private TextView mine_name;
	private CircleImageView mine_avatar;

	private UserInfo mCurrentUser;
	
	private Bitmap muserIconBmp;
	private String mImagePath = null;// 头像本地地址
	
	private boolean isCanDownloadUserIcon = true;
	
	private Handler mHandler = new Handler() {

		@Override
		public void handleMessage(Message msg) {
			// TODO Auto-generated method stub
			super.handleMessage(msg);
			switch (msg.what) {
			case UPDATE_USER_ICON:
				if (TextUtils.isEmpty(mImagePath)) {
					return;
				}
				muserIconBmp = BitmapFactory.decodeFile(mImagePath);
				if (mine_avatar != null) {
					mine_avatar.setImageBitmap(muserIconBmp);
				}
				break;
			case 0:// upload avatar success
					// updata database
				String url = (String) msg.obj;
				mCurrentUser.setPic_url(url);
				DBManager.getUserDBHandler().updateUserInfo(mCurrentUser);

				String avatarName = FileTools.getUserAvatarName(url);
				String avatarPath = FileTools.getUserAvatarDir(DBManager.getUserDBHandler().getLoginUser().getId() + "");
				mImagePath = avatarPath + "/" + avatarName;

				muserIconBmp = BitmapFactory.decodeFile(mImagePath);
				// PersonalCenterFragment.mBitmapIcon = mBmp;
				if (mine_avatar != null) {
					mine_avatar.setImageBitmap(muserIconBmp);
				}

				ToastFactory.showToast(MineActivity.this, "上传头像成功", Toast.LENGTH_SHORT);
				break;
			case 1:// upload avatar fail
			case 2:// upload avatar fail

				String message = (String) msg.obj;

				ToastFactory.showToast(MineActivity.this, message + "", Toast.LENGTH_SHORT);

				break;
			default:
				break;
			}
		}

	};
	
	
	@Override
	protected int getContentView() {
		// TODO Auto-generated method stub
		return R.layout.activity_mine;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		initView();
		setListener();
	}

	private void initView() {
		mine_information = (RelativeLayout) findViewById(R.id.mine_information);
		mine_record = (RelativeLayout) findViewById(R.id.mine_record);
		mine_integral = (RelativeLayout) findViewById(R.id.mine_integral);
		mine_address = (RelativeLayout) findViewById(R.id.mine_address);
		mine_setting = (RelativeLayout) findViewById(R.id.mine_setting);
		
		mine_information_right = (TextView) findViewById(R.id.mine_information_right);
		mine_record_right = (TextView) findViewById(R.id.mine_record_right);
		mine_integral_right = (TextView) findViewById(R.id.mine_integral_right);
		mine_address_right = (TextView) findViewById(R.id.mine_address_right);
		mine_setting_right = (TextView) findViewById(R.id.mine_setting_right);
		
		mine_information_icon = (TextView) findViewById(R.id.mine_information_icon);
		mine_record_icon = (TextView) findViewById(R.id.mine_record_icon);
		mine_integral_icon = (TextView) findViewById(R.id.mine_integral_icon);
		mine_address_icon = (TextView) findViewById(R.id.mine_address_icon);
		mine_setting_icon = (TextView) findViewById(R.id.mine_setting_icon);
		
		mine_name = (TextView) findViewById(R.id.mine_name);
		mine_avatar = (CircleImageView) findViewById(R.id.mine_avatar);
		
		mine_information_right.setTypeface(App.getInstance().getCommnonTypeface());
		mine_record_right.setTypeface(App.getInstance().getCommnonTypeface());
		mine_integral_right.setTypeface(App.getInstance().getCommnonTypeface());
		mine_address_right.setTypeface(App.getInstance().getCommnonTypeface());
		mine_setting_right.setTypeface(App.getInstance().getCommnonTypeface());
		mine_information_right.setText(Html.fromHtml("&#xe60d"));
		mine_record_right.setText(Html.fromHtml("&#xe60d"));
		mine_integral_right.setText(Html.fromHtml(""));
		mine_address_right.setText(Html.fromHtml("&#xe60d"));
		mine_setting_right.setText(Html.fromHtml("&#xe60d"));
		
		mine_information_icon.setTypeface(App.getInstance().getCommnonTypeface());
		mine_record_icon.setTypeface(App.getInstance().getCommnonTypeface());
		mine_integral_icon.setTypeface(App.getInstance().getCommnonTypeface());
		mine_address_icon.setTypeface(App.getInstance().getCommnonTypeface());
		mine_setting_icon.setTypeface(App.getInstance().getCommnonTypeface());
		mine_information_icon.setText(Html.fromHtml("&#xe619"));
		mine_record_icon.setText(Html.fromHtml("&#xe618"));
		mine_integral_icon.setText(Html.fromHtml("&#xe608"));
		mine_address_icon.setText(Html.fromHtml("&#xe616"));
		mine_setting_icon.setText(Html.fromHtml("&#xe617"));
		
		headerRight.setTextSize(Tools.dipToPx(6));
	}

	private void setListener() {
		mine_information.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent();
				i.setClass(MineActivity.this, WebViewActivity.class);
				i.putExtra("title", "华硕商用资料平台");
				i.putExtra("url", "http://www.ahaokang.com/Product/index");
				startActivity(i);
			}
		});
		mine_record.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent recordIntent = new Intent();
				recordIntent.setClass(MineActivity.this, ForRecordActivity.class);
				startActivity(recordIntent);
			}
		});
//		mine_integral.setOnClickListener(new OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				// TODO Auto-generated method stub
//				// 积分
//			}
//		});
		mine_address.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent();
				i.setClass(MineActivity.this, AddressListActivity.class);
				startActivity(i);
			}
		});
		mine_setting.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent();
				i.setClass(MineActivity.this, SettingActivity.class);
				startActivity(i);
			}
		});
		mine_avatar.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				showSelecteDialog();
			}
		});
	}
	
	private void initData() {
		mCurrentUser = DBManager.getUserDBHandler().getLoginUser();
		if(mCurrentUser != null) {
			try {
				checkUploadAvatar(false);
			} catch (Exception e) {
				e.printStackTrace();
			}
			mine_name.setText(mCurrentUser.getName());
			mine_integral_right.setText(Html.fromHtml(mCurrentUser.getScore() + ""));
		}
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		initData();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	protected String getHeaderBarTitle() {
		// TODO Auto-generated method stub
		return "";
	}

	@Override
	protected boolean onClickLeftEvent() {
		// TODO Auto-generated method stub
		finish();
		return super.onClickLeftEvent();
	}

	@Override
	protected boolean onClickRightEvent() {
		// TODO Auto-generated method stub
		Intent i = new Intent();
		i.setClass(MineActivity.this, MineEditActivity.class);
		startActivityForResult(i, 0);
		return super.onClickRightEvent();
	}

	@Override
	protected String getHeaderBarRightIcon() {
		// TODO Auto-generated method stub
		return "编辑";
	}

	@Override
	protected String getHeaderBarLeftIcon() {
		// TODO Auto-generated method stub
		return "&#xe607";
	}
	
	
	private void showSelecteDialog() {
		WanDialog dialog = new WanDialog(this, "更换头像");
		dialog.setItemContent(new String[] { "相册", "拍照" },
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						switch (which) {
						case 0:
							openAlbums();
							break;
						case 1:
							openCamera();
							break;
						}
						dialog.dismiss();
					}
				});
		dialog.setCanceledOnTouchOutside(true);
		dialog.show();
	}

	protected void openCamera() {
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		String tempPath = FileTools.getCacheDir()
				+ AppConstants.FILE_NAME_CAMERA_TEMP;
		intent.putExtra(MediaStore.EXTRA_OUTPUT,
				Uri.fromFile(new File(tempPath)));
		startActivityForResult(intent, CAMERA_REQUEST_CODE);
	}

	protected void openAlbums() {
		Intent intent = new Intent(Intent.ACTION_GET_CONTENT, null);
		intent.setType("image/*");
		startActivityForResult(intent, PHOTO_REQUEST_CODE);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if(resultCode == Activity.RESULT_OK) {
			if (requestCode == PHOTO_REQUEST_CODE) {// 调用Gallery返回的
				if (data == null) {
					return;
				}
				// 进入图片裁剪页面
				Intent albumIntent = new Intent(this, ImageCutActivity.class);
				albumIntent.putExtra("action", ImageCutActivity.ALBUM);
				albumIntent.putExtra("type", "avatar");
				albumIntent.setData(data.getData());
				startActivityForResult(albumIntent, SELECT_IMAGE_REQUEST);
			} else if (requestCode == CAMERA_REQUEST_CODE) {// 照相机程序返回的,再次调用图片剪辑程序去修剪图片

				String loacalPath = FileTools.getCacheDir()
						+ AppConstants.FILE_NAME_CAMERA_TEMP;
				if (FileTools.fileIsExists(loacalPath)) {
					Intent cameraIntent = new Intent(this,
							ImageCutActivity.class);
					cameraIntent.putExtra("action", ImageCutActivity.CAMERA);
					cameraIntent.putExtra("type", "avatar");
					startActivityForResult(cameraIntent, SELECT_IMAGE_REQUEST);
				}

			} else if (requestCode == SELECT_IMAGE_REQUEST) {// 剪裁页面返回的。

				if (data == null) {
					// setPhotoAdapterNone();
					return;
				}
				String imagePath = data.getStringExtra("image_path");
				mImagePath = imagePath;
				muserIconBmp = BitmapFactory.decodeFile(mImagePath);
				// PersonalCenterFragment.mBitmapIcon = mBmp;
				if (mine_avatar != null) {
					BitmapTool.releaseImageViewResouce(mine_avatar);
					mine_avatar.setImageBitmap(muserIconBmp);
				}
				checkUploadAvatar(true);
			}
		}
	}
	
	/************************************ 头像处理 ****************************************/

	private void checkUploadAvatar(boolean forceUpdate) {

		if (mCurrentUser != null) { // 检查图片上传

			if (forceUpdate) {// 裁剪后图片处理
				String cutPath = FileTools.getCacheDir()
						+ AppConstants.FILE_NAME_IMAGECUT_TEMP;
				requestUploadAvatar(cutPath);
			} else {

				String serverUserIcon = mCurrentUser.getPic_url();
				String avatarName = FileTools.getUserAvatarName(serverUserIcon);
				String avatarPath = FileTools.getUserAvatarDir(mCurrentUser.getId() + "");

				if (FileTools.fileIsExists(avatarPath + "/" + avatarName)
						&& !TextUtils.isEmpty(avatarName)) {// 本地有最新的头像图片
					muserIconBmp = BitmapFactory.decodeFile(avatarPath + "/" + avatarName);
					if (mine_avatar != null) {
						mine_avatar.setImageBitmap(muserIconBmp);
					}
				} else {// download
					mine_avatar.setImageResource(R.drawable.icon_avatar_photo);
					// download user icon
					startDownloadUserIcon(serverUserIcon, avatarPath, avatarName);

				}
			}
		}
	}

	private synchronized void startDownloadUserIcon(final String downUrl,
			final String savePath, final String filename) {

		if (!isCanDownloadUserIcon) {
			return;
		}
		isCanDownloadUserIcon = false;

		new Thread() {
			@Override
			public void run() {
				super.run();
				boolean result = FileTools.getImageFromNetAndSave(downUrl,
						savePath, filename);
				if (result) {
					mImagePath = savePath + "/" + filename;
					Message msg = new Message();
					msg.what = UPDATE_USER_ICON;
					// msg.arg1 = 1;
					mHandler.sendMessage(msg);
				}
				isCanDownloadUserIcon = true;
			}
		}.start();
	}

	/**
	 * 上传头像
	 * @param path
	 */
	private void requestUploadAvatar(final String path) {
		showWaitDialog();
		HttpProxy.requestUploadUserPic(MineActivity.this, mCurrentUser.getId(), path, new AsyncResponseListener() {
			
			@Override
			public void onSuccess(String data) {
				// TODO Auto-generated method stub
				Log.d("larry", "上传头像成功 " + data);
				hideWaitDialog();
				
				Message msg = new Message();
				msg.what = 0;
				msg.obj = data;
				mHandler.sendMessage(msg);

				// copy file to avatar folder
				String avatarName = FileTools.getUserAvatarName(data);
				String avatarPath = FileTools
						.getUserAvatarDir(DBManager.getUserDBHandler().getLoginUser().getId() + "");
				String newPath = avatarPath + "/" + avatarName;
				FileTools.copyFile(path, newPath);
			}
			
			@Override
			public void onFailure(String error) {
				// TODO Auto-generated method stub
				hideWaitDialog();
				Message msg = new Message();
				msg.what = 2;
				msg.obj = error;
				mHandler.sendMessage(msg);
			}
		});
	}
	
}
