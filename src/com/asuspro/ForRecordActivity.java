package com.asuspro;

import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;
import android.widget.Toast;

import com.asuspro.adapter.ForRecordAdapter;
import com.asuspro.base.BaseActivity;
import com.asuspro.dbhelper.DBManager;
import com.asuspro.manager.HttpProxy;
import com.asuspro.manager.HttpProxy.AsyncResponseListener;
import com.asuspro.manager.JsonParser;
import com.asuspro.model.BaseResult;
import com.asuspro.model.ExchangeInfo;
import com.asuspro.model.UserInfo;
import com.asuspro.tools.ToastFactory;

/**
 * 兑换记录 larry create on 2015-5-30
 */
public class ForRecordActivity extends BaseActivity {
	private ListView mListView;
	private ForRecordAdapter mAdapter;
	private UserInfo mCurrentUser;

	private int page = 0;
	
	@Override
	protected int getContentView() {
		// TODO Auto-generated method stub
		return R.layout.activity_for_record;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		initView();
		initData();
		
		mAdapter = new ForRecordAdapter(this);
		mListView.setAdapter(mAdapter);
		
		requestRecordData();
	}

	private void initView() {
		// TODO Auto-generated method stub
		mListView = (ListView) findViewById(R.id.mListView);
	}

	private void initData(){
		mCurrentUser = DBManager.getUserDBHandler().getLoginUser();
		if(mCurrentUser == null) {
			ToastFactory.showToast(this, "登录状态已失效，请重新登录", Toast.LENGTH_SHORT);
			Intent i = new Intent();
			i.setClass(this, LoginActivity.class);
			startActivity(i);
			finish();
			return;
		}
		
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	protected String getHeaderBarTitle() {
		// TODO Auto-generated method stub
		return "兑换记录";
	}

	@Override
	protected boolean onClickLeftEvent() {
		// TODO Auto-generated method stub
		finish();
		return super.onClickLeftEvent();
	}
	
	private void requestRecordData() {
		showWaitDialog();
		HttpProxy.requestGetExchangeList(mCurrentUser.getId(), page, new AsyncResponseListener() {
			
			@Override
			public void onSuccess(String data) {
				// TODO Auto-generated method stub
				Log.d("larry", "--data--- : " + data);
				hideWaitDialog();
				BaseResult result = JsonParser.parserForRecordList(data);
				if(0 == result.getResultCode()) {
					List<ExchangeInfo> exchangeInfos = (List<ExchangeInfo>) result.getResultContent();
					
					mAdapter.setData(exchangeInfos);
					
				} else {
					ToastFactory.showToast(ForRecordActivity.this, result.getResultMsg(), Toast.LENGTH_SHORT);
				}
			}
			
			@Override
			public void onFailure(String error) {
				// TODO Auto-generated method stub
				Log.e("larry", "onFailure " + error);
				hideWaitDialog();
				ToastFactory.showToast(ForRecordActivity.this, error, Toast.LENGTH_SHORT);
			}
		});
	}

}
