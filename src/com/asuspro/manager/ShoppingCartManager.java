package com.asuspro.manager;

import java.util.ArrayList;
import java.util.List;

import com.asuspro.model.ProductInfo;

/**
 * 购物车管理类
 * larry create on 2015-6-19
 */
public class ShoppingCartManager {
	private static ShoppingCartManager instance;
	
	private List<ProductInfo> productInfos;
	
	private ShoppingCartManager() {
		productInfos = new ArrayList<ProductInfo>();
	}
	
	public static ShoppingCartManager getInstance () {
		if(instance == null) {
			instance = new ShoppingCartManager();
		}
		return instance;
	}
	
	public List<ProductInfo> getProductInfos() {
		return productInfos;
	}
	
	public void addProductInfo(ProductInfo product) {
		productInfos.add(product);
	}
	
	public void removeProductInfo(ProductInfo product) {
		productInfos.remove(product);
	}
	
}
