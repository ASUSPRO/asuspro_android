package com.asuspro.manager;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import android.text.TextUtils;

import com.asuspro.model.AddressInfo;
import com.asuspro.model.AdvertisementInfo;
import com.asuspro.model.BaseResult;
import com.asuspro.model.CategoryBean;
import com.asuspro.model.ExchangeInfo;
import com.asuspro.model.GiftInfo;
import com.asuspro.model.ImageInfo;
import com.asuspro.model.NoticeInfo;
import com.asuspro.model.ProductInfo;
import com.asuspro.model.UserInfo;

/**
 * Json解析器 larry create on 2015-6-10
 */
public class JsonParser {
	
	/**
	 * 1.登录数据解析
	 * @param jsonString
	 * @return
	 */
	public static BaseResult parserLogin(String jsonString) {
		BaseResult result = new BaseResult();
		try {
			JSONObject dataObject = new JSONObject(jsonString);
			result.setResultCode(dataObject.optInt("ResultCode"));
			result.setResultMsg(dataObject.optString("ResultMsg"));
			String content = dataObject.optString("ResultContent");
			if(TextUtils.isEmpty(content)) {
				result.setResultContent(new UserInfo());
			} else {
				JSONObject contentObject = new JSONObject(content);
				UserInfo userInfo = new UserInfo();
				userInfo.setId(contentObject.optInt("id"));
				userInfo.setUsername(contentObject.optString("username"));
				userInfo.setPassword(contentObject.optString("password"));
				userInfo.setName(contentObject.optString("name"));
				userInfo.setGender(contentObject.optInt("gender"));
				userInfo.setBirthday(contentObject.optString("birthday"));
				userInfo.setAddress(contentObject.optString("address"));
				userInfo.setMobile(contentObject.optString("mobile"));
				userInfo.setEmail(contentObject.optString("email"));
				userInfo.setScore(contentObject.optLong("score"));
				userInfo.setDealer_id(contentObject.optInt("dealer_id"));
				userInfo.setUser_type(contentObject.optInt("user_type"));
				userInfo.setPic_url(contentObject.optString("pic_url"));
				userInfo.setToken(contentObject.optString("token"));
				userInfo.setCompany(contentObject.optString("company"));
				userInfo.setPhone(contentObject.optString("phone"));
				result.setResultContent(userInfo);
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			result.setResultCode(10000);
			result.setResultMsg("数据解析错误");
			result.setResultContent(null);
			return result;
		}
		return result;
	}
	
	/**
	 * 2.解析栏目页上方图片
	 * @param jsonString
	 * @return
	 */
	public static BaseResult parserColumnPic(String jsonString) {
		BaseResult result = new BaseResult();
		try {
			JSONObject dataObject = new JSONObject(jsonString);
			result.setResultCode(dataObject.optInt("ResultCode"));
			result.setResultMsg(dataObject.optString("ResultMsg"));
			String content = dataObject.optString("ResultContent");
			if(TextUtils.isEmpty(content)) {
				result.setResultContent(new ArrayList<AdvertisementInfo>());
			} else {
				JSONArray contentArray = new JSONArray(content);
				List<AdvertisementInfo> adverInfos = new ArrayList<AdvertisementInfo>();
				for(int i=0; i<contentArray.length(); i++) {
					JSONObject contentObject = contentArray.getJSONObject(i);
					AdvertisementInfo adverInfo = new AdvertisementInfo();
					ImageInfo imageInfo = new ImageInfo();
					imageInfo.setOriginalURL(contentObject.optString("pic_url"));
					adverInfo.setImage(imageInfo);
					adverInfo.setLink(contentObject.optString("link_url"));
					adverInfos.add(adverInfo);
				}
				result.setResultContent(adverInfos);
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			result.setResultCode(10000);
			result.setResultMsg("数据解析错误");
			result.setResultContent(null);
			return result;
		}
		return result;
	}
	
	/**
	 * 3.解析积分商城商品列表
	 * @param jsonString
	 * @return
	 */
	public static BaseResult parserGifList(String jsonString) {
		BaseResult result = new BaseResult();
		try {
			JSONObject dataObject = new JSONObject(jsonString);
			result.setResultCode(dataObject.optInt("ResultCode"));
			result.setResultMsg(dataObject.optString("ResultMsg"));
			String content = dataObject.optString("ResultContent");
			if(TextUtils.isEmpty(content)) {
				result.setResultContent(new ArrayList<GiftInfo>());
			} else {
				JSONArray contentArray = new JSONArray(content);
				List<GiftInfo> giftInfos = new ArrayList<GiftInfo>();
				for(int i=0; i<contentArray.length(); i++) {
					JSONObject contentObject = contentArray.getJSONObject(i);
					GiftInfo giftInfo = new GiftInfo();
					giftInfo.setId(contentObject.optInt("id"));
					giftInfo.setGift_name(contentObject.optString("gift_name"));
					giftInfo.setCat_id(contentObject.optInt("cat_id"));
					giftInfo.setInner_score(contentObject.optInt("inner_score"));
					giftInfo.setOuter_score(contentObject.optInt("outer_score"));
					giftInfo.setNum(contentObject.optInt("num"));
					giftInfo.setGift_pic(contentObject.optString("gift_pic"));
					giftInfo.setGift_desc(contentObject.optString("gift_desc"));
					giftInfo.setGift_note(contentObject.optString("gift_note"));
					giftInfo.setRecommend(contentObject.optInt("recommend"));
					giftInfo.setRecommend_pic(contentObject.optString("recommend_pic"));
					giftInfo.setShow_flag(contentObject.optInt("show_flag"));
					giftInfo.setCreate_time(contentObject.optString("create_time"));
					giftInfo.setIs_asus(contentObject.optInt("is_asus"));
					giftInfos.add(giftInfo);
				}
				result.setResultContent(giftInfos);
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			result.setResultCode(10000);
			result.setResultMsg("数据解析错误");
			result.setResultContent(null);
			return result;
		}
		return result;
	}
	
	/**
	 * 4.解析分类
	 * @param jsonString
	 * @return
	 */
	public static BaseResult parserCategory(String jsonString) {
		BaseResult result = new BaseResult();
		try {
			JSONObject dataObject = new JSONObject(jsonString);
			result.setResultCode(dataObject.optInt("ResultCode"));
			result.setResultMsg(dataObject.optString("ResultMsg"));
			String content = dataObject.optString("ResultContent");
			if(TextUtils.isEmpty(content)) {
				result.setResultContent(new ArrayList<CategoryBean>());
			} else {
				JSONArray contentArray = new JSONArray(content);
				List<CategoryBean> classifys = new ArrayList<CategoryBean>();
				for(int i=0; i<contentArray.length(); i++) {
					JSONObject contentObject = contentArray.getJSONObject(i);
					CategoryBean classify = new CategoryBean();
					classify.setColumn_id(contentObject.optInt("id"));
					classify.setColumn_name(contentObject.optString("name"));
					classifys.add(classify);
				}
				result.setResultContent(classifys);
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			result.setResultCode(10000);
			result.setResultMsg("数据解析错误");
			result.setResultContent(null);
		}
		return result;
	}
	
	/**
	 * 5.解析政策通知
	 * @param jsonString
	 * @return
	 */
	public static BaseResult parserNotice(String jsonString)  {
		BaseResult result = new BaseResult();
		try {
			JSONObject dataObject = new JSONObject(jsonString);
			result.setResultCode(dataObject.optInt("ResultCode"));
			result.setResultMsg(dataObject.optString("ResultMsg"));
			String content = dataObject.optString("ResultContent");
			if(TextUtils.isEmpty(content)) {
				result.setResultContent(new ArrayList<NoticeInfo>());
			} else {
				JSONArray contentArray = new JSONArray(content);
				List<NoticeInfo> notices = new ArrayList<NoticeInfo>();
				for(int i=0; i<contentArray.length(); i++) {
					JSONObject contentObject = contentArray.getJSONObject(i);
					NoticeInfo notice = new NoticeInfo();
					notice.setId(contentObject.optInt("id"));
					notice.setMaintitle(contentObject.optString("maintitle"));
					notice.setSource(contentObject.optString("source"));
					notice.setCreatetime(contentObject.optLong("createtime"));
					notices.add(notice);
				}
				result.setResultContent(notices);
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			result.setResultCode(10000);
			result.setResultMsg("数据解析错误");
			result.setResultContent(null);
		}
		return result;
	}
	
	/**
	 * 6.解析政策通知详情
	 * @param jsonString
	 * @return
	 */
	public static BaseResult parserNoticeDetail(String jsonString) {
		BaseResult result = new BaseResult();
		try {
			JSONObject dataObject = new JSONObject(jsonString);
			result.setResultCode(dataObject.optInt("ResultCode"));
			result.setResultMsg(dataObject.optString("ResultMsg"));
			JSONObject contentObject = dataObject.optJSONObject("ResultContent");
			if(contentObject == null) {
				result.setResultContent(new NoticeInfo());
			} else {
				NoticeInfo notice = new NoticeInfo();
				notice.setId(contentObject.optInt("id"));
				notice.setMaintitle(contentObject.optString("maintitle"));
				notice.setSource(contentObject.optString("source"));
				notice.setCreatetime(contentObject.optLong("createtime"));
				notice.setContent(contentObject.optString("content"));
				result.setResultContent(notice);
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			result.setResultCode(10000);
			result.setResultMsg("数据解析错误");
			result.setResultContent(null);
		}
		return result;
	}
	
	/**
	 * 7.解析产品分类
	 * @param jsonString
	 * @return
	 */
	public static BaseResult parserProductCategory(String jsonString) {
		BaseResult result = new BaseResult();
		try {
			JSONObject dataObject = new JSONObject(jsonString);
			result.setResultCode(dataObject.optInt("ResultCode"));
			result.setResultMsg(dataObject.optString("ResultMsg"));
			String content = dataObject.optString("ResultContent");
			if(TextUtils.isEmpty(content)) {
				result.setResultContent(new ArrayList<CategoryBean>());
			} else {
				JSONArray contentArray = new JSONArray(content);
				List<CategoryBean> classifys = new ArrayList<CategoryBean>();
				for(int i=0; i<contentArray.length(); i++) {
					JSONObject contentObject = contentArray.getJSONObject(i);
					CategoryBean classify = new CategoryBean();
					classify.setColumn_id(contentObject.optInt("id"));
					classify.setColumn_name(contentObject.optString("category_name"));
					classify.setParent_id(contentObject.optString("parent_id"));
					classifys.add(classify);
				}
				result.setResultContent(classifys);
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			result.setResultCode(10000);
			result.setResultMsg("数据解析错误");
			result.setResultContent(null);
		}
		return result;
	}
	
	/**
	 * 8.解析产品
	 * @param jsonString
	 * @return
	 */
	public static BaseResult parserProduct(String jsonString) {
		BaseResult result = new BaseResult();
		try {
			JSONObject dataObject = new JSONObject(jsonString);
			result.setResultCode(dataObject.optInt("ResultCode"));
			result.setResultMsg(dataObject.optString("ResultMsg"));
			String content = dataObject.optString("ResultContent");
			if(TextUtils.isEmpty(content)) {
				result.setResultContent(new ArrayList<ProductInfo>());
			} else {
				JSONArray contentArray = new JSONArray(content);
				List<ProductInfo> products = new ArrayList<ProductInfo>();
				for(int i=0; i<contentArray.length(); i++) {
					JSONObject contentObject = contentArray.getJSONObject(i);
					ProductInfo product = new ProductInfo();
					product.setProduct_id(contentObject.optInt("id"));
					product.setCat_id(contentObject.optLong("cat_id"));
					product.setType(contentObject.optString("type"));
					product.setNum(contentObject.optInt("num"));
					product.setPart_number(contentObject.optString("part_number"));
					product.setPrice(contentObject.optLong("price"));
					product.setStock_position(contentObject.getString("stock_position"));
					products.add(product);
				}
				result.setResultContent(products);
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			result.setResultCode(10000);
			result.setResultMsg("数据解析错误");
			result.setResultContent(null);
		}
		return result;
	}
	
	/**
	 * 9.解析礼品商城首页的商品推荐列表（简化数据）
	 * 		只包含 id、cat_id、recommeng_pic三个字段
	 * @param jsonString
	 * @return
	 */
	public static BaseResult parserShopIndexList(String jsonString) {
		BaseResult result = new BaseResult();
		try {
			JSONObject dataObject = new JSONObject(jsonString);
			result.setResultCode(dataObject.optInt("ResultCode"));
			result.setResultMsg(dataObject.optString("ResultMsg"));
			String content = dataObject.optString("ResultContent");
			if(TextUtils.isEmpty(content)) {
				result.setResultContent(new ArrayList<GiftInfo>());
			} else {
				JSONArray contentArray = new JSONArray(content);
				List<GiftInfo> giftInfos = new ArrayList<GiftInfo>();
				for(int i=0; i<contentArray.length(); i++) {
					JSONObject contentObject = contentArray.getJSONObject(i);
					GiftInfo giftInfo = new GiftInfo();
					giftInfo.setId(contentObject.optInt("id"));
					giftInfo.setCat_id(contentObject.optInt("cat_id"));
					giftInfo.setRecommend_pic(contentObject.optString("recommend_pic"));
					giftInfos.add(giftInfo);
				}
				result.setResultContent(giftInfos);
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			result.setResultCode(10000);
			result.setResultMsg("数据解析错误");
			result.setResultContent(null);
			return result;
		}
		return result;
	}
	
	/**
	 * 10.解析商品详情
	 * @param jsonString
	 * @return
	 */
	public static BaseResult parserGifDetail(String jsonString) {
		BaseResult result = new BaseResult();
		try {
			JSONObject dataObject = new JSONObject(jsonString);
			result.setResultCode(dataObject.optInt("ResultCode"));
			result.setResultMsg(dataObject.optString("ResultMsg"));
			String content = dataObject.optString("ResultContent");
			if(TextUtils.isEmpty(content)) {
				result.setResultContent(new GiftInfo());
			} else {
				JSONObject contentObject = new JSONObject(content);
				GiftInfo giftInfo = new GiftInfo();
				giftInfo.setId(contentObject.optInt("id"));
				giftInfo.setGift_name(contentObject.optString("gift_name"));
				giftInfo.setCat_id(contentObject.optInt("cat_id"));
				giftInfo.setInner_score(contentObject.optInt("inner_score"));
				giftInfo.setOuter_score(contentObject.optInt("outer_score"));
				giftInfo.setNum(contentObject.optInt("num"));
				giftInfo.setGift_pic(contentObject.optString("gift_pic"));
				giftInfo.setGift_desc(contentObject.optString("gift_desc"));
				giftInfo.setGift_note(contentObject.optString("gift_note"));
				giftInfo.setRecommend(contentObject.optInt("recommend"));
				giftInfo.setRecommend_pic(contentObject.optString("recommend_pic"));
				giftInfo.setShow_flag(contentObject.optInt("show_flag"));
				giftInfo.setCreate_time(contentObject.optString("create_time"));
				giftInfo.setIs_asus(contentObject.optInt("is_asus"));
				result.setResultContent(giftInfo);
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			result.setResultCode(10000);
			result.setResultMsg("数据解析错误");
			result.setResultContent(null);
			return result;
		}
		return result;
	}
	
	/**
	 * 11.解析兑换记录列表
	 * @param jsonString
	 * @return
	 */
	public static BaseResult parserForRecordList(String jsonString) {
		BaseResult result = new BaseResult();
		try {
			JSONObject dataObject = new JSONObject(jsonString);
			result.setResultCode(dataObject.optInt("ResultCode"));
			result.setResultMsg(dataObject.optString("ResultMsg"));
			String content = dataObject.optString("ResultContent");
			if(TextUtils.isEmpty(content)) {
				result.setResultContent(new ArrayList<ExchangeInfo>());
			} else {
				JSONArray contentArray = new JSONArray(content);
				List<ExchangeInfo> exchangeInfos = new ArrayList<ExchangeInfo>();
				for(int i=0; i<contentArray.length(); i++) {
					JSONObject contentObject = contentArray.getJSONObject(i);
					ExchangeInfo exchangeInfo = new ExchangeInfo();
					exchangeInfo.setId(contentObject.optInt("id"));
					exchangeInfo.setOrder_id(contentObject.optString("order_id"));
					exchangeInfo.setGift_id(contentObject.optInt("gift_id"));
					exchangeInfo.setGift_name(contentObject.optString("gift_name"));
					exchangeInfo.setGift_pic(contentObject.optString("gift_pic"));
					exchangeInfo.setScore(contentObject.optInt("score"));
					exchangeInfo.setNum(contentObject.optInt("num"));
					exchangeInfo.setCreate_time(contentObject.optLong("create_time"));
					exchangeInfo.setExpress(contentObject.optString("express"));
					exchangeInfo.setExpress_no(contentObject.optString("express_no"));
					exchangeInfos.add(exchangeInfo);
				}
				result.setResultContent(exchangeInfos);
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			result.setResultCode(10000);
			result.setResultMsg("数据解析错误");
			result.setResultContent(null);
			return result;
		}
		return result;
	}
	
	/**
	 * 12.解析收货地址列表
	 * @param jsonString
	 * @return
	 */
	public static BaseResult parserAddressList(String jsonString) {
		BaseResult result = new BaseResult();
		try {
			JSONObject dataObject = new JSONObject(jsonString);
			result.setResultCode(dataObject.optInt("ResultCode"));
			result.setResultMsg(dataObject.optString("ResultMsg"));
			String content = dataObject.optString("ResultContent");
			if(TextUtils.isEmpty(content)) {
				result.setResultContent(new ArrayList<AddressInfo>());
			} else {
				JSONArray contentArray = new JSONArray(content);
				List<AddressInfo> addressInfos = new ArrayList<AddressInfo>();
				for(int i=0; i<contentArray.length(); i++) {
					JSONObject contentObject = contentArray.getJSONObject(i);
					AddressInfo addressInfo = new AddressInfo();
					addressInfo.setId(contentObject.optInt("id"));
					addressInfo.setUser_id(contentObject.optInt("user_id"));
					addressInfo.setUsername(contentObject.optString("username"));
					addressInfo.setProvince(contentObject.optString("province"));
					addressInfo.setCity(contentObject.optString("city"));
					addressInfo.setArea(contentObject.optString("area"));
					addressInfo.setAddress(contentObject.optString("address"));
					addressInfo.setZip(contentObject.optString("zip"));
					addressInfo.setMobile(contentObject.optString("mobile"));
					addressInfo.setIs_default(contentObject.optInt("is_default"));
					addressInfos.add(addressInfo);
				}
				result.setResultContent(addressInfos);
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			result.setResultCode(10000);
			result.setResultMsg("数据解析错误");
			result.setResultContent(null);
			return result;
		}
		return result;
	}
	
	
	
}
