package com.asuspro.manager;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import com.asuspro.config.AppConstants;
import com.asuspro.model.ProductInfo;
import com.asuspro.tools.FileTools;
import com.asuspro.tools.IOUtils;
import com.lidroid.xutils.http.client.multipart.HttpMultipartMode;
import com.lidroid.xutils.http.client.multipart.MultipartEntity;
import com.lidroid.xutils.http.client.multipart.content.FileBody;
import com.lidroid.xutils.http.client.multipart.content.StringBody;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

/**
 * 网络请求代理类 larry create on 2015-6-8
 */
public class HttpProxy {
	private static AsyncHttpClient asyncHttpClient = new AsyncHttpClient();
	
	private static final String API_URL = "http://www.ahaokang.com/";
	private static final String API_URL_INTERFACE = API_URL + "Interface/";
	
	private static void postRequest(String url, RequestParams params, final AsyncResponseListener listener) {
		AsyncHttpResponseHandler responseHandler = new AsyncHttpResponseHandler(){
			@Override
			public void onSuccess(int statusCode, Header[] headers,
					String content) {
				// TODO Auto-generated method stub
				super.onSuccess(statusCode, headers, content);
				listener.onSuccess(content);
			}

			@Override
			public void onFailure(Throwable error, String content) {
				// TODO Auto-generated method stub
				super.onFailure(error, content);
				listener.onFailure(content);
			}
		};
		asyncHttpClient.post(url, params, responseHandler);
	}
	
	/**
	 * 上传图片
	 * @param b 图片字节流
	 */
	private static void postRequest(Context context, String url, RequestParams params, final AsyncResponseListener listener) {
		AsyncHttpResponseHandler responseHandler = new AsyncHttpResponseHandler(){
			@Override
			public void onSuccess(int statusCode, Header[] headers,
					String content) {
				// TODO Auto-generated method stub
				super.onSuccess(statusCode, headers, content);
				listener.onSuccess(content);
			}

			@Override
			public void onFailure(Throwable error, String content) {
				// TODO Auto-generated method stub
				super.onFailure(error, content);
				listener.onFailure(content);
			}
		};
		
//		HttpEntity entity = new ByteArrayEntity(b);
//		asyncHttpClient.post(context, url, entity, "application/octet-stream", responseHandler);
//		asyncHttpClient.post(url, params, responseHandler);
		asyncHttpClient.post(context, url, null, params, "image/png", responseHandler);
	}
	
	
	/**
	 * 1.用户登录
	 * @param account
	 * @param password
	 * @param responseHandler
	 */
	public static void requestUserLogin(String account, String password, AsyncResponseListener listener) {
		String url = API_URL_INTERFACE + "userLogin";
		RequestParams params = new RequestParams();
		params.put(AppConstants.PARAMS_TAG_ACCOUNT, account);
		params.put(AppConstants.PARAMS_TAG_PASSWORD, password);
		postRequest(url, params, listener);
	}
	
	/**
	 * 2.检查用户Token是否有效
	 * @param token
	 * @param listener
	 */
	public static void requestCheckUserToken(String token, AsyncResponseListener listener) {
		String url = API_URL_INTERFACE + "checkUserToken";
		RequestParams params = new RequestParams();
		params.put(AppConstants.PARAMS_TAG_TOKEN, token);
		postRequest(url, params, listener);
	}
	
	/**
	 * 3.获得省份列表
	 * @param listener
	 */
	public static void requestGetProvince(AsyncResponseListener listener) {
		String url = API_URL_INTERFACE + "getProvince";
		RequestParams params = new RequestParams();
		postRequest(url, params, listener);
	}
	
	/**
	 * 4.获得城市列表
	 * @param province_code
	 * 			如果province_code大于0，获得该省份下的所有城市列表
	 *			如果province_code为0，获得所有省份下的城市列表
	 * @param listener
	 */
	public static void requestGetCity(int province_code, AsyncResponseListener listener) {
		String url = API_URL_INTERFACE + "getCity";
		RequestParams params = new RequestParams();
		params.put(AppConstants.PARAMS_TAG_PROVINCE_CODE, "" + province_code);
		postRequest(url, params, listener);
	}
	
	/**
	 * 5.获得地区列表
	 * @param city_code
	 * @param listener
	 */
	public static void requestGetArea(int city_code, AsyncResponseListener listener) {
		String url = API_URL_INTERFACE + "getArea";
		RequestParams params = new RequestParams();
		params.put(AppConstants.PARAMS_TAG_CITY_CODE, "" + city_code);
		postRequest(url, params, listener);
	}
	
	/**
	 * 6.更新用户头像
	 * @param user_id
	 * 			更新头像的用户id
	 * @param pic_name
	 * 			上传头像的控件名称
	 * @param listener
	 */
	public static void requestUploadUserPic(Context context, int user_id, String path, AsyncResponseListener listener) {
//		String url = API_URL_INTERFACE + "uploadUserPic";
//		RequestParams params = new RequestParams();
//		params.put(AppConstants.PARAMS_TAG_USER_ID, "" + user_id);
//		params.put(AppConstants.PARAMS_TAG_PIC_URL, buffer);
////		postRequest(url, params, listener);
//		postRequest(context, url, params, listener);
//		String avatarName = FileTools.getUserAvatarName(path);
		Map<String, String> params = new HashMap<String, String>();
		params.put(AppConstants.PARAMS_TAG_USER_ID, "" + user_id);
		File file = new File(path);
		requestUploadUserAvater(user_id + "", params, file, listener);
	}
	
	/**
	 * 上传用户头像
	 * 
	 * @param filename
	 *            头像文件名
	 * @param data
	 *            头像文件转化字节数组
	 * @param listener
	 */
	public static void requestUploadUserAvater(final String userid,
			final Map<String, String> params, final File file,
			final AsyncResponseListener listener) {
		final String url = API_URL_INTERFACE + "uploadUserPic";
		Log.d("larry", "===== Thread =====");
		new Thread(new Runnable() {
			@Override
			public void run() {
				StringBuilder sb = new StringBuilder();
				sb.append("?")
						.append(AppConstants.PARAMS_TAG_USER_ID).append("=")
						.append(userid);
				List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
				NameValuePair nameValuePair = new BasicNameValuePair(
						AppConstants.PARAMS_TAG_USER_ID, userid);
				nameValuePairs.add(nameValuePair);
				nameValuePair = new BasicNameValuePair(AppConstants.PARAMS_TAG_PIC_URL,
						file.getAbsolutePath());
				nameValuePairs.add(nameValuePair);
				HttpClient httpClient = new DefaultHttpClient();
				HttpContext localContext = new BasicHttpContext();
				HttpResponse response = null;
				HttpPost httpPost = new HttpPost(url + sb.toString());
				try {
					MultipartEntity entity = new MultipartEntity(
							HttpMultipartMode.BROWSER_COMPATIBLE);

					for (int index = 0; index < nameValuePairs.size(); index++) {
						if (nameValuePairs.get(index).getName()
								.equalsIgnoreCase("pic_url")) {
							entity.addPart(
									nameValuePairs.get(index).getName(),
									new FileBody(new File(nameValuePairs.get(
											index).getValue())));
						} else {
							// Normal string data
							entity.addPart(nameValuePairs.get(index).getName(),
									new StringBody(nameValuePairs.get(index)
											.getValue()));
						}
					}

					httpPost.setEntity(entity);
					response = httpClient.execute(httpPost, localContext);
					int status = response.getStatusLine().getStatusCode();
					Log.d("larry", "----status---" + status);
					if (status == 200) {
						try {
							listener.onSuccess(IOUtils.convertInputStreamToString(response.getEntity().getContent()));
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					} else {
						listener.onFailure(response.getStatusLine().getReasonPhrase());
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}).start();

	}
	
	/**
	 * 7.添加收货地址
	 * @param user_id  必填
	 * @param username  必填
	 * @param province
	 * 			省份
	 * @param city
	 * 			城市
	 * @param area
	 * 			地区
	 * @param address  必填
	 * @param zip
	 * 			邮编
	 * @param mobile  必填
	 * @param is_default
	 * 			是否作为默认收货地址   0-非默认，1-默认
	 * @param listener
	 */
	public static void requestAddReceive(int user_id, String username, String province, String city, String area,
			String address, String zip, String mobile, int is_default, AsyncResponseListener listener) {
		String url = API_URL_INTERFACE + "addReceive";
		RequestParams params = new RequestParams();
		params.put(AppConstants.PARAMS_TAG_USER_ID, "" + user_id);
		params.put(AppConstants.PARAMS_TAG_USERNAME, username);
		params.put(AppConstants.PARAMS_TAG_PROVINCE, province);
		params.put(AppConstants.PARAMS_TAG_CITY, city);
		params.put(AppConstants.PARAMS_TAG_AREA, area);
		params.put(AppConstants.PARAMS_TAG_ADDRESS, address);
		params.put(AppConstants.PARAMS_TAG_ZIP, zip);
		params.put(AppConstants.PARAMS_TAG_MOBILE, mobile);
		params.put(AppConstants.PARAMS_TAG_IS_DEFAULT, "" + is_default);
		postRequest(url, params, listener);
	}
	
	/**
	 * 8.获取收货地址信息列表
	 * @param user_id
	 * @param listener
	 */
	public static void requestGetReceive(int user_id, AsyncResponseListener listener) {
		String url = API_URL_INTERFACE + "getReceive";
		RequestParams params = new RequestParams();
		params.put(AppConstants.PARAMS_TAG_USER_ID, "" + user_id);
		postRequest(url, params, listener);
	}
	
	/**
	 * 9.订单信息上传
	 * @param user_id
	 * 			购买人ID，即当前登录用户的ID
	 * @param receive_id
	 * 			收货人地址信息的id，通过getReceive接口获得的id
	 * @param useScore
	 * 			兑换需要的总积分
	 * @param products
	 * 			购买的商品
	 * @param listener
	 */
	public static void requestUploadOrder(int user_id, int receive_id, int useScore, ArrayList<ProductInfo> products, AsyncResponseListener listener) {
		String url = API_URL_INTERFACE + "uploadOrder";
		String data = "";
		try {
			JSONObject orderInfoObject = new JSONObject();
			orderInfoObject.put("UserId", user_id);
			orderInfoObject.put("ReceiveId", receive_id);
			orderInfoObject.put("UseScore", useScore);
			JSONArray orderDetailArray = new JSONArray();
			for (int i=0; i<products.size(); i++) {
				JSONObject productObject = new JSONObject();
				productObject.put("ProductID", products.get(i).getProduct_id());
				productObject.put("Num", products.get(i).getNum());
				orderDetailArray.put(productObject);
			}
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("OrderInfo", orderInfoObject);
			jsonObject.put("OrderDetail", orderDetailArray);
			data = jsonObject.toString();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		RequestParams params = new RequestParams();
		params.put(AppConstants.PARAMS_TAG_DATA, data);
		postRequest(url, params, listener);
	}
	
	/**
	 * 10.获得兑换订单信息列表
	 * @param user_id
	 * @param page
	 * 			page=0 获得所有的订单信息
	 *			page>0 获得某一页的订单信息
	 * @param listener
	 */
	public static void requestGetOrderList(int user_id, int page, AsyncResponseListener listener) {
		String url = API_URL_INTERFACE + "getOrderList";
		RequestParams params = new RequestParams();
		params.put(AppConstants.PARAMS_TAG_USER_ID, "" + user_id);
		params.put(AppConstants.PARAMS_TAG_PAGE, "" + page);
		postRequest(url, params, listener);
	}
	
	/**
	 * 11.获得兑换信息列表
	 * @param user_id
	 * @param page
	 * @param listener
	 */
	public static void requestGetExchangeList(int user_id, int page, AsyncResponseListener listener) {
		String url = API_URL_INTERFACE + "getExchangeList";
		RequestParams params = new RequestParams();
		params.put(AppConstants.PARAMS_TAG_USER_ID, "" + user_id);
		params.put(AppConstants.PARAMS_TAG_PAGE, "" + page);
		postRequest(url, params, listener);
	}
	
	/**
	 * 12.获取礼品商城商品的分类信息
	 * @param listener
	 */
	public static void requestGiftCategory(AsyncResponseListener listener) {
		String url = API_URL_INTERFACE + "giftCategory";
		RequestParams params = new RequestParams();
		postRequest(url, params, listener);
	}
	
	/**
	 * 13.获得礼品商城本月新品的商品列表
	 * @param listener
	 */
	public static void requestGetNewGiftList(AsyncResponseListener listener) {
		String url = API_URL_INTERFACE + "getNewGiftList";
		RequestParams params = new RequestParams();
		postRequest(url, params, listener);
	}
	
	/**
	 * 14.获得礼品商城华硕产品的商品列表
	 * @param listener
	 */
	public static void requestGetAsusGiftList(AsyncResponseListener listener) {
		String url = API_URL_INTERFACE + "getAsusGiftList";
		RequestParams params = new RequestParams();
		postRequest(url, params, listener);
	}
	
	/**
	 * 15.获得礼品商城首页的商品推荐列表
	 * @param listener
	 */
	public static void requestGetShopIndexList(AsyncResponseListener listener) {
		String url = API_URL_INTERFACE + "getShopIndexList";
		RequestParams params = new RequestParams();
		postRequest(url, params, listener);
	}
	
	/**
	 * 16.获得礼品商城某个分类下的商品列表
	 * @param category_id
	 * 			通过getCategory接口获得的商品分类id
	 * @param page
	 * 			page=0 获得所有的商品信息
	 *			page>0 获得某一页的商品信息
	 * @param listener
	 */
	public static void requestGiftList(int category_id, int page, AsyncResponseListener listener) {
		String url = API_URL_INTERFACE + "giftList";
		RequestParams params = new RequestParams();
		params.put(AppConstants.PARAMS_TAG_CATEGORY_ID, "" + category_id);
		params.put(AppConstants.PARAMS_TAG_PAGE, "" + page);
		postRequest(url, params, listener);
	}
	
	/**
	 * 17.获得礼品商城商品的详细信息
	 * @param gift_id
	 * 			商品的ID
	 * @param listener
	 */
	public static void requestGiftDetail(int gift_id, AsyncResponseListener listener) {
		String url = API_URL_INTERFACE + "giftDetail";
		RequestParams params = new RequestParams();
		params.put(AppConstants.PARAMS_TAG_GIFT_ID, "" + gift_id);
		postRequest(url, params, listener);
	}
	
	/**
	 * 18.获取库存产品的分类信息
	 * @param listener
	 */
	public static void requestProductCategory(AsyncResponseListener listener) {
		String url = API_URL_INTERFACE + "productCategory";
		RequestParams params = new RequestParams();
		postRequest(url, params, listener);
	}
	
	/**
	 * 19.获得某个分类下的库存产品列表
	 * @param category_id
	 * 			通过getCategory接口获得的商品分类id
	 * @param page
	 * 			page=0 获得所有的商品信息
	 *			page>0 获得某一页的商品信息
	 * @param listener
	 */
	public static void requestProductList(int category_id, int page, AsyncResponseListener listener) {
		String url = API_URL_INTERFACE + "productList";
		RequestParams params = new RequestParams();
		params.put(AppConstants.PARAMS_TAG_CATEGORY_ID, "" + category_id);
		params.put(AppConstants.PARAMS_TAG_PAGE, "" + page);
		postRequest(url, params, listener);
	}
	
	/**
	 * 20.获得栏目页上方图片
	 * @param listener
	 */
	public static void requestGetColumnPic(AsyncResponseListener listener) {
		String url = API_URL_INTERFACE + "getColumnPic";
		RequestParams params = new RequestParams();
		postRequest(url, params, listener);
	}
	
	/**
	 * 21.获得政策通知/技术分享列表信息
	 * @param cid
	 * 			政策通知cid=108
	 *			技术分享 cid=109
	 * @param page
	 * 			page=0 获得所有的列表内容
	 *			page>0 获得某一页的列表内容
	 * @param listener
	 */
	public static void requestArticleList(int cid, int page, AsyncResponseListener listener) {
		String url = API_URL_INTERFACE + "articleList";
		RequestParams params = new RequestParams();
		params.put(AppConstants.PARAMS_TAG_CID, "" + cid);
		params.put(AppConstants.PARAMS_TAG_PAGE, "" + page);
		postRequest(url, params, listener);
	}
	
	/**
	 * 22.获得政策通知/技术分享详细内容信息
	 * @param cid
	 * @param page
	 * @param listener
	 */
	public static void requestGetArticleContent(int cid, int id, AsyncResponseListener listener) {
		String url = API_URL_INTERFACE + "getArticleContent";
		RequestParams params = new RequestParams();
		params.put(AppConstants.PARAMS_TAG_CID, "" + cid);
		params.put(AppConstants.PARAMS_TAG_ID, "" + id);
		postRequest(url, params, listener);
	}
	
	/**
	 * 23.更新用户信息
	 * @param user_id
	 * @param name
	 * 			用户姓名
	 * @param pay_password
	 * 			支付密码，用于设置支付密码
	 * @param birthday
	 * @param mobile
	 * @param phone
	 * 			座机
	 * @param listener
	 */
	public static void requestUpdateUserInfo(int user_id, String name, String pay_password, String birthday,
			String mobile, String phone, AsyncResponseListener listener) {
		String url = API_URL_INTERFACE + "updateUserInfo";
		RequestParams params = new RequestParams();
		params.put(AppConstants.PARAMS_TAG_USER_ID, "" + user_id);
		params.put(AppConstants.PARAMS_TAG_NAME, name);
		params.put(AppConstants.PARAMS_TAG_PAY_PASSWORD, pay_password);
		params.put(AppConstants.PARAMS_TAG_BIRTHDAY, birthday);
		params.put(AppConstants.PARAMS_TAG_MOBILE, mobile);
		params.put(AppConstants.PARAMS_TAG_PHONE, phone);
		postRequest(url, params, listener);
	}
	
	/**
	 * 24.验证用户支付密码
	 * @param user_id
	 * @param pay_password
	 * @param listener
	 */
	public static void requestCheckPayPass(int user_id, String pay_password, AsyncResponseListener listener) {
		String url = API_URL_INTERFACE + "checkPayPass";
		RequestParams params = new RequestParams();
		params.put(AppConstants.PARAMS_TAG_USER_ID, "" + user_id);
		params.put(AppConstants.PARAMS_TAG_PAY_PASSWORD, pay_password);
		postRequest(url, params, listener);
	}
	
	/**
	 * 25.找回密码发送邮件
	 * @param email
	 * @param listener
	 */
	public static void requestReBackPass(String email, AsyncResponseListener listener) {
		String url = API_URL_INTERFACE + "reBackPass";
		RequestParams params = new RequestParams();
		params.put(AppConstants.PARAMS_TAG_EMAIL, email);
		postRequest(url, params, listener);
	}
	
	/**
	 * 26.商城搜索列表
	 * @param keyword
	 * @param listener
	 */
	public static void requestSearchGift(String keyword, AsyncResponseListener listener) {
		String url = API_URL_INTERFACE + "searchGift";
		RequestParams params = new RequestParams();
		params.put(AppConstants.PARAMS_TAG_KEYWORD, keyword);
		postRequest(url, params, listener);
	}
	
	/**
	 * 27.首页搜索
	 * @param keyword
	 * @param listener
	 */
	public static void requestSearchArticle(String keyword, AsyncResponseListener listener) {
		String url = API_URL_INTERFACE + "searchArticle";
		RequestParams params = new RequestParams();
		params.put(AppConstants.PARAMS_TAG_KEYWORD, keyword);
		postRequest(url, params, listener);
	}
	
	/**
	 * 28.用户反馈上报
	 * @param user_id
	 * @param username
	 * @param feedback
	 * 			反馈内容
	 * @param listener
	 */
	public static void requestUserFeedback(int user_id, String username, String feedback, AsyncResponseListener listener) {
		String url = API_URL_INTERFACE + "userFeedback";
		RequestParams params = new RequestParams();
		params.put(AppConstants.PARAMS_TAG_USER_ID, "" + user_id);
		params.put(AppConstants.PARAMS_TAG_USERNAME, username);
		params.put(AppConstants.PARAMS_TAG_FEEDBACK, feedback);
		postRequest(url, params, listener);
	}
	
	/**
	 * 29.删除收货地址信息
	 * @param user_id
	 * @param address_id
	 * @param listener
	 */
	public static void requestDeleteReceive(int user_id, int address_id, AsyncResponseListener listener) {
		String url = API_URL_INTERFACE + "deleteReceive";
		RequestParams params = new RequestParams();
		params.put(AppConstants.PARAMS_TAG_USER_ID, "" + user_id);
		params.put(AppConstants.PARAMS_TAG_ADDRESS_ID, "" + address_id);
		postRequest(url, params, listener);
	}
	
	/**
	 * 30.获得政策通知/技术分享分类信息
	 * @param cid
	 * 			政策通知 cid=108
	 *			技术分享 cid=109
	 * @param listener
	 */
	public static void requestGetColumnList(int cid, AsyncResponseListener listener) {
		String url = API_URL_INTERFACE + "getColumnList";
		RequestParams params = new RequestParams();
		params.put(AppConstants.PARAMS_TAG_CID, "" + cid);
		postRequest(url, params, listener);
	}
	
	/**
	 * 31.修改用户登录密码
	 * @param user_id
	 * @param old_pass
	 * @param new_pass
	 * @param listener
	 */
	public static void requestResetPassword(int user_id, String old_pass, String new_pass, AsyncResponseListener listener) {
		String url = API_URL_INTERFACE + "resetPassword";
		RequestParams params = new RequestParams();
		params.put(AppConstants.PARAMS_TAG_USER_ID, "" + user_id);
		params.put(AppConstants.PARAMS_TAG_OLD_PASS, old_pass);
		params.put(AppConstants.PARAMS_TAG_NEW_PASS, new_pass);
		postRequest(url, params, listener);
	}
	
	public interface AsyncResponseListener {
		
		void onSuccess(String data);
		
		void onFailure(String error);
	}
}
