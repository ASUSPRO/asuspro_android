package com.asuspro;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.DownloadListener;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.asuspro.base.BaseActivity;

public class WebViewActivity extends BaseActivity {

	public static final String ACTIVITY_NAME = "WebViewActivity";
	// 返回按钮
	private WebView wv;

	private String title = "";
	private String url = "";
	private String flagUrl = "";
	private WebSettings setting;
	private ProgressBar right_loading;

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// 获取Intent参数
		title = getIntent().getStringExtra("title");
		if (TextUtils.isEmpty(title))
			title = "AUSUPRO";
		url = getIntent().getStringExtra("url");
		if (TextUtils.isEmpty(url))
			url = "";

		headerTitle.setText(title);
		Log.i("InfoViewActivity", "title:" + title + ",url:" + url);
		// 初始化webview控件
		wv = (WebView) this.findViewById(R.id.webview);
		right_loading = (ProgressBar) findViewById(R.id.header_right_loading);
		right_loading.setVisibility(View.INVISIBLE);
		
		wv.clearCache(true);
		setting = wv.getSettings();
		setting.setJavaScriptEnabled(true); // 支持js脚本
		setting.setBuiltInZoomControls(true);
		setting.setCacheMode(MODE_APPEND);// 追加模式
		setting.setDefaultTextEncodingName("UTF-8");// 编码setting.setBuiltInZoomControls(true);
		setting.setUseWideViewPort(true);
		setting.setLoadWithOverviewMode(true);

		if (android.os.Build.VERSION.SDK_INT > 11) {
			setting.setDisplayZoomControls(false);
		}
		wv.requestFocus();

		wv.setWebViewClient(new WebViewClient() {
			@Override
			public void onReceivedError(WebView view, int errorCode,
					String description, String failingUrl) {
				super.onReceivedError(view, errorCode, description, failingUrl);
			}

			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				return super.shouldOverrideUrlLoading(view, url);
			}

			@Override
			public void onReceivedSslError(WebView view,
					SslErrorHandler handler, SslError error) {
				handler.proceed();
			}

		});

		wv.setWebChromeClient(new WebChromeClient() {
			@Override
			public void onProgressChanged(WebView view, int newProgress) {
				super.onProgressChanged(view, newProgress);
				right_loading.setVisibility(View.VISIBLE);
		        if(newProgress == 100) {
		        	right_loading.setVisibility(View.INVISIBLE);
		        }
			}

		});

		// 实现webview中下载功能
		wv.setDownloadListener(new DownloadListener() {
			@Override
			public void onDownloadStart(String url, String userAgent,
					String contentDisposition, String mimetype,
					long contentLength) {
				Uri uri = Uri.parse(url);
				Intent intent = new Intent(Intent.ACTION_VIEW, uri);
				startActivity(intent);
				finish();
			}
		});

		wv.loadUrl(url);
	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	@Override
	protected void onPause() {
		super.onPause();
	}

	@Override
	protected void onStop() {
		super.onStop();
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
			wv.stopLoading();
			if (wv.canGoBack() && !flagUrl.equals(url)) {
				wv.goBack();
			} else {
				WebViewActivity.this.finish();
			}
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

	@Override
	protected String getHeaderBarTitle() {
		// TODO Auto-generated method stub
		return title;
	}

	@Override
	protected String getHeaderBarLeftIcon() {
		// TODO Auto-generated method stub
		return super.getHeaderBarLeftIcon();
	}

	@Override
	protected String getHeaderBarRightIcon() {
		// TODO Auto-generated method stub
		return super.getHeaderBarRightIcon();
	}

	@Override
	protected boolean onClickLeftEvent() {
		wv.stopLoading();
		if (wv.canGoBack() && !flagUrl.equals(url)) {
			wv.goBack();
		} else {
			WebViewActivity.this.finish();
		}
		return super.onClickLeftEvent();
	}

	@Override
	protected boolean onClickRightEvent() {
		return super.onClickRightEvent();
	}

	@Override
	protected int getContentView() {
		return R.layout.activity_webview;
	}

}
