package com.asuspro;

import java.util.Timer;
import java.util.TimerTask;

import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.widget.ImageView;

import com.asuspro.base.BaseActivity;
import com.asuspro.dbhelper.DBManager;
import com.asuspro.model.UserInfo;

/**
 * 欢迎页 larry create on 2015-5-3
 */
public class SplashActivity extends BaseActivity {
	private ImageView dialog_image;
	private AnimationDrawable animationDrawable;
	private UserInfo userInfo;

	@Override
	protected int getContentView() {
		// TODO Auto-generated method stub
		return R.layout.activity_splash;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		dialog_image = (ImageView) findViewById(R.id.dialog_image);
		
		animationDrawable = (AnimationDrawable) dialog_image.getDrawable();
		
		if (animationDrawable != null) {
			animationDrawable.start();
		}
		
		userInfo = DBManager.getUserDBHandler().getLoginUser();
		
		new Timer().schedule(new TimerTask() {
			@Override
			public void run() {
				// TODO Auto-generated method stub
				if (animationDrawable != null) {
					animationDrawable.stop();
				}
				
				Intent intent = new Intent();
				
//				if(userInfo != null && userInfo.getStatus() == 1) {
//					intent.setClass(SplashActivity.this, MainActivity.class);
//				} else {
//				}
				intent.setClass(SplashActivity.this, LoginActivity.class);
				startActivity(intent);
				finish();
			}
		}, 2000);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

}
