package com.asuspro;

import java.io.IOException;

import android.annotation.TargetApi;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.provider.MediaStore.MediaColumns;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Toast;

import com.asuspro.base.BaseActivity;
import com.asuspro.config.AppConstants;
import com.asuspro.dbhelper.DBManager;
import com.asuspro.model.UserInfo;
import com.asuspro.tools.BitmapTool;
import com.asuspro.tools.FileTools;
import com.asuspro.tools.ToastFactory;
import com.edmodo.cropper.CropImageView;
import com.edmodo.cropper.cropwindow.edge.Edge;

/**
 * 头像裁剪
 * 
 */
public class ImageCutActivity extends BaseActivity implements OnClickListener {

	private static final int ROTATE_NINETY_DEGREES = 90;
	private static final int DEFAULT_ASPECT_RATIO_VALUES = 10;
	private CropImageView cropImageView;
	private String filePath; // 临时图片存放路径
	public static final int CAMERA = 1;
	public static final int ALBUM = 2;
	private int flag = 0;
	private float rate = 1.0f;

	private String type = "avatar";

	private UserInfo mCurrentUser;

	
	@Override
	protected int getContentView() {
		// TODO Auto-generated method stub
		return R.layout.kalagame_image_cut;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		type = getIntent().getStringExtra("type");

		if ("bindcard".equals(type)) {
			rate = 1.5f;
		} else {
			rate = 1.0f;
		}

		initView();
		mCurrentUser = DBManager.getUserDBHandler().getLoginUser();
		if (mCurrentUser == null) {
			finish();
		}

		initData();
	}

	private void initView() {
		cropImageView = (CropImageView) this
				.findViewById(R.id.kalagame_id_iv_cut_img);
		cropImageView.setAspectRatio(
				(int) (DEFAULT_ASPECT_RATIO_VALUES * rate),
				DEFAULT_ASPECT_RATIO_VALUES);
		cropImageView.setFixedAspectRatio(true);
		View roate = this.findViewById(R.id.kalagame_id_btn_cut_roate);
		View cut = this.findViewById(R.id.kalagame_id_btn_cut);
		View back = this.findViewById(R.id.kalagame_id_ib_cut_back);
		roate.setOnClickListener(this);
		cut.setOnClickListener(this);
		back.setOnClickListener(this);

	}

	private void initData() {
		// filePath = this.getFilesDir().toString() + File.separator
		// + "avatar.jpg";

		filePath = FileTools.getCacheDir()
				+ AppConstants.FILE_NAME_IMAGECUT_TEMP;

		Intent kalaIntent = getIntent();
		int optionAction = kalaIntent.getIntExtra("action", ALBUM);
		int widthScreen = this.getResources().getDisplayMetrics().widthPixels;
		int heightScreen = this.getResources().getDisplayMetrics().heightPixels;
		switch (optionAction) {
		case ALBUM:
			flag = 0;
			Uri uri = kalaIntent.getData();
			String path = getPicPathByUri(uri);
			ExifInterface exif;
			Bitmap gintama = null;
			try {
				gintama = /* BitmapTool. */reLoadBitmapSize(path, widthScreen,
						heightScreen);
				if (gintama != null) {
					exif = new ExifInterface(path);
					cropImageView.setImageBitmap(gintama, exif);
				} else {
					finish();
				}
			} catch (IOException e) {
				if (gintama != null && !gintama.isRecycled()) {
					gintama.recycle();
				}
				e.printStackTrace();
			}

			// cv.setImageURI(kalaIntent.getData());

			break;

		case CAMERA:
			flag = 1;
			path = FileTools.getCacheDir()
					+ AppConstants.FILE_NAME_CAMERA_TEMP;
			gintama = null;
			try {
				gintama = /* BitmapTool. */reLoadBitmapSize(path, widthScreen,
						heightScreen);
				if (gintama != null) {
					exif = new ExifInterface(path);
					cropImageView.setImageBitmap(gintama, exif);
				} else {
					finish();
				}
			} catch (IOException e) {
				if (gintama != null && !gintama.isRecycled()) {
					gintama.recycle();
				}
				e.printStackTrace();
			}
			break;
		default:
			break;
		}
	}

	public Bitmap reLoadBitmapSize(String filename, int width, int height) {
		Bitmap resizedBmp = null;
		try {
			int scale = 1;
			BitmapFactory.Options options = new BitmapFactory.Options();
			options.inJustDecodeBounds = true;
			try {
				BitmapFactory.decodeFile(filename, options);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			options.inSampleSize = 1; // inSample:1 表示不做重采样，图片大小不改变
			if (options.outHeight != 0
					&& options.outWidth != 0
					&& options.outHeight * options.outWidth < Edge.MIN_CROP_LENGTH_PX
							* Edge.MIN_CROP_LENGTH_PX) {
				ToastFactory.showToast(this.getApplicationContext(),
						"图片尺寸太小，请重新选择图片！", Toast.LENGTH_SHORT);
				return null;
			}
			float oriScale = options.outHeight * options.outWidth;
			if (oriScale != 0) {
				float pixScale = width * height * 2f;
				scale = (int) (oriScale / pixScale + 0.5f);
			}
			options.inJustDecodeBounds = false;
			options.inSampleSize = scale;

			resizedBmp = BitmapFactory.decodeFile(filename, options);
		} catch (OutOfMemoryError e) {
			ToastFactory.showToast(this.getApplicationContext(),
					"内存不足请重新选择图片！", Toast.LENGTH_SHORT);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return resizedBmp;
	}

	@TargetApi(19)
	private String getPicPathByUri(Uri uri) {
		String path = null;

		if (uri != null) {
			if (uri.toString().startsWith("file")) {
				path = uri.toString().substring(7);
			} else if (Build.VERSION.SDK_INT >= 19) {
				if (DocumentsContract.isDocumentUri(this, uri)) {
					String wholeID = DocumentsContract.getDocumentId(uri);
					String id = wholeID.split(":")[1];
					String[] column = { MediaStore.Images.Media.DATA };
					String sel = MediaStore.Images.Media._ID + "=?";
					Cursor cursor = getContentResolver().query(
							MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
							column, sel, new String[] { id }, null);
					int columnIndex = cursor.getColumnIndex(column[0]);
					if (cursor.moveToFirst()) {
						path = cursor.getString(columnIndex);
					}
					cursor.close();
				} else {
					String[] proj = { MediaColumns.DATA };
					Cursor cursor = this.getContentResolver().query(uri, proj,
							null, null, null);
					if (cursor != null) {
						int column_index = cursor
								.getColumnIndexOrThrow(MediaColumns.DATA);
						if (cursor.moveToFirst()) {
							path = cursor.getString(column_index);
						}
					} else {
						path = uri.toString();
					}
				}
			} else {
				String[] proj = { MediaColumns.DATA };
				Cursor cursor = this.getContentResolver().query(uri, proj,
						null, null, null);
				if (cursor != null) {
					int column_index = cursor
							.getColumnIndexOrThrow(MediaColumns.DATA);
					if (cursor.moveToFirst()) {
						path = cursor.getString(column_index);
					}
				} else {
					path = uri.toString();
				}

				// cursor.close();
			}
		}

		return path;
	}

	/*
	 * private void g(Context context, Uri contentUri){
	 * 
	 * if(DocumentsContract.isDocumentUri(context, contentUri)){ String wholeID
	 * = DocumentsContract.getDocumentId(contentUri); String id =
	 * wholeID.split(":")[1]; String[] column = { MediaStore.Images.Media.DATA
	 * }; String sel = MediaStore.Images.Media._ID + "=?"; Cursor cursor =
	 * context
	 * .getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
	 * column, sel, new String[] { id }, null); int columnIndex =
	 * cursor.getColumnIndex(column[0]); if (cursor.moveToFirst()) { filePath =
	 * cursor.getString(columnIndex); } cursor.close(); }else{
	 * 
	 * }
	 * 
	 * }
	 */
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.kalagame_id_ib_cut_back: // 返回
			finish();
			break;
		case R.id.kalagame_id_btn_cut_roate: // 旋转
			cropImageView.rotateImage(ROTATE_NINETY_DEGREES);
			break;
		case R.id.kalagame_id_btn_cut: // 裁剪
			final Bitmap croppedImage = cropImageView.getCroppedImage();

			Thread thread = new Thread(new Runnable() {
				@Override
				public void run() {

					if (croppedImage != null) {
						BitmapTool.creatJPGFromBitmap(croppedImage, filePath);
						if (!croppedImage.isRecycled()) {
							croppedImage.recycle();
						}
						handler.post(new Runnable() {

							@Override
							public void run() {
								hideWaitDialog();
								Intent data = new Intent();
								data.putExtra("image_path", filePath);
								setResult(RESULT_OK, data);
								finish();
							}
						});

					}

				}
			});
			thread.setDaemon(true);
			thread.start();

			break;
		}

	}

	private Handler handler = new Handler() {

		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case 100:
				// UserData.uploadUserIcon(ImageCutActivity.this, 100, new
				// File(filePath),
				// responseListener);
				break;
			}
			super.handleMessage(msg);
		}
	};


	// private MResponseListener responseListener = new MResponseListener() {
	//
	// @Override
	// public void onSuccess(int requestId, JSONObject objResult) {
	// hideWaitDialog();
	// Intent data = new Intent();
	// data.putExtra("image_path", filePath);
	// setResult(RESULT_OK, data);
	// finish();
	// }
	//
	// };

}
