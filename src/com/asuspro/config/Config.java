package com.asuspro.config;

import com.asuspro.tools.Tools;

/**
 * 全局常量信息
 * larry create on 2015-5-2
 */
public class Config {
	
	public static final String TYPEFACE_COMMON_PATH = "fonts/iconfont.ttf";

	public static String SDCARD_PATH = Tools.getSDPath() + "/asuspro/";

	public static String SDCARD_DOWNLOAD_FILE_PATH = SDCARD_PATH
			+ "download/file/";

	public static String SDCARD_PICTURE_PATH = SDCARD_PATH + "picture/";
	
	public final static String SDCARD_CACHE_PATH = SDCARD_PATH + "cache/";
	
	public final static String SDCARD_USER_PATH = SDCARD_PATH + "user/";
	
	public final static String userFolderNameAvatar = "avatar";

	/** 拍照图片前缀 */
	public static final String JPEG_FILE_PREFIX = "IMG_";
	/** 拍照图片后缀 */
	public static final String JPEG_FILE_SUFFIX = ".jpg";

	/**
	 * 设备信息
	 * 
	 * @author larry
	 */
	public static class DeviceInfo {

		public static int screenHeight = 0;

		public static int screenWidth = 0;

		public static int screenActionHeight = 0;

		public static float density = 0;

		public static float densityDpi = 0;

		public static float scaledDensity = 0.0f;

		public static final String sDefaultKeyHeight = "keyheight";
	}
	
	
}
