package com.asuspro.config;
/**
 * 常量类
 * larry create on 2015-6-8
 */
public class AppConstants {

	public static final String PARAMS_TAG_ACCOUNT = "account";
	public static final String PARAMS_TAG_ADDRESS = "address";
	public static final String PARAMS_TAG_ADDRESS_ID = "address_id";
	public static final String PARAMS_TAG_AREA = "area";
	public static final String PARAMS_TAG_BIRTHDAY = "birthday";
	public static final String PARAMS_TAG_CATEGORY_ID = "category_id";
	public static final String PARAMS_TAG_CID = "cid";
	public static final String PARAMS_TAG_CITY = "city";
	public static final String PARAMS_TAG_CITY_CODE = "city_code";
	public static final String PARAMS_TAG_DATA = "data";
	public static final String PARAMS_TAG_EMAIL = "email";
	public static final String PARAMS_TAG_FEEDBACK = "feedback";
	public static final String PARAMS_TAG_GIFT_ID = "gift_id";
	public static final String PARAMS_TAG_ID = "id";
	public static final String PARAMS_TAG_IS_DEFAULT = "is_default";
	public static final String PARAMS_TAG_KEYWORD = "keyword";
	public static final String PARAMS_TAG_MOBILE = "mobile";
	public static final String PARAMS_TAG_NAME = "name";
	public static final String PARAMS_TAG_NEW_PASS = "new_pass";
	public static final String PARAMS_TAG_OLD_PASS = "old_pass";
	public static final String PARAMS_TAG_PAGE = "page";
	public static final String PARAMS_TAG_PASSWORD = "password";
	public static final String PARAMS_TAG_PAY_PASSWORD = "pay_password";
	public static final String PARAMS_TAG_PHONE = "phone";
	public static final String PARAMS_TAG_PIC_URL = "pic_url";
	public static final String PARAMS_TAG_PROVINCE = "province";
	public static final String PARAMS_TAG_PROVINCE_CODE = "province_code";
	public static final String PARAMS_TAG_TOKEN = "token";
	public static final String PARAMS_TAG_USER_ID = "user_id";
	public static final String PARAMS_TAG_USERNAME = "username";
	public static final String PARAMS_TAG_ZIP = "zip";
	
	
	public static int CURRENT_USER_ID = 0;// 当前登录用户ID
	
	public static final String FILE_NAME_CAMERA_TEMP = "cameraTemp.jpg";
	
	public static final String FILE_NAME_IMAGECUT_TEMP = "imageCutTemp.jpg";
	
	public static class SharePreference {
		public static final String PREFERENCE_TAG_ISLOGIN = "is_login";
	}
	
}
