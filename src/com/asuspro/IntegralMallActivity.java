package com.asuspro;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.asuspro.adapter.IntegralMallAdapter;
import com.asuspro.adapter.IntegralMallCategoryAdapter;
import com.asuspro.base.App;
import com.asuspro.base.BaseActivity;
import com.asuspro.manager.HttpProxy;
import com.asuspro.manager.HttpProxy.AsyncResponseListener;
import com.asuspro.manager.JsonParser;
import com.asuspro.model.BaseResult;
import com.asuspro.model.CategoryBean;
import com.asuspro.model.GiftInfo;
import com.asuspro.tools.ToastFactory;

/**
 * 积分商城
 * larry create on 2015-6-12
 */
public class IntegralMallActivity extends BaseActivity {
	
	private TextView right2;
	private ListView mallListView;
	private RelativeLayout layer;
	private RelativeLayout menu;
	private ExpandableListView menuListView;
	private RelativeLayout integral_mall_search_layer;
	private TextView integral_mall_search_icon;
	private EditText integral_mall_search;
	
	private IntegralMallAdapter integralMallAdapter;
	private IntegralMallCategoryAdapter categoryAdapter;
	
	private List<CategoryBean> categorys = new ArrayList<CategoryBean>();

	@Override
	protected int getContentView() {
		// TODO Auto-generated method stub
		return R.layout.activity_integral_mall;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		initView();
		setListener();
		
		getGetAsusGiftList();
		getGiftCategory();
	}
	
	private void initView() {
		right2 = (TextView) findViewById(R.id.header_right2);
		mallListView = (ListView) findViewById(R.id.mListView);
		layer = (RelativeLayout) findViewById(R.id.integral_mall_layer);
		menu = (RelativeLayout) findViewById(R.id.integral_mall_menu);
		menuListView = (ExpandableListView) findViewById(R.id.integral_mall_menu_listview);
		integral_mall_search_layer = (RelativeLayout) findViewById(R.id.integral_mall_search_layer);
		integral_mall_search = (EditText) findViewById(R.id.integral_mall_search);
		integral_mall_search_icon = (TextView) findViewById(R.id.integral_mall_search_icon);
		
		right2.setTypeface(App.getInstance().getCommnonTypeface());
		right2.setText(Html.fromHtml("&#xe601"));
		integral_mall_search_icon.setTypeface(App.getInstance().getCommnonTypeface());
		integral_mall_search_icon.setText(Html.fromHtml("&#xe60a"));
		
		integralMallAdapter = new IntegralMallAdapter(this);
		mallListView.setAdapter(integralMallAdapter);
		
		menuListView.setGroupIndicator(null);
		menuListView.setVerticalScrollBarEnabled(false);
		categoryAdapter = new IntegralMallCategoryAdapter(this);
		menuListView.setAdapter(categoryAdapter);
		menuListView.expandGroup(2);
	}

	private void setListener() {
		layer.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(menuIsShown()) {
					hideMenu(new OnClickListener() {
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							
						}
					});
				}
			}
		});
		right2.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent();
				i.setClass(IntegralMallActivity.this, ShoppingCartActivity.class);
				startActivity(i);
			}
		});
		mallListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				Intent i = new Intent(IntegralMallActivity.this, IntegralMallDetailActivity.class);
				i.putExtra("id", ((GiftInfo)integralMallAdapter.getItem(position)).getId());
				startActivity(i);
			}
		});
		menuListView.setOnGroupClickListener(new OnGroupClickListener() {
			@Override
			public boolean onGroupClick(ExpandableListView parent, View v,
					int groupPosition, long id) {
				// TODO Auto-generated method stub
				
				Intent i = new Intent(IntegralMallActivity.this, IntegralMallTwoActivity.class);
				if(groupPosition == 0) {//本月新品
					i.putExtra("id", -10);
					startActivity(i);
					hideMenu(new OnClickListener() {
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
						}
					});
				} else if(groupPosition == 1) {// 华硕产品
					i.putExtra("id", -11);
					startActivity(i);
					hideMenu(new OnClickListener() {
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
						}
					});
				}
				
				return false;
			}
		});
		menuListView.setOnChildClickListener(new OnChildClickListener() {
			@Override
			public boolean onChildClick(ExpandableListView parent, View v,
					int groupPosition, int childPosition, long id) {
				// TODO Auto-generated method stub
				
				hideMenu(new OnClickListener() {
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
					}
				});
				
				if(groupPosition == 2) { //数码产品
				}
				Intent i = new Intent(IntegralMallActivity.this, IntegralMallTwoActivity.class);
				i.putExtra("id", (int)((CategoryBean)categoryAdapter.getChild(groupPosition, childPosition)).getColumn_id());
				startActivity(i);
				return false;
			}
		});
	}
	
	private void initGiftData(ArrayList<GiftInfo> info) {
		integralMallAdapter.setData(info);
		integralMallAdapter.notifyDataSetChanged();
	}
	
	private void initCategoryData(List<CategoryBean> info) {
		categoryAdapter.setChildData(info);
		categoryAdapter.notifyDataSetChanged();
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	protected String getHeaderBarTitle() {
		// TODO Auto-generated method stub
		return "积分商城";
	}

	@Override
	protected String getHeaderBarRightIcon() {
		// TODO Auto-generated method stub
		return "&#xe615";
	}

	@Override
	protected boolean onClickLeftEvent() {
		// TODO Auto-generated method stub
		finish();
		return super.onClickLeftEvent();
	}

	@Override
	protected boolean onClickRightEvent() {
		// TODO Auto-generated method stub
		if(!menuIsShown()) {
			showMenu();
		}
		return super.onClickRightEvent();
	}
	
	private boolean menuIsShown() {
		if(menu.getVisibility() == View.VISIBLE) {
			return true;
		}
		return false;
	}
	
	// 打开菜单
	private void showMenu() {
		layer.setVisibility(View.VISIBLE);
		menu.setVisibility(View.VISIBLE);
		TranslateAnimation translate = null;
		translate = new TranslateAnimation(1000f, 0, 0f, 0f);
		translate.setDuration(500);
		// translate.setStartOffset(200);
		translate.setFillAfter(true);
		translate.setInterpolator(new AccelerateDecelerateInterpolator());// 加速
		// translate.setInterpolator(new DecelerateInterpolator());//减速
		translate.setAnimationListener(new AnimationListener() {
			@Override
			public void onAnimationStart(Animation animation) {
			}

			@Override
			public void onAnimationRepeat(Animation animation) {
			}

			@Override
			public void onAnimationEnd(Animation animation) {
			}
		});
		menu.startAnimation(translate);
		// cancel.startAnimation(translate);

		AlphaAnimation alphaAnimation = new AlphaAnimation(0f, 1f);
		alphaAnimation.setDuration(500);
		alphaAnimation.setInterpolator(new AccelerateDecelerateInterpolator());
		alphaAnimation.setFillAfter(true);
		layer.startAnimation(alphaAnimation);
	}
	
	// 关闭菜单
	private void hideMenu(final OnClickListener l) {
		TranslateAnimation translate = null;
		translate = new TranslateAnimation(0, 1000f, 0f, 0f);
		translate.setDuration(500);
		// translate.setStartOffset(200);
		translate.setFillAfter(true);
		translate.setInterpolator(new DecelerateInterpolator());// 减速
		translate.setAnimationListener(new AnimationListener() {
			@Override
			public void onAnimationStart(Animation animation) {
			}

			@Override
			public void onAnimationRepeat(Animation animation) {
			}

			@Override
			public void onAnimationEnd(Animation animation) {
				// dialog.setVisibility(View.INVISIBLE);
				if (l != null) {
					layer.setVisibility(View.GONE);
					menu.setVisibility(View.GONE);
					l.onClick(null);
					menu.layout(0, 0, 1000, 0);
					layer.layout(0, 0, 0, 0);
					InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);  
					imm.hideSoftInputFromWindow(integral_mall_search.getWindowToken(), 0);
				}
			}
		});
		menu.startAnimation(translate);
		// cancel.setAnimation(translate);

		AlphaAnimation alphaAnimation = new AlphaAnimation(1f, 0f);
		alphaAnimation.setDuration(500);
		alphaAnimation.setInterpolator(new DecelerateInterpolator());
		alphaAnimation.setFillAfter(true);
		layer.startAnimation(alphaAnimation);
	}

	// 获取首页推荐商品列表
	private void getGetAsusGiftList() {
		showWaitDialog();
		HttpProxy.requestGetShopIndexList(new AsyncResponseListener() {
			
			@Override
			public void onSuccess(String data) {
				// TODO Auto-generated method stub
				Log.d("larry", "" + data);
				hideWaitDialog();
				BaseResult result = JsonParser.parserShopIndexList(data);
				if(result.getResultCode() == 0) {
					try {
						ArrayList<GiftInfo> info = (ArrayList<GiftInfo>) result.getResultContent();
						initGiftData(info);
					} catch (Exception e) {
						e.printStackTrace();
					}
				} else {
					ToastFactory.showToast(IntegralMallActivity.this, result.getResultMsg(), Toast.LENGTH_SHORT);
				}
			}
			
			@Override
			public void onFailure(String error) {
				// TODO Auto-generated method stub
				hideWaitDialog();
				ToastFactory.showToast(IntegralMallActivity.this, error, Toast.LENGTH_SHORT);
			}
		});
	}
	
	// 获取礼品商城商品的分类信息
	private void getGiftCategory() {
		HttpProxy.requestGiftCategory(new AsyncResponseListener() {
			
			@Override
			public void onSuccess(String data) {
				// TODO Auto-generated method stub
				BaseResult result = JsonParser.parserProductCategory(data);
				if(result.getResultCode() == 0) {
					try {
						List<CategoryBean> info = (List<CategoryBean>) result.getResultContent();
						initCategoryData(info);
					} catch (Exception e) {
						e.printStackTrace();
					}
				} else {
					ToastFactory.showToast(IntegralMallActivity.this, result.getResultMsg(), Toast.LENGTH_SHORT);
				}
			}
			
			@Override
			public void onFailure(String error) {
				// TODO Auto-generated method stub
//				ToastFactory.showToast(IntegralMallActivity.this, error, Toast.LENGTH_SHORT);
			}
		});
	}
	
	
}
