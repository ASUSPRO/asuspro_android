package com.asuspro;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.asuspro.adapter.AddressAdapter;
import com.asuspro.base.BaseActivity;
import com.asuspro.dbhelper.DBManager;
import com.asuspro.manager.HttpProxy;
import com.asuspro.manager.HttpProxy.AsyncResponseListener;
import com.asuspro.manager.JsonParser;
import com.asuspro.model.AddressInfo;
import com.asuspro.model.BaseResult;
import com.asuspro.model.UserInfo;
import com.asuspro.tools.ToastFactory;

/**
 * 地址列表 larry create on 2015-8-6
 */
public class AddressListActivity extends BaseActivity {
	
	private UserInfo mCurrentUser;
	
	private AddressAdapter mAdapter;
	
	private List<AddressInfo> addressInfos = new ArrayList<AddressInfo>();
	
	private ListView mListView;
	
	private String from = "";

	@Override
	protected int getContentView() {
		// TODO Auto-generated method stub
		return R.layout.activity_address_list;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		from = getIntent().getStringExtra("from");
		initView();
		initData();
		requestAddressData();
	}
	
	private void initView() {
		mListView = (ListView) findViewById(R.id.addresslist_listview);
		
		View footer = LayoutInflater.from(this).inflate(R.layout.addresslist_footer, null);
		TextView textview = (TextView) footer.findViewById(R.id.addresslist_footer_textview);
		textview.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// 新增地址
				Intent i = new Intent();
				i.setClass(AddressListActivity.this, EditAddressActivity.class);
				i.putExtra("title", "新增地址");
				i.putExtra("type", 0);
				startActivity(i);
			}
		});
		mListView.addFooterView(footer);
		mAdapter = new AddressAdapter(this);
		mListView.setAdapter(mAdapter);
		
		
		mListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				if("exchange".equals(from)) {
					AddressInfo mDefault = DBManager.getAddressDBHandler().getDefaultAddress();
					mDefault.setIs_default(0);
					DBManager.getAddressDBHandler().updateAddressInfo(mDefault);
					
					AddressInfo mCurrent = DBManager.getAddressDBHandler().getAddressInfoById(addressInfos.get(position).getId());
					mCurrent.setIs_default(1);
					DBManager.getAddressDBHandler().updateAddressInfo(mCurrent);
					
					setResult(RESULT_OK);
					finish();
				} else {
					Intent i = new Intent();
					i.setClass(AddressListActivity.this, EditAddressActivity.class);
					i.putExtra("title", "编辑地址");
					i.putExtra("address", addressInfos.get(position));
					i.putExtra("type", 1);
					startActivity(i);
				}
			}
		});
		
	}

	private void initData() {
		mCurrentUser = DBManager.getUserDBHandler().getLoginUser();
		if(mCurrentUser == null) {
			Intent i = new Intent();
			i.setClass(AddressListActivity.this, LoginActivity.class);
			i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(i);
			return;
		}
		
		List<AddressInfo> mAddressInfos = DBManager.getAddressDBHandler().getAddressInfos();
		mAdapter.setData(mAddressInfos);
		addressInfos.clear();
		addressInfos.addAll(mAddressInfos);
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	protected String getHeaderBarTitle() {
		// TODO Auto-generated method stub
		return "地址";
	}

	@Override
	protected String getHeaderBarRightIcon() {
		// TODO Auto-generated method stub
		return super.getHeaderBarRightIcon();
	}

	@Override
	protected boolean onClickLeftEvent() {
		// TODO Auto-generated method stub
		finish();
		return super.onClickLeftEvent();
	}
	
	private void requestAddressData(){
		showWaitDialog();
		HttpProxy.requestGetReceive(mCurrentUser.getId(), new AsyncResponseListener() {
			
			@Override
			public void onSuccess(String data) {
				// TODO Auto-generated method stub
				hideWaitDialog();
				Log.d("larry", "----data--- : " + data);
				BaseResult result = JsonParser.parserAddressList(data);
				if(0 == result.getResultCode()) {
					List<AddressInfo> mAddressInfos = (List<AddressInfo>) result.getResultContent();
					
					mAdapter.setData(mAddressInfos);
					addressInfos.clear();
					addressInfos.addAll(mAddressInfos);
					
					for(AddressInfo newAddress : mAddressInfos) {
						AddressInfo oldAddress = DBManager.getAddressDBHandler().getAddressInfoById(newAddress.getId());
						if(oldAddress != null) {
							DBManager.getAddressDBHandler().updateAddressInfo(newAddress);
						} else {
							DBManager.getAddressDBHandler().addAddressInfo(newAddress);
						}
					}
					
				} else {
					ToastFactory.showToast(AddressListActivity.this, result.getResultMsg(), Toast.LENGTH_SHORT);
				}
			}
			
			@Override
			public void onFailure(String error) {
				// TODO Auto-generated method stub
				hideWaitDialog();
				ToastFactory.showToast(AddressListActivity.this, error, Toast.LENGTH_SHORT);
			}
		});
	}

}
