package com.asuspro;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.text.Html;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import cn.trinea.android.view.autoscrollviewpager.AutoScrollViewPager;

import com.asuspro.adapter.ImagePagerAdapter;
import com.asuspro.base.App;
import com.asuspro.dbhelper.DBManager;
import com.asuspro.manager.HttpProxy;
import com.asuspro.manager.HttpProxy.AsyncResponseListener;
import com.asuspro.manager.JsonParser;
import com.asuspro.model.AdvertisementInfo;
import com.asuspro.model.BaseResult;
import com.asuspro.model.UserInfo;
import com.asuspro.tools.ListUtils;
import com.asuspro.tools.ToastFactory;
import com.asuspro.view.CircleImageView;

public class MainActivity extends Activity {
	private AutoScrollViewPager viewPager;
	private LinearLayout auto_scroll_dot_layout;
	private RelativeLayout main_store, main_notice, main_accessories, main_share;
	private TextView store_icon, notice_icon, accessories_icon, share_icon;
	private TextView main_search_icon, main_search_right;
	private CircleImageView main_avatar;
	
	private List<AdvertisementInfo> imageIdList = new ArrayList<AdvertisementInfo>();
	private ImagePagerAdapter imagePagerAdapter;
//	private SmartImageView image;
	private UserInfo mCurrentUser;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	
		initView();
		
		setListener();
		
		getPicture();
		
		initData();
		
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		viewPager.startAutoScroll();
		super.onResume();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		viewPager.stopAutoScroll();
		super.onPause();
	}

	private void initView() {
		main_avatar = (CircleImageView) findViewById(R.id.main_avatar);
		viewPager = (AutoScrollViewPager) findViewById(R.id.main_images);
		auto_scroll_dot_layout = (LinearLayout) findViewById(R.id.auto_scroll_dot_layout);
		
		main_store = (RelativeLayout) findViewById(R.id.main_store);
		main_notice = (RelativeLayout) findViewById(R.id.main_notice);
		main_accessories = (RelativeLayout) findViewById(R.id.main_accessories);
		main_share = (RelativeLayout) findViewById(R.id.main_share);
		
		store_icon = (TextView) findViewById(R.id.main_store_icon);
		notice_icon = (TextView) findViewById(R.id.main_notice_icon);
		accessories_icon = (TextView) findViewById(R.id.main_accessories_icon);
		share_icon = (TextView) findViewById(R.id.main_share_icon);
		
		main_search_icon = (TextView) findViewById(R.id.main_search_icon);
		main_search_right = (TextView) findViewById(R.id.main_search_right);
		main_search_icon.setTypeface(App.getInstance().getCommnonTypeface());
		main_search_right.setTypeface(App.getInstance().getCommnonTypeface());
		main_search_icon.setText(Html.fromHtml("&#xe60a"));
		main_search_right.setText(Html.fromHtml("&#xe602"));
		
		store_icon.setTypeface(App.getInstance().getCommnonTypeface());
		notice_icon.setTypeface(App.getInstance().getCommnonTypeface());
		accessories_icon.setTypeface(App.getInstance().getCommnonTypeface());
		share_icon.setTypeface(App.getInstance().getCommnonTypeface());
		store_icon.setText(Html.fromHtml("&#xe60c"));
		notice_icon.setText(Html.fromHtml("&#xe60e"));
		accessories_icon.setText(Html.fromHtml("&#xe606"));
		share_icon.setText(Html.fromHtml("&#xe60f"));
		
//		image = (SmartImageView) findViewById(R.id.main_image);
		
		imagePagerAdapter = new ImagePagerAdapter(this).setInfiniteLoop(true);
        imagePagerAdapter.setData(imageIdList);
        viewPager.setAdapter(imagePagerAdapter);
        viewPager.setOnPageChangeListener(new MyOnPageChangeListener());
        
        initViewPageData(null);
	}
	
	private void initViewPageData(List<AdvertisementInfo> data) {
		if(data == null || data.size() == 0) {
			imageIdList.add(null);
		} else {
			imageIdList.clear();
			imageIdList.addAll(data);
			imagePagerAdapter.setData(imageIdList);
			imagePagerAdapter.notifyDataSetChanged();
		}
        
		initRadio(imageIdList.size());
        
        viewPager.setInterval(5000);
        viewPager.startAutoScroll();
	}
	
	private void setListener() {
		main_store.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent store = new Intent();
				store.setClass(MainActivity.this, IntegralMallActivity.class);
				startActivity(store);
			}
		});
		main_notice.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent noticeIntent = new Intent(MainActivity.this, NoticeActivity.class);
				noticeIntent.putExtra("cid", 108);
				startActivity(noticeIntent);
			}
		});
		main_accessories.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent accessoriesIntent = new Intent(MainActivity.this, AccessoriesActivity.class);
				startActivity(accessoriesIntent);
			}
		});
		main_share.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent shareIntent = new Intent(MainActivity.this, NoticeActivity.class);
				shareIntent.putExtra("cid", 109);
				startActivity(shareIntent);
			}
		});
		main_avatar.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent mineInfo = new Intent(MainActivity.this, MineActivity.class);
				startActivity(mineInfo);
			}
		});
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		if(keyCode == KeyEvent.KEYCODE_BACK) {
//			android.os.Process.killProcess(android.os.Process.myPid());    //获取PID
//			System.exit(0);
			setResult(RESULT_OK);
			finish();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	private void initData() {
		mCurrentUser = DBManager.getUserDBHandler().getLoginUser();
		if(mCurrentUser != null) {
			try {
				main_avatar.setImageUrl(mCurrentUser.getPic_url(), 0, false);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	protected void initRadio(int total) {
		if (total < 2) {
			auto_scroll_dot_layout.setVisibility(View.GONE);			
			return;
		}
		auto_scroll_dot_layout.setVisibility(View.VISIBLE);
		for (int i = 0; i < total; i++) {
			ImageView img = new ImageView(this);
			LinearLayout.LayoutParams rl = new LinearLayout.LayoutParams(
					RelativeLayout.LayoutParams.WRAP_CONTENT,
					RelativeLayout.LayoutParams.WRAP_CONTENT);
			rl.setMargins(5, 0, 5, 0);
			if (i == 0) {
				img.setImageResource(R.drawable.dot01);
			} else {
				img.setImageResource(R.drawable.dot02);
			}
			auto_scroll_dot_layout.addView(img, i, rl);
		}
	}
	
	protected void setRadioProminent(int curr, int total) {
		if (total < 2)
			return;
		for (int i = 0; i < total; i++) {
			if (i == curr) {
				((ImageView) auto_scroll_dot_layout.getChildAt(i))
						.setImageResource(R.drawable.dot01);
			} else {
				((ImageView) auto_scroll_dot_layout.getChildAt(i))
						.setImageResource(R.drawable.dot02);
			}
		}
	}
	
	public class MyOnPageChangeListener implements OnPageChangeListener {

        @Override
        public void onPageSelected(int position) {
        	
        	setRadioProminent((position) % ListUtils.getSize(imageIdList), ListUtils.getSize(imageIdList));
        }

        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}

        @Override
        public void onPageScrollStateChanged(int arg0) {}
    }
	
	/**
	 * 获取轮播图片
	 */
	private void getPicture() {
		HttpProxy.requestGetColumnPic(new AsyncResponseListener() {
			@Override
			public void onSuccess(String data) {
				// TODO Auto-generated method stub
				Log.d("larry", "content " + data);
				BaseResult result = JsonParser.parserColumnPic(data);
				if(result.getResultCode() == 0) {
					try {
						List<AdvertisementInfo> info = (List<AdvertisementInfo>) result.getResultContent();
						initViewPageData(info);
//						image.setImageUrl(info.getImage().getOriginalURL(), null , true);
					} catch (Exception e) {
						e.printStackTrace();
					}
				} else {
					ToastFactory.showToast(MainActivity.this, result.getResultMsg(), Toast.LENGTH_SHORT);
				}
			}
			
			@Override
			public void onFailure(String error) {
				// TODO Auto-generated method stub
				ToastFactory.showToast(MainActivity.this, error, Toast.LENGTH_SHORT);
			}
		});
	}

}
