package com.asuspro.dbhelper;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import android.text.TextUtils;
import android.util.Log;

import com.asuspro.model.AddressInfo;

/**
 * 地址表 larry create on 2015-8-25
 */
public class AddressDao implements IAddressDao {

	private Context mContext;
	private static final String[] TABLE_COLUMNS;
	private static final String TABLE_NAME = "address";
	private static final String DATABASE_NAME = "address.db";
	private static final int DATABASE_VERSION = 1; // TODO data base version

	private AddressSqliteHelper mSqliteHelper = null;

	/*
	 * private int id; 
	 * private int user_id; 			// 用户ID 
	 * private String username; 		// 用户姓名 
	 * private String province; 		// 省份 
	 * private String city; 			// 城市 
	 * private String area; 			// 地区 
	 * private String address; 			// 收货地址 
	 * private String zip; 				// 邮编 
	 * private String mobile; 			// 收货人电话 
	 * private int is_default; 			// 1为默认收货地址，0为非默认(non-Javadoc)
	 */
	
	private static final String CREATE_SQL = "CREATE TABLE " + TABLE_NAME
			+ " (" + "id INTEGER PRIMARY KEY, " + "user_id TEXT, " + "username TEXT, " + "province TEXT, "
			+ "city TEXT, " + "area TEXT, " + "address TEXT, " + "zip TEXT, "
			+ "mobile TEXT, " + "is_default INTEGER" + ")";
	
	private static final String ADD_ONE_ADDRESS = "INSERT INTO " + TABLE_NAME
			+ " (id, user_id, username, province, city, area, address, zip, mobile, is_default) VALUES "
			+ " (?,?,?,?,?,?,?,?,?,?)";
	
	private static final String DELETE_ADDRESS = "DROP TABLE addresses";
	
	static {
		String[] columnStrings = new String[10];
		columnStrings[0] = "id";
        columnStrings[1] = "user_id";
        columnStrings[2] = "username";
        columnStrings[3] = "province";
        columnStrings[4] = "city";
        columnStrings[5] = "area";
        columnStrings[6] = "address";
        columnStrings[7] = "zip";
        columnStrings[8] = "mobile";
        columnStrings[9] = "is_default";
		TABLE_COLUMNS = columnStrings;
	}

	public AddressDao(Context mContext) {
		// TODO Auto-generated constructor stub
		this.mContext = mContext;
	}
	
	public class AddressSqliteHelper extends SQLiteOpenHelper {
		public AddressSqliteHelper(Context context) {
			// TODO Auto-generated constructor stub
			super(context, DATABASE_NAME, null, DATABASE_VERSION);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			// TODO Auto-generated method stub
			db.execSQL(CREATE_SQL);
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			// TODO Auto-generated method stub

		}

	}
	
	private void openConnection() {
		mSqliteHelper = new AddressSqliteHelper(this.mContext);
	}

	private void closeConnection() {
		mSqliteHelper = null;
	}
	
	@Override
	public int addAddressInfo(AddressInfo address) {
		// TODO Auto-generated method stub
		int res = 0;
		 SQLiteDatabase db = null;
		 try {
			openConnection();

           db = mSqliteHelper.getWritableDatabase();
           db.beginTransaction();
//	            db.execSQL("delete from users");
           SQLiteStatement statement = db.compileStatement(ADD_ONE_ADDRESS);
          
           DBTools.bindLong(statement, 1, address.getId());
           DBTools.bindLong(statement, 2, address.getUser_id());
           DBTools.bindString(statement, 3, !TextUtils.isEmpty(address.getUsername()) ? address.getUsername() : "");
           DBTools.bindString(statement, 4, !TextUtils.isEmpty(address.getProvince()) ? address.getProvince() : "");
           DBTools.bindString(statement, 5, TextUtils.isEmpty(address.getCity())? address.getCity() : "");
           DBTools.bindString(statement, 6, !TextUtils.isEmpty(address.getArea()) ? address.getArea() : "");
           DBTools.bindString(statement, 7, !TextUtils.isEmpty(address.getAddress()) ? address.getAddress() : "");
           DBTools.bindString(statement, 8, !TextUtils.isEmpty(address.getZip()) ? address.getZip() : "");
           DBTools.bindString(statement, 9, !TextUtils.isEmpty(address.getMobile()) ? address.getMobile() : "");
           DBTools.bindLong(statement, 10, address.getIs_default());
           
           long rowid = statement.executeInsert();
           if (rowid == -1) {
               res = -1;
           }
           db.setTransactionSuccessful();
           Log.d("larry","addAddressInfo-->rowid-=" + rowid);
	            
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
           if (db != null) {
               db.endTransaction();
               db.close();
           }
           closeConnection();
       }
		return res;
	}
	
	@Override
	public synchronized int updateAddressInfo(AddressInfo address) {
		// TODO Auto-generated method stub
		if (address == null) {
    		return -1;
    	}
        int res = 0;
        SQLiteDatabase db = null;
        try {
        	openConnection();

            db = mSqliteHelper.getWritableDatabase();
            db.beginTransaction();
            
            ContentValues updatedValues = new ContentValues();
            /*
           	 * private int id; 
           	 * private int user_id; 			// 用户ID 
           	 * private String username; 		// 用户姓名 
           	 * private String province; 		// 省份 
           	 * private String city; 			// 城市 
           	 * private String area; 			// 地区 
           	 * private String address; 			// 收货地址 
           	 * private String zip; 				// 邮编 
           	 * private String mobile; 			// 收货人电话 
           	 * private int is_default; 			// 1为默认收货地址，0为非默认(non-Javadoc)
           	 */
            
            int userId = address.getUser_id();
            updatedValues.put(TABLE_COLUMNS[1], userId);
            String username = address.getUsername();
            if (!TextUtils.isEmpty(username)) {
            	updatedValues.put(TABLE_COLUMNS[2], username);
            }
            String province = address.getProvince();
            if(!TextUtils.isEmpty(province)) {
            	updatedValues.put(TABLE_COLUMNS[3], province);
            }
            String city = address.getCity();
            if(!TextUtils.isEmpty(city)) {
            	updatedValues.put(TABLE_COLUMNS[4], city);
            }
            String area = address.getArea();
            if(TextUtils.isEmpty(area)) {
            	updatedValues.put(TABLE_COLUMNS[5], area);
            }
            String addr = address.getAddress();
            if(!TextUtils.isEmpty(addr)) {
            	updatedValues.put(TABLE_COLUMNS[6], addr);
            }
            String zip = address.getZip();
            if(!TextUtils.isEmpty(zip)) {
            	updatedValues.put(TABLE_COLUMNS[7], zip);
            }
            String mobile = address.getMobile();
            if(!TextUtils.isEmpty(mobile)) {
            	updatedValues.put(TABLE_COLUMNS[8], mobile);
            }
            long is_default = address.getIs_default();
            updatedValues.put(TABLE_COLUMNS[9], is_default);
            
            String where = TABLE_COLUMNS[0] + "=" + address.getId();
            // Update the row with the specified index with the new values.
            int rows = db.update(TABLE_NAME, updatedValues, where, null);
            Log.d("larry","updateaddressInfo-->rowsid-=" + rows + "address.getUser_id()==" + address.getUser_id());
            // Only rows == 0 is ok
            if (rows != 0) {
                res = -1;
            }
            db.setTransactionSuccessful();
            
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			if (db != null) {
                db.endTransaction();
                db.close();
            }
            closeConnection();
		}
		return res;
	}
	
	@Override
	public int updateAddressInfos(List<AddressInfo> addresses) {
		// TODO Auto-generated method stub
		if (addresses == null || addresses.size() <= 0) {
    		return -1;
    	}
        int res = 0;
        SQLiteDatabase db = null;
        try {
        	openConnection();

            db = mSqliteHelper.getWritableDatabase();
            db.beginTransaction();
            
            for(AddressInfo address : addresses) {
            	ContentValues updatedValues = new ContentValues();
            	/*
            	 * private int id; 
            	 * private int user_id; 			// 用户ID 
            	 * private String username; 		// 用户姓名 
            	 * private String province; 		// 省份 
            	 * private String city; 			// 城市 
            	 * private String area; 			// 地区 
            	 * private String address; 			// 收货地址 
            	 * private String zip; 				// 邮编 
            	 * private String mobile; 			// 收货人电话 
            	 * private int is_default; 			// 1为默认收货地址，0为非默认(non-Javadoc)
            	 */
            	
            	int id = address.getId();
            	updatedValues.put(TABLE_COLUMNS[0], id);
            	String username = address.getUsername();
            	if (!TextUtils.isEmpty(username)) {
            		updatedValues.put(TABLE_COLUMNS[2], username);
            	}
            	String province = address.getProvince();
            	if(!TextUtils.isEmpty(province)) {
            		updatedValues.put(TABLE_COLUMNS[3], province);
            	}
            	String city = address.getCity();
            	if(!TextUtils.isEmpty(city)) {
            		updatedValues.put(TABLE_COLUMNS[4], city);
            	}
            	String area = address.getArea();
            	if(TextUtils.isEmpty(area)) {
            		updatedValues.put(TABLE_COLUMNS[5], area);
            	}
            	String addr = address.getAddress();
            	if(!TextUtils.isEmpty(addr)) {
            		updatedValues.put(TABLE_COLUMNS[6], addr);
            	}
            	String zip = address.getZip();
            	if(!TextUtils.isEmpty(zip)) {
            		updatedValues.put(TABLE_COLUMNS[7], zip);
            	}
            	String mobile = address.getMobile();
            	if(!TextUtils.isEmpty(mobile)) {
            		updatedValues.put(TABLE_COLUMNS[8], mobile);
            	}
            	long is_default = address.getIs_default();
            	updatedValues.put(TABLE_COLUMNS[9], is_default);
            	
            	String where = TABLE_COLUMNS[1] + "=" + address.getUser_id();
            	// Update the row with the specified index with the new values.
            	int rows = db.update(TABLE_NAME, updatedValues, where, null);
            	Log.d("larry","updateaddressInfo-->rowsid-=" + rows + "address.getUser_id()==" + address.getUser_id());
            	// Only rows == 0 is ok
            	if (rows != 0) {
            		res = -1;
            	}
            	
            }
            db.setTransactionSuccessful();
            
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			if (db != null) {
                db.endTransaction();
                db.close();
            }
            closeConnection();
		}
		return res;
	}
	
	@Override
	public AddressInfo getDefaultAddress() {
		// TODO Auto-generated method stub
		AddressInfo address = null;
        SQLiteDatabase dbInstance = null;
        Cursor cursor = null;

        try {
            openConnection();
            dbInstance = mSqliteHelper.getReadableDatabase();
            dbInstance.beginTransaction();
            cursor = dbInstance.query(TABLE_NAME, TABLE_COLUMNS,
                    TABLE_COLUMNS[9] + "='" + 1 + "'", null, null, null, null);
            if (cursor != null && cursor.getCount() != 0) {
                cursor.moveToFirst();
                address = extractAnAddress(cursor);
            }
            dbInstance.setTransactionSuccessful();
        } catch (Exception e) {
            throw new SQLiteException(e.toString());
        } finally {
            // close book data base file
            if (dbInstance != null) {
                dbInstance.endTransaction();
                dbInstance.close();
            }
            if (cursor != null) {
                cursor.close();
            }
            closeConnection();
        }
        return address;
	}

	@Override
	public List<AddressInfo> getAddressInfos() {
		// TODO Auto-generated method stub
		List<AddressInfo> list = new ArrayList<AddressInfo>();
        SQLiteDatabase dbInstance = null;
        Cursor cursor = null;

        try {
            openConnection();
            dbInstance = mSqliteHelper.getReadableDatabase();
            dbInstance.beginTransaction();
            cursor = dbInstance.query(TABLE_NAME, TABLE_COLUMNS,
                    null, null, null, null, null);
            
            if (cursor != null) {
                cursor.moveToFirst();
                while (cursor != null && cursor.getCount() != 0
                        && !cursor.isAfterLast()) {
                	AddressInfo address = extractAnAddress(cursor);
                    list.add(address);
                    cursor.moveToNext();
                }
            }
            
            dbInstance.setTransactionSuccessful();
        } catch (Exception e) {
            throw new SQLiteException(e.toString());
        } finally {
            // close book data base file
            if (dbInstance != null) {
                dbInstance.endTransaction();
                dbInstance.close();
            }
            if (cursor != null) {
                cursor.close();
            }
            closeConnection();
        }
        return list;
	}
	
	 /*
   	 * private int id; 
   	 * private int user_id; 			// 用户ID 
   	 * private String username; 		// 用户姓名 
   	 * private String province; 		// 省份 
   	 * private String city; 			// 城市 
   	 * private String area; 			// 地区 
   	 * private String address; 			// 收货地址 
   	 * private String zip; 				// 邮编 
   	 * private String mobile; 			// 收货人电话 
   	 * private int is_default; 			// 1为默认收货地址，0为非默认(non-Javadoc)
   	 */
	private AddressInfo extractAnAddress(Cursor cursor) {
		AddressInfo address = new AddressInfo();
		address.setId(cursor.getInt(cursor.getColumnIndex(TABLE_COLUMNS[0])));
		address.setUser_id(cursor.getInt(cursor.getColumnIndex(TABLE_COLUMNS[1])));
		address.setUsername(cursor.getString(cursor.getColumnIndex(TABLE_COLUMNS[2])));
		address.setProvince(cursor.getString(cursor.getColumnIndex(TABLE_COLUMNS[3])));
		address.setCity(cursor.getString(cursor.getColumnIndex(TABLE_COLUMNS[4])));
		address.setArea(cursor.getString(cursor.getColumnIndex(TABLE_COLUMNS[5])));
		address.setAddress(cursor.getString(cursor.getColumnIndex(TABLE_COLUMNS[6])));
		address.setZip(cursor.getString(cursor.getColumnIndex(TABLE_COLUMNS[7])));
		address.setMobile(cursor.getString(cursor.getColumnIndex(TABLE_COLUMNS[8])));
		address.setIs_default(cursor.getInt(cursor.getColumnIndex(TABLE_COLUMNS[9])));
		return address;
	}

	@Override
	public AddressInfo getAddressInfoById(int id) {
		// TODO Auto-generated method stub
		AddressInfo address = null;
        SQLiteDatabase dbInstance = null;
        Cursor cursor = null;

        try {
            openConnection();
            dbInstance = mSqliteHelper.getReadableDatabase();
            dbInstance.beginTransaction();
            cursor = dbInstance.query(TABLE_NAME, TABLE_COLUMNS,
                    TABLE_COLUMNS[0] + "='" + id + "'", null, null, null, null);
            if (cursor != null && cursor.getCount() != 0) {
                cursor.moveToFirst();
                address = extractAnAddress(cursor);
            }
            dbInstance.setTransactionSuccessful();
        } catch (Exception e) {
            throw new SQLiteException(e.toString());
        } finally {
            // close book data base file
            if (dbInstance != null) {
                dbInstance.endTransaction();
                dbInstance.close();
            }
            if (cursor != null) {
                cursor.close();
            }
            closeConnection();
        }
        return address;
	}


}
