package com.asuspro.dbhelper;

import java.util.List;

import com.asuspro.model.ProductInfo;

/**
 * 购物车内商品
 * larry create on 2015-8-31
 */
public interface IProductDao {

	public abstract List<ProductInfo> getProductInfos();
	
	public abstract int addProductInfo(ProductInfo product);
	
	public abstract int removeProductInfo(ProductInfo product);
	
}
