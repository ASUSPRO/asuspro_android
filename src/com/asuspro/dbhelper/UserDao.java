package com.asuspro.dbhelper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import android.text.TextUtils;
import android.util.Log;

import com.asuspro.config.AppConstants;
import com.asuspro.model.UserInfo;

/**
 * User表
 * 
 * larry create on 2015-8-4
 */
public class UserDao implements IUserDao {

	private Context mContext;
	private static final String[] TABLE_COLUMNS;
	private static final String TABLE_NAME = "user";
	private static final String DATABASE_NAME = "user.db";
	private static final int DATABASE_VERSION = 1; // TODO data base version

	private UserSqliteHelper mSqliteHelper = null;

	/**
	 * private int id; 			// 用户的id
	private String username;	// 用户帐号
	private String password;	// 登录密码，md5加密
	private String name;		// 用户姓名
	private int gender;			// 0-男，1-女
	private String birthday;	// 出生日期
	private String address;		// 地址
	private String mobile;		// 手机号
	private String email;		// 电子邮件
	private long score;			// 用户积分
	private int dealer_id;		// 所属经销商id，此字段不用关注
	private int user_type;		// 1-内部人员，2-经销商
	private String pic_url;		// 用户头像
	private String token;		// token
	private String company;		// 公司名称
	private String phone;		// 座机
	
	private int status;			// 登录态
	 */
	private static final String CREATE_SQL = "CREATE TABLE " + TABLE_NAME
			+ " (" + "id INTEGER PRIMARY KEY, " + "username TEXT, " + "password TEXT, "
			+ "name TEXT, " + "gender INTEGER, " + "birthday TEXT, " + "address TEXT, "
			+ "mobile TEXT, " + "email TEXT, " + "score INTEGER, " + "dealer_id INTEGER, "
			+ "user_type INTEGER, " + "pic_url TEXT, " + "token TEXT, " + "company TEXT, "
			+ "phone TEXT," + "status INTEGER" + ")";

	private static final String ADD_ONE_USER = "INSERT INTO " + TABLE_NAME
			+ " (id, username, password, name, gender, birthday, address, mobile, email,"
			+ " score, dealer_id, user_type, pic_url, token, company, phone, status) VALUES "
			+ " (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	
	private static final String DELETE_USER = "DROP TABLE users";
	
	static {
		String[] columnStrings = new String[17];
		columnStrings[0] = "id";
        columnStrings[1] = "username";
        columnStrings[2] = "password";
        columnStrings[3] = "name";
        columnStrings[4] = "gender";
        columnStrings[5] = "birthday";
        columnStrings[6] = "address";
        columnStrings[7] = "mobile";
        columnStrings[8] = "email";
        columnStrings[9] = "score";
        columnStrings[10] = "dealer_id";
        columnStrings[11] = "user_type";
        columnStrings[12] = "pic_url";
        columnStrings[13] = "token";
        columnStrings[14] = "company";
        columnStrings[15] = "phone";
        columnStrings[16] = "status";
		TABLE_COLUMNS = columnStrings;
	}
	
	
	public UserDao(Context mContext) {
		// TODO Auto-generated constructor stub
		this.mContext = mContext;
	}

	public class UserSqliteHelper extends SQLiteOpenHelper {
		public UserSqliteHelper(Context context) {
			// TODO Auto-generated constructor stub
			super(context, DATABASE_NAME, null, DATABASE_VERSION);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			// TODO Auto-generated method stub
			db.execSQL(CREATE_SQL);
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			// TODO Auto-generated method stub

		}

	}
	
	private void addColumn(SQLiteDatabase db, String dbTable, String columnName,
			String columnDefinition) {
        db.execSQL("ALTER TABLE " + dbTable + " ADD COLUMN " + columnName + " "
			+ columnDefinition);
    }

	private void openConnection() {
		mSqliteHelper = new UserSqliteHelper(this.mContext);
	}

	private void closeConnection() {
		mSqliteHelper = null;
	}

	@Override
	public UserInfo getLoginUser() {
		// TODO Auto-generated method stub
		return getUserById(AppConstants.CURRENT_USER_ID);
	}

	@Override
	public synchronized int updateUserInfo(UserInfo user) {
		// TODO Auto-generated method stub
		if (user == null) {
    		return -1;
    	}
        int res = 0;
        SQLiteDatabase db = null;
        try {
        	openConnection();

            db = mSqliteHelper.getWritableDatabase();
            db.beginTransaction();
            
            ContentValues updatedValues = new ContentValues();
            
            String username = user.getUsername();
            if (!TextUtils.isEmpty(username)) {
            	updatedValues.put(TABLE_COLUMNS[1], username);
            }
            String password = user.getPassword();
            if(!TextUtils.isEmpty(password)) {
            	updatedValues.put(TABLE_COLUMNS[2], password);
            }
            String name = user.getName();
            if(!TextUtils.isEmpty(name)) {
            	updatedValues.put(TABLE_COLUMNS[3], name);
            }
            int gender = user.getGender();
            updatedValues.put(TABLE_COLUMNS[4], gender);
            String birthday = user.getBirthday();
            if(!TextUtils.isEmpty(birthday)) {
            	updatedValues.put(TABLE_COLUMNS[5], birthday);
            }
            String address = user.getAddress();
            if(!TextUtils.isEmpty(address)) {
            	updatedValues.put(TABLE_COLUMNS[6], address);
            }
            String mobile = user.getMobile();
            if(!TextUtils.isEmpty(mobile)) {
            	updatedValues.put(TABLE_COLUMNS[7], mobile);
            }
            String email = user.getEmail();
            if(!TextUtils.isEmpty(email)) {
            	updatedValues.put(TABLE_COLUMNS[8], email);
            }
            long score = user.getScore();
            updatedValues.put(TABLE_COLUMNS[9], score);
            int dealer_id = user.getDealer_id();
            updatedValues.put(TABLE_COLUMNS[10], dealer_id);
            int user_type = user.getUser_type();
            updatedValues.put(TABLE_COLUMNS[11], user_type);
            String pic_url = user.getPic_url();
            if(!TextUtils.isEmpty(pic_url)) {
            	updatedValues.put(TABLE_COLUMNS[12], pic_url);
            }
            String token = user.getToken();
            if(!TextUtils.isEmpty(token)) {
            	updatedValues.put(TABLE_COLUMNS[13], token);
            }
            String company = user.getCompany();
            if(!TextUtils.isEmpty(company)) {
            	updatedValues.put(TABLE_COLUMNS[14], company);
            }
            String phone = user.getPhone();
            if(!TextUtils.isEmpty(phone)) {
            	updatedValues.put(TABLE_COLUMNS[15], phone);
            }
            int status = user.getStatus();
            updatedValues.put(TABLE_COLUMNS[16], status);
            
            String where = TABLE_COLUMNS[0] + "=" + user.getId();
            // Update the row with the specified index with the new values.
            int rows = db.update(TABLE_NAME, updatedValues, where, null);
            Log.d("larry","updateUserInfo-->rowsid-=" + rows + "user.getUser_id()==" + user.getId());
            // Only rows == 0 is ok
            if (rows != 0) {
                res = -1;
            }
            db.setTransactionSuccessful();
            
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			if (db != null) {
                db.endTransaction();
                db.close();
            }
            closeConnection();
		}
		return res;
	}

	@Override
	public int addUserInfo(UserInfo user) {
		// TODO Auto-generated method stub
		 int res = 0;
		 SQLiteDatabase db = null;
		 try {
			openConnection();

            db = mSqliteHelper.getWritableDatabase();
            db.beginTransaction();
//	            db.execSQL("delete from users");
            SQLiteStatement statement = db.compileStatement(ADD_ONE_USER);
            
            DBTools.bindLong(statement, 1, user.getId());
            DBTools.bindString(statement, 2, !TextUtils.isEmpty(user.getUsername()) ? user.getUsername() : "");
            DBTools.bindString(statement, 3, !TextUtils.isEmpty(user.getPassword()) ? user.getPassword() : "");
            DBTools.bindString(statement, 4, !TextUtils.isEmpty(user.getName()) ? user.getName() : "");
            DBTools.bindLong(statement, 5, user.getGender());
            DBTools.bindString(statement, 6, !TextUtils.isEmpty(user.getBirthday()) ? user.getBirthday() : "");
            DBTools.bindString(statement, 7, !TextUtils.isEmpty(user.getAddress()) ? user.getAddress() : "");
            DBTools.bindString(statement, 8, !TextUtils.isEmpty(user.getMobile()) ? user.getMobile() : "");
            DBTools.bindString(statement, 9, !TextUtils.isEmpty(user.getEmail()) ? user.getEmail() : "");
            DBTools.bindLong(statement, 10, user.getScore());
            DBTools.bindLong(statement, 11, user.getDealer_id());
            DBTools.bindLong(statement, 12, user.getUser_type());
            DBTools.bindString(statement, 13, !TextUtils.isEmpty(user.getPic_url()) ? user.getPic_url() : "");
            DBTools.bindString(statement, 14, !TextUtils.isEmpty(user.getToken()) ? user.getToken() : "");
            DBTools.bindString(statement, 15, !TextUtils.isEmpty(user.getCompany()) ? user.getCompany() : "");
            DBTools.bindString(statement, 16, !TextUtils.isEmpty(user.getPhone()) ? user.getPhone() : "");
            DBTools.bindLong(statement, 17, user.getStatus());
            
            long rowid = statement.executeInsert();
            if (rowid == -1) {
                res = -1;
            }
            db.setTransactionSuccessful();
            Log.d("larry","addUserInfo-->rowid-=" + rowid);
	            
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
            if (db != null) {
                db.endTransaction();
                db.close();
            }
            closeConnection();
        }
		return res;
	}

	@Override
	public int deleteUserInfo(UserInfo user) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public UserInfo getUserById(int userId) {
		// TODO Auto-generated method stub
		UserInfo user = null;
        SQLiteDatabase dbInstance = null;
        Cursor cursor = null;

        try {
            openConnection();
            dbInstance = mSqliteHelper.getReadableDatabase();
            dbInstance.beginTransaction();
            cursor = dbInstance.query(TABLE_NAME, TABLE_COLUMNS,
                    TABLE_COLUMNS[0] + "='" + userId + "'", null, null, null, null);
            if (cursor != null && cursor.getCount() != 0) {
                cursor.moveToFirst();
                user = extractAnUser(cursor);
            }
            dbInstance.setTransactionSuccessful();
        } catch (Exception e) {
            throw new SQLiteException(e.toString());
        } finally {
            // close book data base file
            if (dbInstance != null) {
                dbInstance.endTransaction();
                dbInstance.close();
            }
            if (cursor != null) {
                cursor.close();
            }
            closeConnection();
        }
        return user;
	}
	
	/**
	 * private int id; 			// 用户的id
	private String username;	// 用户帐号
	private String password;	// 登录密码，md5加密
	private String name;		// 用户姓名
	private int gender;			// 0-男，1-女
	private String birthday;	// 出生日期
	private String address;		// 地址
	private String mobile;		// 手机号
	private String email;		// 电子邮件
	private long score;			// 用户积分
	private int dealer_id;		// 所属经销商id，此字段不用关注
	private int user_type;		// 1-内部人员，2-经销商
	private String pic_url;		// 用户头像
	private String token;		// token
	private String company;		// 公司名称
	private String phone;		// 座机
	 */
	private UserInfo extractAnUser(Cursor cursor) {
		UserInfo user = new UserInfo();
		user.setId(cursor.getInt(cursor.getColumnIndex(TABLE_COLUMNS[0])));
		user.setUsername(cursor.getString(cursor.getColumnIndex(TABLE_COLUMNS[1])));
		user.setPassword(cursor.getString(cursor.getColumnIndex(TABLE_COLUMNS[2])));
		user.setName(cursor.getString(cursor.getColumnIndex(TABLE_COLUMNS[3])));
		user.setGender(cursor.getInt(cursor.getColumnIndex(TABLE_COLUMNS[4])));
		user.setBirthday(cursor.getString(cursor.getColumnIndex(TABLE_COLUMNS[5])));
		user.setAddress(cursor.getString(cursor.getColumnIndex(TABLE_COLUMNS[6])));
		user.setMobile(cursor.getString(cursor.getColumnIndex(TABLE_COLUMNS[7])));
		user.setEmail(cursor.getString(cursor.getColumnIndex(TABLE_COLUMNS[8])));
		user.setScore(cursor.getLong(cursor.getColumnIndex(TABLE_COLUMNS[9])));
		user.setDealer_id(cursor.getInt(cursor.getColumnIndex(TABLE_COLUMNS[10])));
		user.setUser_type(cursor.getInt(cursor.getColumnIndex(TABLE_COLUMNS[11])));
		user.setPic_url(cursor.getString(cursor.getColumnIndex(TABLE_COLUMNS[12])));
		user.setToken(cursor.getString(cursor.getColumnIndex(TABLE_COLUMNS[13])));
		user.setCompany(cursor.getString(cursor.getColumnIndex(TABLE_COLUMNS[14])));
		user.setPhone(cursor.getString(cursor.getColumnIndex(TABLE_COLUMNS[15])));
		user.setStatus(cursor.getInt(cursor.getColumnIndex(TABLE_COLUMNS[16])));
		return user;
	}
}
