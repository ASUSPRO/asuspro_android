package com.asuspro.dbhelper;

import android.content.Context;

import com.asuspro.base.App;

/**
 * 数据库管理类 larry create on 2015-8-4
 */
public class DBManager {

	private static final Object mInstanceSync = new Object();
	private static DBManager mInstance;
	private DaoFactory mDaoFactory;
	private Context mAppContext;

	public static IUserDao getUserDBHandler() {
		return _getInstance()._getUserDBHandler();
	}

	public static IAddressDao getAddressDBHandler() {
		return _getInstance()._getAddressDBHandler();
	}

	public static IProductDao getProductDBHandler() {
		return _getInstance()._getProductDBHandler();
	}

	private IUserDao _getUserDBHandler() {
		return mDaoFactory.getUserDao(mAppContext);
	}

	private IAddressDao _getAddressDBHandler() {
		return mDaoFactory.getAddressDao(mAppContext);
	}

	private IProductDao _getProductDBHandler() {
		return mDaoFactory.getProductDao(mAppContext);
	}

	/**
	 * @param context
	 * @return
	 */
	private static DBManager _getInstance() {
		synchronized (mInstanceSync) {
			if (mInstance == null) {
				mInstance = new DBManager();
			}
		}
		return mInstance;
	}

	/**
	 * @notice should not call new DataManager() direct
	 */
	private DBManager() {
		mDaoFactory = new DaoFactory();
		mAppContext = App.getInstance().getApplicationContext();
	}

}
