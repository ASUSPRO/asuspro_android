package com.asuspro.dbhelper;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import android.text.TextUtils;
import android.util.Log;

import com.asuspro.model.ProductInfo;

/**
 * 购物车 larry create on 2015-8-31
 */
public class ProductDao implements IProductDao {

	private static final String[] TABLE_COLUMNS;
	private static final String TABLE_NAME = "product";
	private static final String DATABASE_NAME = "product.db";
	private static final int DATABASE_VERSION = 1; // TODO data base version

	private ProductSqliteHelper mSqliteHelper = null;

	/*
	 * private int product_id; 
	 * private long cat_id;//所属分类的id 
	 * private String part_number;//料号 
	 * private String type;//型号 
	 * private String stock_position;//库位 
	 * private int num;//库存数量 
	 * private long price;
	 */
	private static final String CREATE_SQL = "CREATE TABLE " + TABLE_NAME
			+ " (" + "product_id INTEGER PRIMARY KEY, " + "cat_id INTEGER, "
			+ "part_number TEXT, " + "type TEXT, " + "stock_position TEXT, "
			+ "num INTEGER, " + "price INTEGER" + ")";

	private static final String ADD_ONE_PRODUCT = "INSERT INTO "
			+ TABLE_NAME
			+ " (product_id, cat_id, part_number, type, stock_position, num, price) VALUES "
			+ " (?,?,?,?,?,?,?)";

	private static final String DELETE_PRODUCT = "DROP TABLE product";

	static {
		String[] columnStrings = new String[7];
		columnStrings[0] = "product_id";
		columnStrings[1] = "cat_id";
		columnStrings[2] = "part_number";
		columnStrings[3] = "type";
		columnStrings[4] = "stock_position";
		columnStrings[5] = "num";
		columnStrings[6] = "price";
		TABLE_COLUMNS = columnStrings;
	}

	private Context mContext;

	public ProductDao(Context context) {

	}
	
	public class ProductSqliteHelper extends SQLiteOpenHelper {
		public ProductSqliteHelper(Context context) {
			// TODO Auto-generated constructor stub
			super(context, DATABASE_NAME, null, DATABASE_VERSION);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			// TODO Auto-generated method stub
			db.execSQL(CREATE_SQL);
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			// TODO Auto-generated method stub

		}

	}
	
	private void openConnection() {
		mSqliteHelper = new ProductSqliteHelper(this.mContext);
	}

	private void closeConnection() {
		mSqliteHelper = null;
	}

	@Override
	public List<ProductInfo> getProductInfos() {
		// TODO Auto-generated method stub
		List<ProductInfo> list = new ArrayList<ProductInfo>();
        SQLiteDatabase dbInstance = null;
        Cursor cursor = null;

        try {
            openConnection();
            dbInstance = mSqliteHelper.getReadableDatabase();
            dbInstance.beginTransaction();
            cursor = dbInstance.query(TABLE_NAME, TABLE_COLUMNS,
                    null, null, null, null, null);
            
            if (cursor != null) {
                cursor.moveToFirst();
                while (cursor != null && cursor.getCount() != 0
                        && !cursor.isAfterLast()) {
                	ProductInfo product = extractAnProduct(cursor);
                    list.add(product);
                    cursor.moveToNext();
                }
            }
            
            dbInstance.setTransactionSuccessful();
        } catch (Exception e) {
            throw new SQLiteException(e.toString());
        } finally {
            // close book data base file
            if (dbInstance != null) {
                dbInstance.endTransaction();
                dbInstance.close();
            }
            if (cursor != null) {
                cursor.close();
            }
            closeConnection();
        }
        return list;
	}
	
	private ProductInfo extractAnProduct(Cursor cursor) {
		ProductInfo product = new ProductInfo();
		product.setProduct_id(cursor.getInt(cursor.getColumnIndex(TABLE_COLUMNS[0])));
		product.setCat_id(cursor.getInt(cursor.getColumnIndex(TABLE_COLUMNS[1])));
		product.setPart_number(cursor.getString(cursor.getColumnIndex(TABLE_COLUMNS[2])));
		product.setType(cursor.getString(cursor.getColumnIndex(TABLE_COLUMNS[3])));
		product.setStock_position(cursor.getString(cursor.getColumnIndex(TABLE_COLUMNS[4])));
		product.setNum(cursor.getInt(cursor.getColumnIndex(TABLE_COLUMNS[5])));
		product.setPrice(cursor.getInt(cursor.getColumnIndex(TABLE_COLUMNS[6])));
		return product;
	}

	/*
	 * private int product_id; 
	 * private long cat_id;//所属分类的id 
	 * private String part_number;//料号 
	 * private String type;//型号 
	 * private String stock_position;//库位 
	 * private int num;//库存数量 
	 * private long price;
	 */
	@Override
	public int addProductInfo(ProductInfo product) {
		// TODO Auto-generated method stub
		int res = 0;
		 SQLiteDatabase db = null;
		 try {
			openConnection();

          db = mSqliteHelper.getWritableDatabase();
          db.beginTransaction();
//	            db.execSQL("delete from users");
          SQLiteStatement statement = db.compileStatement(ADD_ONE_PRODUCT);
         
          DBTools.bindLong(statement, 1, product.getProduct_id());
          DBTools.bindLong(statement, 2, product.getCat_id());
          DBTools.bindString(statement, 3, !TextUtils.isEmpty(product.getPart_number()) ? product.getPart_number() : "");
          DBTools.bindString(statement, 4, !TextUtils.isEmpty(product.getType()) ? product.getType() : "");
          DBTools.bindString(statement, 5, TextUtils.isEmpty(product.getStock_position())? product.getStock_position() : "");
          DBTools.bindLong(statement, 6, product.getNum());
          DBTools.bindLong(statement, 7, product.getPrice());
          
          long rowid = statement.executeInsert();
          if (rowid == -1) {
              res = -1;
          }
          db.setTransactionSuccessful();
          Log.d("larry","addProductInfo-->rowid-=" + rowid);
	            
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			if (db != null) {
			    db.endTransaction();
			    db.close();
			}
			closeConnection();
      }
		return res;
	}

	@Override
	public int removeProductInfo(ProductInfo product) {
		// TODO Auto-generated method stub
		if(product == null) {
			return -1;
		}
		int res = 0;
		SQLiteDatabase db = null;
		 try {
			openConnection();
			db = mSqliteHelper.getWritableDatabase();
			db.beginTransaction();
            //调用delete方法，删除数据  
            db.delete(TABLE_NAME, "product_id=?", new String[]{product.getProduct_id() + ""}); 
		} catch (Exception e) {
			// TODO: handle exception
        	e.printStackTrace();
		} finally {
	        if (db != null) {
	            db.endTransaction();
	            db.close();
	        }
	        closeConnection();
		}
		return res;
	}

}
