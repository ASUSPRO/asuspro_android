package com.asuspro.dbhelper;

import com.asuspro.model.UserInfo;

/**
 * 
 * larry create on 2015-8-4
 */
public interface IUserDao {

	
	public abstract UserInfo getLoginUser();
//	public abstract UserInfo getRecentLogoutUser();
//	public abstract List<String> getUserIdList();
//	public abstract UserInfo getDefaultRegistUser();
	
	public abstract int updateUserInfo(UserInfo user);
	public abstract int addUserInfo(UserInfo user);
	public abstract int deleteUserInfo(UserInfo user);
	
	public abstract UserInfo getUserById(int userId);
}
