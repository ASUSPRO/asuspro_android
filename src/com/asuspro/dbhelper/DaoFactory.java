package com.asuspro.dbhelper;

import android.content.Context;

/**
 * 
 * larry create on 2015-8-4
 */
public class DaoFactory {

	private UserDao mUserDBHandler;
	private AddressDao mAddressDBHandler;
	private ProductDao mProductDBHandler;

	public DaoFactory() {
		// TODO Auto-generated constructor stub
	}

	public IUserDao getUserDao(Context context) {

		synchronized (this) {
			if (mUserDBHandler == null) {
				mUserDBHandler = new UserDao(context);
			}
		}
		return mUserDBHandler;
	}

	public IAddressDao getAddressDao(Context context) {
		
		synchronized (this) {
			if (mAddressDBHandler == null) {
				mAddressDBHandler = new AddressDao(context);
			}
		}
		return mAddressDBHandler;
	}
	
	public IProductDao getProductDao(Context context) {
		synchronized (this) {
			if (mProductDBHandler == null) {
				mProductDBHandler = new ProductDao(context);
			}
		}
		return mProductDBHandler;
	}

}
