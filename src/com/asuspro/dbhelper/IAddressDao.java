package com.asuspro.dbhelper;

import java.util.List;

import com.asuspro.model.AddressInfo;

/**
 * 
 * larry create on 2015-8-25
 */
public interface IAddressDao {

	public abstract AddressInfo getDefaultAddress();
	
	public abstract List<AddressInfo> getAddressInfos();
	
	public abstract int addAddressInfo(AddressInfo address);
	
	public abstract int updateAddressInfo(AddressInfo address);

	public abstract int updateAddressInfos(List<AddressInfo> addresses);
	
	public abstract AddressInfo getAddressInfoById(int id);
}
