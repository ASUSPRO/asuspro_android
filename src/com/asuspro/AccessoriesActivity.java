package com.asuspro;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.asuspro.adapter.AccessoriesAdapter;
import com.asuspro.adapter.ClassifyAdapter;
import com.asuspro.base.App;
import com.asuspro.base.BaseActivity;
import com.asuspro.manager.HttpProxy;
import com.asuspro.manager.HttpProxy.AsyncResponseListener;
import com.asuspro.manager.JsonParser;
import com.asuspro.model.BaseResult;
import com.asuspro.model.CategoryBean;
import com.asuspro.model.ProductInfo;
import com.asuspro.tools.ToastFactory;

/**
 * 配件价格
 * larry create on 2015-7-6
 */
public class AccessoriesActivity extends BaseActivity {
	private TextView notice_title;
	private ListView mListView;
	private RelativeLayout notice_classify_bg;
	private GridView notice_grid;
	
	private List<CategoryBean> classifys = new ArrayList<CategoryBean>();
	private List<ProductInfo> products = new ArrayList<ProductInfo>();
	
	private AccessoriesAdapter mAdapter;
	private ClassifyAdapter classifyAdapter;
	
	private int category_id = 0;

	@Override
	protected int getContentView() {
		// TODO Auto-generated method stub
		return R.layout.activity_accessories;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		initView();
		setListener();
		getColumnListDataFromNet();
		getAccessoriesListData();
	}

	private void initView() {
		notice_title = (TextView) findViewById(R.id.notice_title);
		mListView = (ListView) findViewById(R.id.mListView);
		notice_classify_bg = (RelativeLayout) findViewById(R.id.notice_classify_bg);
		notice_grid = (GridView) findViewById(R.id.notice_grid);
		
		notice_title.setTypeface(App.getInstance().getCommnonTypeface());
		notice_title.setText(Html.fromHtml("全部分类 &#xe602"));
		
		classifyAdapter = new ClassifyAdapter(this);
		classifyAdapter.setData(classifys);
		notice_grid.setAdapter(classifyAdapter);
		
		mAdapter = new AccessoriesAdapter(this);
		mAdapter.setData(products);
		mListView.setAdapter(mAdapter);
	}
	
	private void initColumnListData(List<CategoryBean> classifys) {
		this.classifys = classifys;
		classifyAdapter.setData(classifys);
		classifyAdapter.notifyDataSetChanged();
	}
	
	private void initProductListData(List<ProductInfo> data) {
		this.products = data;
		mAdapter.setData(data);
		mAdapter.notifyDataSetChanged();
	}
	
	private void setListener() {
		notice_title.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(notice_classify_bg.getVisibility() == View.VISIBLE) {
					notice_classify_bg.setVisibility(View.GONE);
					notice_title.setText(Html.fromHtml("全部分类 &#xe602"));
				} else {
					notice_classify_bg.setVisibility(View.VISIBLE);
					notice_title.setText(Html.fromHtml("全部分类 &#xe61c"));
				}
			}
		});
		
		notice_classify_bg.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				notice_classify_bg.setVisibility(View.GONE);
				notice_title.setText(Html.fromHtml("全部分类 &#xe602"));
			}
		});
		notice_grid.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				notice_classify_bg.setVisibility(View.GONE);
				if(position == 0) {
					notice_title.setText(Html.fromHtml("全部分类 &#xe602"));
					category_id = 0;
				} else {
					notice_title.setText(Html.fromHtml(classifys.get(position).getColumn_name() + " &#xe602"));
					category_id = (int) classifys.get(position).getColumn_id();
				}
				getAccessoriesListData();
			}
		});
		mListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
//				Intent i = new Intent();
//				i.setClass(AccessoriesActivity.this, NoticeDetailActivity.class);
//				i.putExtra("id", notices.get(position).getId());
//				i.putExtra("cid", pcid);
//				startActivity(i);
			}
		});
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	protected String getHeaderBarTitle() {
		// TODO Auto-generated method stub
		return "配件价格";
	}

	@Override
	protected String getHeaderBarRightIcon() {
		// TODO Auto-generated method stub
		return "";
	}

	@Override
	protected boolean onClickLeftEvent() {
		// TODO Auto-generated method stub
		finish();
		return super.onClickLeftEvent();
	}
	
	//获取分类名目
	private void getColumnListDataFromNet() {
		HttpProxy.requestProductCategory(new AsyncResponseListener() {
			
			@Override
			public void onSuccess(String data) {
				// TODO Auto-generated method stub
				Log.d("larry", "--getColumnListDataFromNet--" + data);
				hideWaitDialog();
				BaseResult result = JsonParser.parserProductCategory(data);
				if(result.getResultCode() == 0) {
					try {
						List<CategoryBean> classifys = (List<CategoryBean>) result.getResultContent();
						initColumnListData(classifys);
					} catch (Exception e) {
						e.printStackTrace();
					}
				} else {
					ToastFactory.showToast(AccessoriesActivity.this, result.getResultMsg(), Toast.LENGTH_SHORT);
				}
			}
			
			@Override
			public void onFailure(String error) {
				// TODO Auto-generated method stub
				ToastFactory.showToast(AccessoriesActivity.this, error, Toast.LENGTH_SHORT);
			}
		});
	}
	
	// 获取列表信息
	private void getAccessoriesListData() {
		showWaitDialog();
		HttpProxy.requestProductList(category_id, 0, new AsyncResponseListener() {
			
			@Override
			public void onSuccess(String data) {
				// TODO Auto-generated method stub
				hideWaitDialog();
				Log.d("larry", "--getAccessoriesListData--" + data);
				BaseResult result = JsonParser.parserProduct(data);
				if(result.getResultCode() == 0) {
					try {
						List<ProductInfo> products = (List<ProductInfo>) result.getResultContent();
						initProductListData(products);
					} catch (Exception e) {
						e.printStackTrace();
					}
				} else {
					ToastFactory.showToast(AccessoriesActivity.this, result.getResultMsg(), Toast.LENGTH_SHORT);
				}
			}
			
			@Override
			public void onFailure(String error) {
				// TODO Auto-generated method stub
				hideWaitDialog();
				ToastFactory.showToast(AccessoriesActivity.this, error, Toast.LENGTH_SHORT);
			}
		});
	}

}
