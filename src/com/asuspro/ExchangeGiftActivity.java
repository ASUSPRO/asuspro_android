package com.asuspro;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.asuspro.base.BaseActivity;
import com.asuspro.dbhelper.DBManager;
import com.asuspro.model.AddressInfo;
import com.asuspro.model.ExchangeInfo;
import com.asuspro.model.UserInfo;

/**
 * 商品兑换界面
 * larry create on 2015-8-15
 */
public class ExchangeGiftActivity extends BaseActivity {
	private TextView exchange_gift_edit_address;
	private RelativeLayout exchange_gift_address_layout;
	private TextView exchange_gift_name;
	private TextView exchange_gift_email;
	private TextView exchange_gift_address;
	
	private TextView exchange_gift_mine_score;
	private TextView exchange_gift_need_score;
	
	private Button exchange_gift_next;
	
	private UserInfo userInfo;
	
	private AddressInfo addressInfo;
	
	private ExchangeInfo exchangeInfo = null;

	@Override
	protected int getContentView() {
		// TODO Auto-generated method stub
		return R.layout.activity_exchange_gift;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		exchangeInfo = (ExchangeInfo) getIntent().getSerializableExtra("exchange");
		initView();
		initData();
		setListener();
	}
	
	private void initView() {
		exchange_gift_edit_address = (TextView) findViewById(R.id.exchange_gift_edit_address);
		exchange_gift_address_layout = (RelativeLayout) findViewById(R.id.exchange_gift_address_layout);
		exchange_gift_name = (TextView) findViewById(R.id.exchange_gift_name);
		exchange_gift_email = (TextView) findViewById(R.id.exchange_gift_email);
		exchange_gift_address = (TextView) findViewById(R.id.exchange_gift_address);
		exchange_gift_mine_score = (TextView) findViewById(R.id.exchange_gift_mine_score);
		exchange_gift_need_score = (TextView) findViewById(R.id.exchange_gift_need_score);
		exchange_gift_next = (Button) findViewById(R.id.exchange_gift_next);
	}
	
	private void initData() {
		userInfo = DBManager.getUserDBHandler().getLoginUser();
		
		if(userInfo != null) {
			exchange_gift_mine_score.setText("" + userInfo.getScore());
		}
		if(exchangeInfo != null) {
			exchange_gift_need_score.setText("" + exchangeInfo.getScore());
		}
		
		addressInfo = DBManager.getAddressDBHandler().getDefaultAddress();
		if(addressInfo != null) {
			exchange_gift_name.setText(addressInfo.getUsername() + "\t\t\t\t" + addressInfo.getMobile());
			exchange_gift_address.setText(addressInfo.getAddress());
		}
		
	}
	
	private void setListener() {
		exchange_gift_edit_address.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent();
				i.setClass(ExchangeGiftActivity.this, AddressListActivity.class);
				i.putExtra("from", "exchange");
				startActivityForResult(i, 0);
			}
		});
		
		exchange_gift_next.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
			}
		});
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if(resultCode == Activity.RESULT_OK) {
			if(requestCode == 0) {
				initData();
			}
		}
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	protected String getHeaderBarTitle() {
		// TODO Auto-generated method stub
		return "兑换信息";
	}

	@Override
	protected String getHeaderBarRightIcon() {
		// TODO Auto-generated method stub
		return "";
	}

	@Override
	protected boolean onClickLeftEvent() {
		// TODO Auto-generated method stub
		finish();
		return super.onClickLeftEvent();
	}
	

}
