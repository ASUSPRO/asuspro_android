package com.asuspro.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.asuspro.R;
import com.asuspro.model.NoticeInfo;
import com.asuspro.tools.Tools;

/**
 * 
 * larry create on 2015-5-9
 */
public class NoticeAdapter extends BaseAdapter {
	
	private List<NoticeInfo> notices = new ArrayList<NoticeInfo>();
	private Context mContext;
	
	public NoticeAdapter (Context mContext) {
		this.mContext = mContext;
	}
	
	public void setData(List<NoticeInfo> data) {
		if(data != null) {
			this.notices = data;
		} else {
			this.notices.clear();
		}
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return notices.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return notices.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder holder;
		if(convertView == null) {
			holder = new ViewHolder();
			convertView = LayoutInflater.from(mContext).inflate(R.layout.adapter_notice, null);
			holder.title = (TextView) convertView.findViewById(R.id.adapter_notice_title);
			holder.desc = (TextView) convertView.findViewById(R.id.adapter_notice_desc);
			holder.time = (TextView) convertView.findViewById(R.id.adapter_notice_time);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		holder.title.setText(notices.get(position).getMaintitle());
		holder.time.setText(Tools.convertTimeStamp(notices.get(position).getCreatetime(), "yyyy-MM-dd"));
		holder.desc.setText(notices.get(position).getSource());
		return convertView;
	}

	static class ViewHolder {
		TextView title;
		TextView desc;
		TextView time;
	}
	
}
