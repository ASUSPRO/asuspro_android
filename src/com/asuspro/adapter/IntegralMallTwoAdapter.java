package com.asuspro.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.asuspro.R;
import com.asuspro.model.GiftInfo;
import com.loopj.android.image.SmartImageView;

/**
 * 
 * larry create on 2015-7-23
 */
public class IntegralMallTwoAdapter extends BaseAdapter {
	private Context mContext;
	private List<GiftInfo> gifts = new ArrayList<GiftInfo>();
	
	public IntegralMallTwoAdapter(Context mContext) {
		this.mContext = mContext;
	}
	
	public void setData(List<GiftInfo> gifts) {
		if(gifts == null) {
			this.gifts.clear();
		} else {
			this.gifts = gifts;
		}
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return gifts.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return gifts.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder holder;
		if(convertView == null) {
			holder = new ViewHolder();
			convertView = LayoutInflater.from(mContext).inflate(R.layout.adapter_integral_mall_two, null);
			holder.image = (SmartImageView) convertView.findViewById(R.id.adapter_integral_mall_two_image);
			holder.title = (TextView) convertView.findViewById(R.id.adapter_integral_mall_two_title);
			holder.price = (TextView) convertView.findViewById(R.id.adapter_integral_mall_two_price);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		holder.image.setImageUrl(gifts.get(position).getGift_pic());
		holder.title.setText(Html.fromHtml(gifts.get(position).getGift_name()));
		holder.price.setText(Html.fromHtml(gifts.get(position).getInner_score() + "积分"));
		return convertView;
	}

	static class ViewHolder {
		SmartImageView image;
		TextView title;
		TextView price;
	}
	
}
