package com.asuspro.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.asuspro.R;
import com.asuspro.model.CategoryBean;

/**
 * 商城分类列表Adapter
 * larry create on 2015-7-21
 */
public class IntegralMallCategoryAdapter extends BaseExpandableListAdapter {
	private Context mContext;
	private List<String> groups = new ArrayList<String>();
	private List<CategoryBean> categorys = new ArrayList<CategoryBean>();
	
	public IntegralMallCategoryAdapter(Context mContext) {
		this.mContext = mContext;
		
	}
	
	public void setChildData(List<CategoryBean> childs) {
		if(childs == null) {
			categorys.clear();
		} else {
			categorys = childs;
		}
		groups.clear();
		groups.add("本月新品");
		groups.add("华硕产品");
		groups.add("数码产品");
	}
	
	@Override
	public int getGroupCount() {
		// TODO Auto-generated method stub
		return groups.size();
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		// TODO Auto-generated method stub
		if(groupPosition == 2) {
			return categorys.size();
		}
		return 0;
	}

	@Override
	public Object getGroup(int groupPosition) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		if(groupPosition == 2) {
			return categorys.get(childPosition);
		}
		return null;
	}

	@Override
	public long getGroupId(int groupPosition) {
		// TODO Auto-generated method stub
		return groupPosition;
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		if(groupPosition == 2) {
			return childPosition;
		}
		return 0;
	}

	@Override
	public boolean hasStableIds() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder holder;
		if(convertView == null) {
			holder = new ViewHolder();
			convertView = LayoutInflater.from(mContext).inflate(R.layout.adapter_integral_mall_category, null);
			holder.name = (TextView) convertView.findViewById(R.id.adapter_integral_mall_category_name);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		holder.name.setTextSize(16);
		holder.name.setText(groups.get(groupPosition));
		return convertView;
	}

	@Override
	public View getChildView(int groupPosition, int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		if(groupPosition != 2) {
			return null;
		}
		ViewHolder holder;
		if(convertView == null) {
			holder = new ViewHolder();
			convertView = LayoutInflater.from(mContext).inflate(R.layout.adapter_integral_mall_category, null);
			holder.name = (TextView) convertView.findViewById(R.id.adapter_integral_mall_category_name);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		holder.name.setPadding(40, 20, 20, 20);
		holder.name.setText(categorys.get(childPosition).getColumn_name());
		return convertView;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return true;
	}

	static class ViewHolder {
		TextView name;
	}
	
}
