package com.asuspro.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.asuspro.R;
import com.asuspro.model.GiftInfo;
import com.loopj.android.image.SmartImageView;

/**
 * 积分商场Adapter
 * larry create on 2015-6-18
 */
public class IntegralMallAdapter extends BaseAdapter {
	private Context mContext;
	private ArrayList<GiftInfo> imageInfos = new ArrayList<GiftInfo>();

	public IntegralMallAdapter (Context mContext) {
		this.mContext = mContext;
	}
	
	public void setData(ArrayList<GiftInfo> imageInfos) {
		if(imageInfos != null) {
			this.imageInfos = imageInfos;
		} else {
			this.imageInfos.clear();
		}
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return imageInfos.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return imageInfos.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder holder;
		if(convertView == null) {
			holder = new ViewHolder();
			convertView = LayoutInflater.from(mContext).inflate(R.layout.adapter_integralmall_layout, null);
			holder.image = (SmartImageView) convertView.findViewById(R.id.integralmall_image);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		holder.image.setImageUrl(imageInfos.get(position).getRecommend_pic());
		return convertView;
	}
	
	static class ViewHolder {
		SmartImageView image;
	}

}
