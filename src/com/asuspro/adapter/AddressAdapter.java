package com.asuspro.adapter;

import java.util.ArrayList;
import java.util.List;

import com.asuspro.R;
import com.asuspro.base.App;
import com.asuspro.model.AddressInfo;
import com.asuspro.tools.StringUtils;
import com.asuspro.tools.Tools;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

/**
 * 
 * larry create on 2015-8-6
 */
public class AddressAdapter extends BaseAdapter {

	private List<AddressInfo> addressInfos = new ArrayList<AddressInfo>();

	private Context mContext;

	public AddressAdapter(Context mContext) {
		this.mContext = mContext;
	}

	public void setData(List<AddressInfo> addressInfos) {
		if (addressInfos == null) {
			this.addressInfos.clear();
		} else {
			this.addressInfos = addressInfos;
		}
		notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return addressInfos.size();
	}

	@Override
	public AddressInfo getItem(int position) {
		// TODO Auto-generated method stub
		return addressInfos.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder holder;
		if (convertView == null) {
			holder = new ViewHolder();
			convertView = LayoutInflater.from(mContext).inflate(
					R.layout.adapter_addresslist, null);
			holder.name = (TextView) convertView
					.findViewById(R.id.adapter_address_name);
			holder.address = (TextView) convertView
					.findViewById(R.id.adapter_address_address);
			holder.icon = (TextView) convertView
					.findViewById(R.id.adapter_address_icon);
			holder.line = (TextView) convertView
					.findViewById(R.id.adapter_address_line);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		holder.address.setText(addressInfos.get(position).getProvince()
				+ addressInfos.get(position).getCity()
				+ addressInfos.get(position).getArea()
				+ addressInfos.get(position).getAddress());
		holder.name.setText(addressInfos.get(position).getUsername());
		holder.icon.setTypeface(App.getInstance().getCommnonTypeface());
		holder.icon.setText(Html.fromHtml("&#xe61a"));
		return convertView;
	}

	static class ViewHolder {
		TextView name;
		TextView address;
		TextView icon;
		TextView line;
	}

}
