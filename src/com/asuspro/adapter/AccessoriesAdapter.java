package com.asuspro.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.asuspro.R;
import com.asuspro.model.ProductInfo;

/**
 * 配件价格Adapter
 * larry create on 2015-7-20
 */
public class AccessoriesAdapter extends BaseAdapter {

	private List<ProductInfo> products = new ArrayList<ProductInfo>();
	private Context mContext;
	
	public AccessoriesAdapter (Context mContext) {
		this.mContext = mContext;
	}
	
	public void setData(List<ProductInfo> info) {
		if(info == null) {
			products.clear();
		} else {
			products = info;
		}
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return products.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder holder;
		if(convertView == null) {
			holder = new ViewHolder();
			convertView = LayoutInflater.from(mContext).inflate(R.layout.adapter_accessories, null);
			holder.typeView = (TextView) convertView.findViewById(R.id.access_type);
			holder.numView = (TextView) convertView.findViewById(R.id.access_num);
			holder.priceView = (TextView) convertView.findViewById(R.id.access_price);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		holder.typeView.setText(products.get(position).getType());
		holder.numView.setText(products.get(position).getNum() + "");
		holder.priceView.setText("￥" + products.get(position).getPrice());
		return convertView;
	}
	
	static class ViewHolder {
		TextView typeView, numView, priceView;
	}

}
