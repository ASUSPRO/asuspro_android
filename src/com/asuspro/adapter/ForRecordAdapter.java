package com.asuspro.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.asuspro.R;
import com.asuspro.model.ExchangeInfo;
import com.asuspro.tools.Tools;
import com.loopj.android.image.SmartImageView;

/**
 * 兑换记录适配器
 * larry create on 2015-5-30
 */
public class ForRecordAdapter extends BaseAdapter {
	
	private Context mContext;
	
	private List<ExchangeInfo> exchangeInfos = new ArrayList<ExchangeInfo>();
	
	public ForRecordAdapter(Context mContext) {
		this.mContext = mContext;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return exchangeInfos.size();
	}
	
	public void setData(List<ExchangeInfo> exchangeInfos) {
		if(exchangeInfos == null) {
			this.exchangeInfos.clear();
		} else {
			this.exchangeInfos = exchangeInfos;
		}
		notifyDataSetChanged();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return exchangeInfos.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder holder;
		if(convertView == null) {
			holder = new ViewHolder();
			convertView = LayoutInflater.from(mContext).inflate(R.layout.adapter_for_record, null);
			holder.img = (SmartImageView) convertView.findViewById(R.id.adapter_for_record_img);
			holder.name = (TextView) convertView.findViewById(R.id.adapter_for_record_name);
			holder.integral = (TextView) convertView.findViewById(R.id.adapter_for_record_integral);
			holder.number = (TextView) convertView.findViewById(R.id.adapter_for_record_number);
			holder.time = (TextView) convertView.findViewById(R.id.adapter_for_record_time);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		holder.name.setText(exchangeInfos.get(position).getGift_name());
		holder.integral.setText("-" + exchangeInfos.get(position).getScore() + "积分");
		holder.img.setImageUrl(exchangeInfos.get(position).getGift_pic());
		holder.number.setText("x" + exchangeInfos.get(position).getNum());
		holder.time.setText(Tools.convertTimeStamp(exchangeInfos.get(position).getCreate_time(), "yyyy-MM-dd"));
		return convertView;
	}

	static class ViewHolder {
		SmartImageView img;
		TextView name, integral, number, time;
	}
	
}
