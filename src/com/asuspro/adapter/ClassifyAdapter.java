package com.asuspro.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.asuspro.R;
import com.asuspro.model.CategoryBean;

/**
 * 分类Adapter
 * larry create on 2015-7-13
 */
public class ClassifyAdapter extends BaseAdapter {
	private List<CategoryBean> data = new ArrayList<CategoryBean>();
	private Context mContext;

	public ClassifyAdapter(Context mContext) {
		this.mContext = mContext;
	}
	
	public void setData(List<CategoryBean> data) {
		if(data != null) {
			this.data = data;
		} else {
			this.data.clear();
		}
		CategoryBean all = new CategoryBean();
		all.setColumn_id(0);
		all.setColumn_name("全部分类");
		this.data.add(0, all);
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return data.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder holder;
		if(convertView == null) {
			holder = new ViewHolder();
			convertView = LayoutInflater.from(mContext).inflate(R.layout.adapter_classify, null);
			holder.name = (TextView) convertView.findViewById(R.id.adapter_classify_name);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		holder.name.setText(data.get(position).getColumn_name());
		return convertView;
	}
	
	static class ViewHolder {
		TextView name;
	}

}
