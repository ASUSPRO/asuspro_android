package com.asuspro.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

import com.asuspro.R;
import com.asuspro.WebViewActivity;
import com.asuspro.base.App;
import com.asuspro.model.AdvertisementInfo;
import com.asuspro.tools.ListUtils;
import com.loopj.android.image.SmartImageView;
import com.loopj.android.image.WebImageCache;

/**
 * 
 * larry create on 2015-2-14
 */
public class ImagePagerAdapter extends RecyclingPagerAdapter {

    private Context       context;
    private List<AdvertisementInfo> imageIdList = new ArrayList<AdvertisementInfo>();

    private int           size = 0;
    private boolean       isInfiniteLoop;

    public ImagePagerAdapter(Context context) {
        this.context = context;
        isInfiniteLoop = false;
    }
    
    public void setData(List<AdvertisementInfo> imageIdList) {
    	if(imageIdList != null) {
    		this.imageIdList = imageIdList;
    	} else {
    		AdvertisementInfo info = new AdvertisementInfo();
    		this.imageIdList.add(info);
    	}
    	this.size = ListUtils.getSize(this.imageIdList);
    }

    @Override
    public int getCount() {
        // Infinite loop
        return isInfiniteLoop ? Integer.MAX_VALUE : ListUtils.getSize(imageIdList);
    }

    /**
     * get really position
     * 
     * @param position
     * @return
     */
    private int getPosition(int position) {
    	if(size == 0) {
    		return 0;
    	}
        return isInfiniteLoop ? position % size : position;
    }

    @Override
    public View getView(final int position, View view, ViewGroup container) {
        ViewHolder holder;
        if (view == null) {
            holder = new ViewHolder();
            view = holder.imageView = new SmartImageView(context);
            view.setTag(holder);
        } else {
            holder = (ViewHolder)view.getTag();
        }
        if(imageIdList != null && imageIdList.size() > 0) {
        	if(imageIdList.get(getPosition(position)) == null) {
        		holder.imageView.setBackgroundResource(R.drawable.icon_default_meeting_list);
        	} else {
//        		WebImageCache cache = new WebImageCache(context);
//        		Bitmap bitmap = cache.get(imageIdList.get(getPosition(position)).getImage().getOriginalURL());
//        		if(bitmap != null) {
//        			holder.imageView.setBackground(new BitmapDrawable(null, bitmap));
//        		}
        		holder.imageView.setImageUrl(imageIdList.get(getPosition(position)).getImage().getOriginalURL(), R.drawable.icon_default_meeting_list, true);
        		holder.imageView.setOnClickListener(new OnClickListener() {
        			@Override
        			public void onClick(View v) {
        				// TODO Auto-generated method stub
        				if(!TextUtils.isEmpty(imageIdList.get(getPosition(position)).getLink())) {
        					Intent intent=new Intent(context, WebViewActivity.class);
        					intent.putExtra("title", App.getInstance().getApplicationInfo().name);
        					intent.putExtra("url", imageIdList.get(getPosition(position)).getLink()); 
        					context.startActivity(intent);
        				}
        			}
        		});
        	}
        } else {
        	holder.imageView.setBackgroundResource(R.drawable.icon_default_meeting_list);
        }
        return view;
    }

    private static class ViewHolder {

        SmartImageView imageView;
    }

    /**
     * @return the isInfiniteLoop
     */
    public boolean isInfiniteLoop() {
        return isInfiniteLoop;
    }

    /**
     * @param isInfiniteLoop the isInfiniteLoop to set
     */
    public ImagePagerAdapter setInfiniteLoop(boolean isInfiniteLoop) {
        this.isInfiniteLoop = isInfiniteLoop;
        return this;
    }
}
