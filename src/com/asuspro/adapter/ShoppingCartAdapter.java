package com.asuspro.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.asuspro.R;
import com.asuspro.model.ExchangeInfo;

/**
 * 购物车适配器
 * larry create on 2015-7-3
 */
public class ShoppingCartAdapter extends BaseAdapter {
	
	private Context mContext;
	private ArrayList<ExchangeInfo> data = new ArrayList<ExchangeInfo>();
	
	public ShoppingCartAdapter (Context mContext) {
		this.mContext = mContext;
	}
	
	public void setData(ArrayList<ExchangeInfo> data) {
		this.data = data;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return 10;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder holder;
		if(convertView == null) {
			holder = new ViewHolder();
			convertView = LayoutInflater.from(mContext).inflate(R.layout.adapter_shopping_cart_layout, null);
			
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		return convertView;
	}
	
	static class ViewHolder {
		
	}

}
