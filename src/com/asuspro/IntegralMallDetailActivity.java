package com.asuspro;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.asuspro.base.App;
import com.asuspro.base.BaseActivity;
import com.asuspro.dbhelper.DBManager;
import com.asuspro.manager.HttpProxy;
import com.asuspro.manager.HttpProxy.AsyncResponseListener;
import com.asuspro.manager.JsonParser;
import com.asuspro.model.BaseResult;
import com.asuspro.model.ExchangeInfo;
import com.asuspro.model.GiftInfo;
import com.asuspro.model.UserInfo;
import com.asuspro.tools.ToastFactory;
import com.asuspro.tools.Tools;

/**
 * 商城商品详情
 * larry create on 2015-7-21
 */
public class IntegralMallDetailActivity extends BaseActivity {

	private WebView webview;
	private WebView webviewDeatil;
	private TextView inner_score; //所需积分
	private TextView detail; //详细规格
	private TextView footerIcon; // 我的积分图标
	private TextView mineScore; //我的积分
	private Button add, exchange; // 添加/兑换
	
	private GiftInfo info = null;
	private UserInfo userInfo;
	private int gift_id;
	
	@Override
	protected int getContentView() {
		// TODO Auto-generated method stub
		return R.layout.activity_integral_mall_detail;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		gift_id = getIntent().getIntExtra("id", 0);
		
		initView();
		setListener();
		initGiftData();
		
		getIntegralMallDetailData();
	}

	private void initView() {
		webview = (WebView) findViewById(R.id.integral_mall_webview);
		webviewDeatil = (WebView) findViewById(R.id.integral_mall_webview_detail);
		inner_score = (TextView) findViewById(R.id.integral_mall_subheader_title);
		detail = (TextView) findViewById(R.id.integral_mall_subheader_detail);
		footerIcon = (TextView) findViewById(R.id.integral_mall_footer_icon);
		mineScore = (TextView) findViewById(R.id.integral_mall_footer_total_money);
		add = (Button) findViewById(R.id.integral_mall_footer_add);
		exchange = (Button) findViewById(R.id.integral_mall_footer_exchange);
		
		webview.getSettings().setDefaultTextEncodingName("utf-8");
		webviewDeatil.getSettings().setDefaultTextEncodingName("utf-8");
		footerIcon.setTypeface(App.getInstance().getCommnonTypeface());
		footerIcon.setText(Html.fromHtml("&#xe608"));
	}
	
	private void initGiftData() {
		userInfo = DBManager.getUserDBHandler().getLoginUser();
		if(info != null) {
			try {
				byte[] b = Base64.decode(info.getGift_desc().getBytes(), Base64.DEFAULT);
				webview.loadData(new String(b), "text/html; charset=utf-8", "utf-8");
				byte[] bDetail = Base64.decode(info.getGift_note().getBytes(), Base64.DEFAULT);
				webviewDeatil.loadData(new String(bDetail), "text/html; charset=utf-8", "utf-8");
				inner_score.setText("所需积分： " + info.getInner_score());
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
		if(userInfo != null) {
			mineScore.setText(Html.fromHtml("" + userInfo.getScore()));
		}
		
	}
	
	private void setListener() {
		detail.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				webviewDeatil.setVisibility(View.VISIBLE);
			}
		});
		add.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				add.setTextColor(getResources().getColor(R.color.white));
				add.setBackgroundColor(getResources().getColor(R.color.header_bg));
				exchange.setTextColor(getResources().getColor(R.color.gray_666666));
				exchange.setBackgroundResource(R.drawable.bg_rectangle_white_alpha_shape_nor);
			}
		});
		exchange.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				exchange.setTextColor(getResources().getColor(R.color.white));
				exchange.setBackgroundColor(getResources().getColor(R.color.header_bg));
				add.setTextColor(getResources().getColor(R.color.gray_666666));
				add.setBackgroundResource(R.drawable.bg_rectangle_white_alpha_shape_nor);
				
				ExchangeInfo exchangeInfo = new ExchangeInfo();
				exchangeInfo.setCreate_time(System.currentTimeMillis());
				exchangeInfo.setGift_id(info.getId());
				exchangeInfo.setGift_name(info.getGift_name());
				exchangeInfo.setGift_pic(info.getGift_pic());
				exchangeInfo.setNum(1);
				if(userInfo.getUser_type() == 1) {//内部员工
					exchangeInfo.setScore(info.getInner_score());
				} else {
					exchangeInfo.setScore(info.getOuter_score());
				}
				
				Intent i = new Intent();
				i.setClass(IntegralMallDetailActivity.this, ExchangeGiftActivity.class);
				i.putExtra("exchange", exchangeInfo);
				startActivity(i);
			}
		});
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	protected String getHeaderBarTitle() {
		// TODO Auto-generated method stub
		return "商品详情";
	}

	@Override
	protected String getHeaderBarRightIcon() {
		// TODO Auto-generated method stub
		return "";
	}

	@Override
	protected boolean onClickLeftEvent() {
		// TODO Auto-generated method stub
		if(webviewDeatil.getVisibility() == View.VISIBLE) {
			webviewDeatil.setVisibility(View.GONE);
		} else {
			finish();
		}
		return super.onClickLeftEvent();
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		if(keyCode == KeyEvent.KEYCODE_BACK) {
			if(webviewDeatil.getVisibility() == View.VISIBLE) {
				webviewDeatil.setVisibility(View.GONE);
				return true;
			}
		}
		return super.onKeyDown(keyCode, event);
	}

	private void getIntegralMallDetailData() {
		showWaitDialog();
		HttpProxy.requestGiftDetail(gift_id, new AsyncResponseListener() {
			
			@Override
			public void onSuccess(String data) {
				// TODO Auto-generated method stub
				Log.d("larry", "---getIntegralMallDetailData----" + data);
				hideWaitDialog();
				BaseResult result = JsonParser.parserGifDetail(data);
				if(result.getResultCode() == 0) {
					try {
						info = (GiftInfo) result.getResultContent();
						initGiftData();
					} catch (Exception e) {
						e.printStackTrace();
					}
				} else {
					ToastFactory.showToast(IntegralMallDetailActivity.this, result.getResultMsg(), Toast.LENGTH_SHORT);
				}
			}
			
			@Override
			public void onFailure(String error) {
				// TODO Auto-generated method stub
				hideWaitDialog();
				ToastFactory.showToast(IntegralMallDetailActivity.this, error, Toast.LENGTH_SHORT);
			}
		});
	}

}
