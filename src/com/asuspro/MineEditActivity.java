package com.asuspro;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.asuspro.base.App;
import com.asuspro.base.BaseActivity;
import com.asuspro.dbhelper.DBManager;
import com.asuspro.manager.HttpProxy;
import com.asuspro.manager.HttpProxy.AsyncResponseListener;
import com.asuspro.manager.JsonParser;
import com.asuspro.model.BaseResult;
import com.asuspro.model.UserInfo;
import com.asuspro.tools.ToastFactory;
import com.asuspro.tools.Tools;

/**
 * 个人资料编辑页 larry create on 2015-5-24
 */
public class MineEditActivity extends BaseActivity {
	private RelativeLayout name_layout, birth_layout, mobile_layout, company_layout, tel_layout;
	private TextView accountsView, nameView, birthView, genderView, mobileView, companyView, telView;
	private TextView nameRight, birthRight, mobileRight, companyRight, telRight;

	private  UserInfo mCurrentUser;
	
	@Override
	protected int getContentView() {
		// TODO Auto-generated method stub
		return R.layout.activity_mine_edit;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		initView();
		setListener();
		initData();
	}

	private void initView() {
		name_layout = (RelativeLayout) findViewById(R.id.mine_edit_name_layout);
		birth_layout = (RelativeLayout) findViewById(R.id.mine_edit_birth_layout);
		mobile_layout = (RelativeLayout) findViewById(R.id.mine_edit_mobile_layout);
		company_layout = (RelativeLayout) findViewById(R.id.mine_edit_company_layout);
		tel_layout = (RelativeLayout) findViewById(R.id.mine_edit_tel_layout);
		accountsView = (TextView) findViewById(R.id.mine_edit_accounts);
		nameView = (TextView) findViewById(R.id.mine_edit_name);
		birthView = (TextView) findViewById(R.id.mine_edit_birth);
		genderView = (TextView) findViewById(R.id.mine_edit_gender);
		mobileView = (TextView) findViewById(R.id.mine_edit_mobile);
		companyView = (TextView) findViewById(R.id.mine_edit_company);
		telView = (TextView) findViewById(R.id.mine_edit_tel);
		nameRight = (TextView) findViewById(R.id.mine_edit_name_right);
		birthRight = (TextView) findViewById(R.id.mine_edit_birth_right);
		mobileRight = (TextView) findViewById(R.id.mine_edit_mobile_right);
		companyRight = (TextView) findViewById(R.id.mine_edit_company_right);
		telRight = (TextView) findViewById(R.id.mine_edit_tel_right);
		
		nameRight.setTypeface(App.getInstance().getCommnonTypeface());
		birthRight.setTypeface(App.getInstance().getCommnonTypeface());
		mobileRight.setTypeface(App.getInstance().getCommnonTypeface());
//		companyRight.setTypeface(App.getInstance().getCommnonTypeface());
//		telRight.setTypeface(App.getInstance().getCommnonTypeface());
		nameRight.setText(Html.fromHtml("&#xe60d"));
		birthRight.setText(Html.fromHtml("&#xe60d"));
		mobileRight.setText(Html.fromHtml("&#xe60d"));
//		companyRight.setText(Html.fromHtml("&#xe60d"));
//		telRight.setText(Html.fromHtml("&#xe60d"));
		
		headerRight.setTextSize(Tools.dipToPx(6));
	}
	
	private void setListener() {
		name_layout.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent();
				i.setClass(MineEditActivity.this, UserFieldEditActivity.class);
				i.putExtra("tag", "name");
				i.putExtra("content", nameView.getText().toString());
				startActivityForResult(i, 0);
			}
		});
		birth_layout.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
//				Intent i = new Intent();
//				i.setClass(MineEditActivity.this, UserFieldEditActivity.class);
//				i.putExtra("tag", "birth");
//				i.putExtra("content", birthView.getText().toString());
//				startActivityForResult(i, 0);
			}
		});
		mobile_layout.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent();
				i.setClass(MineEditActivity.this, UserFieldEditActivity.class);
				i.putExtra("tag", "mobile");
				i.putExtra("content", mobileView.getText().toString());
				startActivityForResult(i, 0);
			}
		});
		company_layout.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
			}
		});
		tel_layout.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
//				Intent i = new Intent();
//				i.setClass(MineEditActivity.this, UserFieldEditActivity.class);
//				i.putExtra("tag", "tel");
//				i.putExtra("content", telView.getText().toString());
//				startActivityForResult(i, 0);
			}
		});
	}
	
	private void initData() {
		mCurrentUser = DBManager.getUserDBHandler().getLoginUser();
		if(mCurrentUser != null) {
			accountsView.setText(mCurrentUser.getUsername());
			nameView.setText(mCurrentUser.getName());
			birthView.setText(mCurrentUser.getBirthday());
			if(mCurrentUser.getGender() == 1) {
				genderView.setText("女");
			} else {
				genderView.setText("男");
			}
			mobileView.setText(mCurrentUser.getMobile());
			companyView.setText(mCurrentUser.getCompany());
			telView.setText(mCurrentUser.getPhone());
		}
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	protected boolean onClickLeftEvent() {
		// TODO Auto-generated method stub
		finish();
		return super.onClickLeftEvent();
	}

	@Override
	protected boolean onClickRightEvent() {
		// TODO Auto-generated method stub
		requestUpdataUserInfo();
		return super.onClickRightEvent();
	}

	@Override
	protected String getHeaderBarTitle() {
		// TODO Auto-generated method stub
		return "个人资料";
	}

	@Override
	protected String getHeaderBarRightIcon() {
		// TODO Auto-generated method stub
		return "完成";
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		
		if(resultCode == Activity.RESULT_OK) {
			if(requestCode == 0) {
				String tag = data.getStringExtra("tag");
				String content = data.getStringExtra("content");
				if("name".equals(tag)) {
					nameView.setText(content);
//				} else if("birth".equals(tag)) {
//					birthView.setText(content);
				} else if("mobile".equals(tag)) {
					mobileView.setText(content);
//				} else if("tel".equals(tag)) {
//					telView.setText(content);
				}
			}
		}
	}

	private void requestUpdataUserInfo() {
		showWaitDialog();
		String name = "";
		String pay_password = "";
		String birthday = "";
		String mobile = "";
		String phone = "";
		try {
			name = nameView.getText().toString();
			pay_password = mCurrentUser.getPassword();
			birthday = birthView.getText().toString();
			mobile = mobileView.getText().toString();
			phone = telView.getText().toString();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
		HttpProxy.requestUpdateUserInfo(mCurrentUser.getId(), name, pay_password, birthday, mobile, phone, new AsyncResponseListener() {
			
			@Override
			public void onSuccess(String data) {
				// TODO Auto-generated method stub
				hideWaitDialog();
				BaseResult result = JsonParser.parserLogin(data);
				if(0 == result.getResultCode()) {
					UserInfo res = (UserInfo) result.getResultContent();
					mCurrentUser = res;
					mCurrentUser.setStatus(1);
					DBManager.getUserDBHandler().updateUserInfo(mCurrentUser);
					
					ToastFactory.showToast(MineEditActivity.this, "信息更新成功", Toast.LENGTH_SHORT);
					
				} else {
					ToastFactory.showToast(MineEditActivity.this, result.getResultMsg(), Toast.LENGTH_SHORT);
				}
			}
			
			@Override
			public void onFailure(String error) {
				// TODO Auto-generated method stub
				hideWaitDialog();
				ToastFactory.showToast(MineEditActivity.this, error, Toast.LENGTH_SHORT);
			}
		});
	}
	
}
