package com.asuspro.listener;

import android.view.View;
import android.view.ViewGroup;

/**
 * listview的子控件监听器
 * 
 * larry create on 2014-2-24
 */
public interface OnItemChildClickListener {
	/**
	 * Callback method to be invoked when an item in this AdapterView's child has been clicked.
	 * 
	 * @param convertView	The view within the AdapterView that was clicked (this will be a view provided by the adapter)
	 * @param parent	The AdapterView where the click happened.
	 * @param position	The position of the view in the adapter.
	 * @param id	The row id of the item's child that was clicked.
	 */
	void onClick(View convertView, ViewGroup parent, int groupPosition, int position, int id);
}
