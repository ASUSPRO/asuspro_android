package com.asuspro.tools;

import com.asuspro.R;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.text.TextUtils;
import android.widget.ImageView;
import android.widget.TextView;

public class ProgressDialogUtil extends Dialog {

	private ImageView animImageView;
	private TextView animTextView;
	private AnimationDrawable animationDrawable;

	public ProgressDialogUtil(Context context, String msg) {
		super(context, R.style.loading_dialog);
		
		setContentView(R.layout.layout_dialog_loading);

		animImageView = (ImageView) findViewById(R.id.dialogImage);
		
		animTextView = (TextView) findViewById(R.id.dialogTitle);

		animationDrawable = (AnimationDrawable) animImageView.getDrawable();
		
		if (!TextUtils.isEmpty(msg)) {
			animTextView.setText(msg);
		}

		this.setCanceledOnTouchOutside(true);
	}

	public void showDialog() {
		if (animationDrawable != null) {
			animationDrawable.start();
		}
		this.show();
	}

	public void hideDialog() {
		if (getContext() == null)
			return;
		if (animationDrawable != null) {
			animationDrawable.stop();
		}
		this.dismiss();
	}

}
