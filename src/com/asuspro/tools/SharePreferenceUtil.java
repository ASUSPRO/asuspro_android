package com.asuspro.tools;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class SharePreferenceUtil {
	/**
	 * SharedPreferences 数据存储
	 * 
	 * @author Administrator
	 * 
	 */
	private Context mContext;

	public SharePreferenceUtil(Context context) {
		this.mContext = context;
	}

	/**
	 * SharedPreferences 初始化
	 * 
	 * @return
	 */
	private SharedPreferences getSharedPreferences() {
		return mContext.getSharedPreferences("app_asus",
				Context.MODE_APPEND);
	}

	/**
	 * SharedPreferences 存储string类型数据
	 * 
	 * @param key
	 * @param value
	 */
	public void putString(String key, String value) {
		SharedPreferences sharedPreferences = getSharedPreferences();
		Editor editor = sharedPreferences.edit();
		editor.putString(key, value);
		editor.commit();
	}

	/**
	 * SharedPreferences 存储int类型数据
	 * 
	 * @param key
	 * @param value
	 */
	public void putInt(String key, int value) {
		SharedPreferences sharedPreferences = getSharedPreferences();
		Editor editor = sharedPreferences.edit();
		editor.putInt(key, value);
		editor.commit();
	}

	/**
	 * SharedPreferences 存储boolean类型数据
	 * 
	 * @param key
	 * @param value
	 */
	public void putBoolean(String key, Boolean value) {
		SharedPreferences sharedPreferences = getSharedPreferences();
		Editor editor = sharedPreferences.edit();
		editor.putBoolean(key, value);
		editor.commit();
	}
	
	/**
	 * SharedPreferences 存储long类型数据
	 * 
	 * @param key
	 * @param value
	 */
	public void putLong(String key, long value) {
		SharedPreferences sharedPreferences = getSharedPreferences();
		Editor editor = sharedPreferences.edit();
		editor.putLong(key, value);
		editor.commit();
	}

	/**
	 * SharedPreferences 读取string类型数据
	 * 
	 * @param key
	 * @return
	 */
	public String getString(String key) {
		SharedPreferences sharedPreferences = getSharedPreferences();
		return sharedPreferences.getString(key, "");
	}

	/**
	 * SharedPreferences 读取int类型数据
	 * 
	 * @param key
	 * @return
	 */
	public int getInt(String key) {
		SharedPreferences sharedPreferences = getSharedPreferences();
		return sharedPreferences.getInt(key, 0);
	}

	/**
	 * SharedPreferences 读取boolean类型数据
	 * 
	 * @param key
	 * @return
	 */
	public boolean getBoolean(String key) {
		SharedPreferences sharedPreferences = getSharedPreferences();
		return sharedPreferences.getBoolean(key, false);
	}
	
	/**
	 * SharedPreferences 读取long类型数据
	 * 
	 * @param key
	 * @return
	 */
	public long getLong(String key) {
		SharedPreferences sharedPreferences = getSharedPreferences();
		return sharedPreferences.getLong(key, 0);
	}

	/**
	 * 删除
	 * 
	 * @param key
	 */
	public void remove(String key) {
		SharedPreferences sharedPreferences = getSharedPreferences();
		Editor editor = sharedPreferences.edit();
		editor.remove(key);
		editor.commit();
	}

}
