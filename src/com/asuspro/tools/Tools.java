package com.asuspro.tools;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.os.Build;
import android.util.DisplayMetrics;
import android.util.Log;
import android.widget.Toast;

import com.asuspro.base.App;
import com.asuspro.config.Config;

/**
 * 通用工具类 larry create on 2014-11-17
 */
public class Tools {

	public static String getSDPath() {
		File sdDir = null;
		if (!FileTools.isExternalStorageWritable()) {
			return "/mnt/sdcard";
		} else {
			sdDir = android.os.Environment.getExternalStorageDirectory();
		}
		return sdDir.toString();
	}

	/**
	 * 检测网络是否连接
	 * 
	 * @return
	 */
	public static boolean checkNetworkState() {
		boolean flag = false;
		ConnectivityManager manager = (ConnectivityManager) App.getInstance()
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		// 去进行判断网络是否连接
		if (manager.getActiveNetworkInfo() != null) {
			flag = manager.getActiveNetworkInfo().isAvailable();
		}
		return flag;
	}

	/**
	 * 获取当前地理位置
	 * @param mContext
	 * @return
	 */
	public static Location getLocationPosition(Context mContext) {
		LocationManager lm = (LocationManager) mContext
				.getSystemService(Context.LOCATION_SERVICE);
		// 返回所有已知的位置提供者的名称列表，包括未获准访问或调用活动目前已停用的。
//		List<String> lp = lm.getAllProviders();
//		for (String item : lp) {
//			Log.i("8023", "可用位置服务：" + item);
//		}
		Criteria criteria = new Criteria();
//		criteria.setCostAllowed(false); // 设置位置服务免费
		criteria.setAccuracy(Criteria.ACCURACY_FINE);//高精度 
        criteria.setAltitudeRequired(false);//不要求海拔 
        criteria.setBearingRequired(false);//不要求方位 
        criteria.setCostAllowed(true);//允许有花费 
        criteria.setPowerRequirement(Criteria.POWER_LOW);//低功耗 

		criteria.setAccuracy(Criteria.ACCURACY_COARSE); // 设置水平位置精度
		// getBestProvider 只有允许访问调用活动的位置供应商将被返回
		String providerName = lm.getBestProvider(criteria, true);
//		Log.i("8023", "------位置服务：" + providerName);

		if (providerName != null) {
			Location location = lm.getLastKnownLocation(providerName);
//			Log.i("8023", "-------" + location);
			if(location != null) {
				// 获取维度信息
				double latitude = location.getLatitude();
				// 获取经度信息
				double longitude = location.getLongitude();
				Log.d("larry", "定位方式： " + providerName + "  维度：" + latitude
						+ "  经度：" + longitude);
				return location;
			} else {
				return null;
			}
		} else {
			Toast.makeText(mContext, "1.请检查网络连接 \n2.请打开我的位置", Toast.LENGTH_SHORT).show();
			return null;
		}
		
	}
	
	/**
	 * px转换成dip
	 * 
	 * @param pxz
	 * @return
	 */
	public static int pxToDip(float px) {
		return (int) (px / Config.DeviceInfo.density + 0.5f);
	}

	/**
	 * dip转换成px
	 * 
	 * @param dip
	 * @return
	 */
	public static int dipToPx(float dip) {
		return (int) (dip * Config.DeviceInfo.density + 0.5f);
	}

	public static void initDeviceInfo(Application application) {
		DisplayMetrics dm = application.getResources().getDisplayMetrics();
		Config.DeviceInfo.screenHeight = dm.heightPixels;
		Config.DeviceInfo.screenWidth = dm.widthPixels;
		Config.DeviceInfo.density = dm.density;
		Config.DeviceInfo.densityDpi = dm.densityDpi;
		Config.DeviceInfo.scaledDensity = dm.scaledDensity;
	}

	/**
	 * @param Unicode编码字符串
	 * @return 汉字字符串
	 */
	public static String convert(String unicodeString) {
		StringBuilder sb = new StringBuilder();
		int i = -1;
		int pos = 0;

		while ((i = unicodeString.indexOf("\\u", pos)) != -1) {
			sb.append(unicodeString.substring(pos, i));
			if (i + 5 < unicodeString.length()) {
				pos = i + 6;
				sb.append((char) Integer.parseInt(
						unicodeString.substring(i + 2, i + 6), 16));
			}
		}
		if (pos > 0 && pos < unicodeString.length()) {
			sb.append(unicodeString.substring(pos));
		}
		if (pos <= 0)
			sb.append(unicodeString);
		return sb.toString();
	}

	/**
	 * 判断手机号格式是否合法
	 * 
	 * @param mobiles
	 * @return
	 */
	public static boolean isMobileNO(String mobiles) {
		Pattern p = Pattern.compile("1[0-9]{10}");
		Matcher m = p.matcher(mobiles);
		return m.matches();
	}

	/**
	 * 隐藏手机号中间四位
	 * 
	 * @param account
	 * @return
	 */
	public static String getSafePhoneNumber(String phone) {
		if (phone != null && phone.length() > 7) {
			String temp = phone.substring(3, 7);
			return phone.replaceAll(temp, "****");
		}
		return phone;
	}

	@SuppressLint("SimpleDateFormat")
	public static String convertTimeStamp(long timestamp, String pattern) {
		long time;
		if (timestamp < 10000000000L) {
			time = timestamp * 1000;
		} else {
			time = timestamp;
		}
		SimpleDateFormat format = new SimpleDateFormat(pattern);
		String str = format.format(new Date(time));
		return str;
	}

	@SuppressLint("SimpleDateFormat")
	public static long conertToTimeStamp(String dateString) {
		if (dateString == null || dateString.equals("")) {
			return 0;
		}
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			Date date = sdf.parse(dateString);
			return date.getTime() > 0 ? date.getTime() : 0;
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return 0;
	}

	@SuppressLint("SimpleDateFormat")
	public static long conertToTimeStamp(String dateString, String pattern) {
		if (dateString == null || dateString.equals("")) {
			return 0;
		}
		SimpleDateFormat sdf = new SimpleDateFormat(pattern);
		try {
			Date date = sdf.parse(dateString);
			return date.getTime() > 0 ? date.getTime() : 0;
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return 0;
	}

	public static boolean hasFroyo() {
		return Build.VERSION.SDK_INT >= Build.VERSION_CODES.FROYO;
	}

	public static boolean hasGingerbread() {
		return Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD;
	}

	public static boolean hasHoneycomb() {
		return Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB;
	}

	public static boolean hasHoneycombMR1() {
		return Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR1;
	}

	public static boolean hasJellyBean() {
		return Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN;
	}

	public static boolean hasKitKat() {
		return Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;
	}
}
