package com.asuspro.tools;

import java.io.IOException;
import java.io.InputStream;

/**
 * 
 * larry create on 2015-8-6
 */
public class IOUtils {
	/**
	 * 将流转变成字符串
	 * 
	 * @param in
	 * @return
	 */
	public static String convertInputStreamToString(InputStream in) {
		StringBuffer out = new StringBuffer();
		byte[] b = new byte[4096];
		int n;
		try {
			while ((n = in.read(b)) != -1) {
				out.append(new String(b, 0, n));
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return out.toString();
	}
}
