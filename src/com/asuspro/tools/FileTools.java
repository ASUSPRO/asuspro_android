package com.asuspro.tools;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.text.TextUtils;

import com.asuspro.config.Config;

/**
 * 
 * larry create on 2014-11-17
 */
public class FileTools {
	/** 检查外部存储是否可读写 */
	public static boolean isExternalStorageWritable() {
		String state = Environment.getExternalStorageState();
		if (Environment.MEDIA_MOUNTED.equals(state)) {
			return true;
		}
		return false;
	}

	/** 检查外部存储是否可读 */
	public static boolean isExternalStorageReadable() {
		String state = Environment.getExternalStorageState();
		if (Environment.MEDIA_MOUNTED.equals(state)
				|| Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
			return true;
		}
		return false;
	}

	/**
	 * 判断文件是否存在
	 * 
	 * @return
	 */
	public static boolean fileIsExists(String filePath) {
		try {
			if (!isExternalStorageReadable()) {
				return false;
			}
			File f = new File(filePath);
			if (!f.exists()) {
				return false;
			}
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	/**
	 * 复制单个文件
	 * 
	 * @param oldPath
	 *            String 原文件路径 如：c:/fqf.txt
	 * @param newPath
	 *            String 复制后路径 如：f:/fqf.txt
	 * @return boolean
	 */
	public static boolean copyFile(String oldPath, String newPath) {
		boolean isok = true;
		try {
			int byteread = 0;
			File oldfile = new File(oldPath);
			File newfile = new File(newPath.substring(0,
					newPath.lastIndexOf("/")));
			if (!newfile.exists()) {
				newfile.mkdirs();
			}
			if (oldfile.exists()) { // 文件存在时
				InputStream inStream = new FileInputStream(oldPath); // 读入原文件
				FileOutputStream fs = new FileOutputStream(newPath);
				byte[] buffer = new byte[1024];
				// int length;
				while ((byteread = inStream.read(buffer)) != -1) {
					fs.write(buffer, 0, byteread);
				}
				fs.flush();
				fs.close();
				inStream.close();
			} else {
				isok = false;
			}
		} catch (Exception e) {
			// System.out.println("复制单个文件操作出错");
			e.printStackTrace();
			isok = false;
		}
		return isok;
	}

	/**
	 * 创建文件
	 * 
	 * @param pathName
	 */
	public static void createPath(String pathName) {
		String path = pathName.substring(0, pathName.lastIndexOf("/"));
		File file = new File(path);
		if (!file.exists() || !file.isDirectory()) {
			file.mkdirs();
		}
	}

	/**
	 * 文件重命名
	 * 
	 * @param oldPath
	 * @param newPath
	 */
	public static void changeFilePath(String oldPath, String newPath) {
		File from = new File(oldPath);
		File to = new File(newPath);
		from.renameTo(to);
		from.delete();
	}

	/**
	 * 删除文件
	 * 
	 * @param inputPath
	 */
	public static void deleteFile(String inputPath/* , String inputFile */) {
		deleteFile(new File(inputPath));
	}

	/**
	 * 删除文件
	 * 
	 * @param file
	 */
	public static void deleteFile(File file) {
		if (file == null)
			return;
		if (file.isFile()) {
			file.delete();
		}
	}

	/**
	 * 删除目录下所有文件
	 * 
	 * @param file
	 * @return
	 */
	public static void delteDirectory(File file) {
		if (file == null)
			return;
		if (file.isDirectory()) {
			File[] childFiles = file.listFiles();
			if (childFiles == null || childFiles.length == 0) {
				file.delete();
				return;
			}

			for (int i = 0; i < childFiles.length; i++) {
				deleteFile(childFiles[i]);
			}
			file.delete();
		}
	}

	/**
	 * 删除指定文件或目录
	 * 
	 * @param root
	 */
	public static void clearDirectory(File root) {
		if (!root.exists()) {
			return;
		}
		File files[] = root.listFiles();
		if (files != null) {
			for (File f : files) {
				if (f.isFile()) {
					f.delete();
				} else {
					clearDirectory(f);
				}
			}
			root.delete();
		}
	}
	
	/**
	 * 获取cache存放的路径
	 */
	public static String getCacheDir() {

		String path = Config.SDCARD_CACHE_PATH;
		return confirmDir(path);
	}
	
	private static String confirmDir(String dir) {
		File file = new File(dir);
		if (!file.exists()) {
			file.mkdirs();
		}
		return dir;
	}
	
	public static boolean getImageFromNetAndSave(String url, String savePath,
			String fileName) {
		try {
			byte[] bytes = getImage(url);
			Bitmap bitmap = BitmapFactory.decodeByteArray(bytes, 0,
					bytes.length);
			return saveFile(bitmap, fileName, savePath);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	/**
	 * Get image from newwork
	 * 
	 * @param path
	 *            The path of image
	 * @return byte[]
	 * @throws Exception
	 */
	public static byte[] getImage(String path) throws Exception {
		URL url = new URL(path);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setConnectTimeout(5 * 1000);
		conn.setRequestMethod("GET");
		InputStream inStream = conn.getInputStream();
		if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
			return readStream(inStream);
		}
		return null;
	}
	
	/**
	 * Get data from stream
	 * 
	 * @param inStream
	 * @return byte[]
	 * @throws Exception
	 */
	public static byte[] readStream(InputStream inStream) throws Exception {
		ByteArrayOutputStream outStream = new ByteArrayOutputStream();
		byte[] buffer = new byte[1024];
		int len = 0;
		while ((len = inStream.read(buffer)) != -1) {
			outStream.write(buffer, 0, len);
		}
		outStream.close();
		inStream.close();
		return outStream.toByteArray();
	}

	/**
	 * 保存文件
	 * 
	 * @param bm
	 * @param fileName
	 * @throws IOException
	 */
	public static boolean saveFile(Bitmap bm, String fileName, String savePath)
			throws IOException {
		try {
			File dirFile = new File(savePath);
			if (!dirFile.exists()) {
				dirFile.mkdirs();
			}
			File myCaptureFile = new File(savePath + "/" + fileName);
			if (!myCaptureFile.exists()) {
				myCaptureFile.createNewFile();
			}
			BufferedOutputStream bos = new BufferedOutputStream(
					new FileOutputStream(myCaptureFile));
			bm.compress(Bitmap.CompressFormat.PNG, 80, bos);
			bos.flush();
			bos.close();
		} catch (Exception e) {
			return false;
		}
		return true;
	}
	
	public static String getUserAvatarName(String avatarUrl) {
		
		if (TextUtils.isEmpty(avatarUrl)) {
			return "";
		}
		
		return avatarUrl.substring(avatarUrl.lastIndexOf("/") + 1,
				avatarUrl.length());
	}

	public static String getUserAvatarDir(String userId) {
		return Config.SDCARD_USER_PATH + userId + "/" + Config.userFolderNameAvatar;
	}
	
}
