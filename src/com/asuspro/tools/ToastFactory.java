package com.asuspro.tools;

import android.content.Context;
import android.widget.TextView;
import android.widget.Toast;

public class ToastFactory {

	private static String oldMsg;
	protected static Toast toast = null;
	protected static TextView tv = null;
	private static long oneTime = 0;
	private static long twoTime = 0;

	public static void showToast(Context context, String s, int duration) {

		if (s == null || context == null) {
			return;
		}

		if (toast == null) {
			toast = Toast.makeText(context, s, duration);
			toast.show();
			oneTime = System.currentTimeMillis();	
		} else {

			twoTime = System.currentTimeMillis();
			if (s.equals(oldMsg)) {
				if (twoTime - oneTime > 2000) { // Toast.LENGTH_SHORT 2000毫秒        Toast.LENGTH_LONG  3500毫秒
					toast.show();
				}
			} else {
				oldMsg = s;
				toast.setText(s);
				// tv.setText(s);
				toast.show();
			}
		}
		oneTime = twoTime;
	}

	/**
	 * @Title: showToastNoRepeat
	 * @Description: 此方法描述的是：Toast提示，不重复出现
	 * @param @param context
	 * @param @param resId
	 * @param @param duration 设定文件
	 * @return： void 返回类型
	 * @throws
	 * @author: @baiqunchao Baidu.Inc
	 * @version: 2014-1-14 下午6:18:24
	 */
	public static void showToastNoRepeat(Context context, int resId,
			int duration) {
		if (toast == null) {
			toast = Toast.makeText(context, resId, duration);
		} else {
			toast.setText(resId);
			toast.setDuration(duration);
		}
		toast.show();
	}

	public static void showToast(Context context, int resId, int duration) {
		showToast(context, context.getString(resId), duration);
	}

}
