package com.asuspro;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.Toast;

import com.asuspro.adapter.IntegralMallTwoAdapter;
import com.asuspro.base.BaseActivity;
import com.asuspro.manager.HttpProxy;
import com.asuspro.manager.HttpProxy.AsyncResponseListener;
import com.asuspro.manager.JsonParser;
import com.asuspro.model.BaseResult;
import com.asuspro.model.GiftInfo;
import com.asuspro.tools.ToastFactory;

/**
 * 商城商品详情2
 * larry create on 2015-7-21
 */
public class IntegralMallTwoActivity extends BaseActivity {

	private GridView gridView;
	private GiftInfo info = new GiftInfo();
	private int id; // -10 本月新品   -11 华硕产品
	
	private IntegralMallTwoAdapter mallTwoAdapter;
	private List<GiftInfo> gifts = new ArrayList<GiftInfo>();
	
	@Override
	protected int getContentView() {
		// TODO Auto-generated method stub
		return R.layout.activity_integral_mall_two;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		id = getIntent().getIntExtra("id", 0);
		
		initView();
		setListener();
		
		if(id == -10) {
			getNewGiftList();
		} else if(id == -11) {
			getAsusGiftList();
		} else {
			getGiftList();
		}
	}

	private void initView() {
		gridView = (GridView) findViewById(R.id.integral_mall_two_grid);
		
		mallTwoAdapter = new IntegralMallTwoAdapter(this);
		gridView.setAdapter(mallTwoAdapter);
	}
	
	private void initGiftData(List<GiftInfo> gifts) {
		if(gifts == null) {
			this.gifts.clear();
		}
		this.gifts = gifts;
		mallTwoAdapter.setData(gifts);
		mallTwoAdapter.notifyDataSetChanged();
	}
	
	private void setListener() {
		gridView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				Intent i = new Intent(IntegralMallTwoActivity.this, IntegralMallDetailActivity.class);
				i.putExtra("id", ((GiftInfo)mallTwoAdapter.getItem(position)).getId());
				startActivity(i);
			}
		});
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	protected String getHeaderBarTitle() {
		// TODO Auto-generated method stub
		return "商品详情";
	}

	@Override
	protected String getHeaderBarRightIcon() {
		// TODO Auto-generated method stub
		return "";
	}

	@Override
	protected boolean onClickLeftEvent() {
		// TODO Auto-generated method stub
		finish();
		return super.onClickLeftEvent();
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		if(keyCode == KeyEvent.KEYCODE_BACK) {
			
		}
		return super.onKeyDown(keyCode, event);
	}

	// 本月新品
	private void getNewGiftList() {
		showWaitDialog();
		HttpProxy.requestGetNewGiftList(new AsyncResponseListener() {
			
			@Override
			public void onSuccess(String data) {
				// TODO Auto-generated method stub
				hideWaitDialog();
				BaseResult result = JsonParser.parserGifList(data);
				if(result.getResultCode() == 0) {
					try {
						ArrayList<GiftInfo> info = (ArrayList<GiftInfo>) result.getResultContent();
						initGiftData(info);
					} catch (Exception e) {
						e.printStackTrace();
					}
				} else {
					ToastFactory.showToast(IntegralMallTwoActivity.this, result.getResultMsg(), Toast.LENGTH_SHORT);
				}
			}
			
			@Override
			public void onFailure(String error) {
				// TODO Auto-generated method stub
				hideWaitDialog();
				ToastFactory.showToast(IntegralMallTwoActivity.this, error, Toast.LENGTH_SHORT);
			}
		});
	}
	
	// 华硕产品
	private void getAsusGiftList() {
		showWaitDialog();
		HttpProxy.requestGetAsusGiftList(new AsyncResponseListener() {
			
			@Override
			public void onSuccess(String data) {
				// TODO Auto-generated method stub
				hideWaitDialog();
				BaseResult result = JsonParser.parserGifList(data);
				if(result.getResultCode() == 0) {
					try {
						ArrayList<GiftInfo> info = (ArrayList<GiftInfo>) result.getResultContent();
						initGiftData(info);
					} catch (Exception e) {
						e.printStackTrace();
					}
				} else {
					ToastFactory.showToast(IntegralMallTwoActivity.this, result.getResultMsg(), Toast.LENGTH_SHORT);
				}
			}
			
			@Override
			public void onFailure(String error) {
				// TODO Auto-generated method stub
				hideWaitDialog();
				ToastFactory.showToast(IntegralMallTwoActivity.this, error, Toast.LENGTH_SHORT);
			}
		});
	}
	
	// 数码产品
	private void getGiftList() {
		showWaitDialog();
		HttpProxy.requestGiftList(id, 0, new AsyncResponseListener() {
			
			@Override
			public void onSuccess(String data) {
				// TODO Auto-generated method stub
				hideWaitDialog();
				BaseResult result = JsonParser.parserGifList(data);
				if(result.getResultCode() == 0) {
					try {
						ArrayList<GiftInfo> info = (ArrayList<GiftInfo>) result.getResultContent();
						initGiftData(info);
					} catch (Exception e) {
						e.printStackTrace();
					}
				} else {
					ToastFactory.showToast(IntegralMallTwoActivity.this, result.getResultMsg(), Toast.LENGTH_SHORT);
				}
			}
			
			@Override
			public void onFailure(String error) {
				// TODO Auto-generated method stub
				hideWaitDialog();
				ToastFactory.showToast(IntegralMallTwoActivity.this, error, Toast.LENGTH_SHORT);
			}
		});
	}
	
	

}
