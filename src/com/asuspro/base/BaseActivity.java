package com.asuspro.base;

import android.app.Activity;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.asuspro.R;
import com.asuspro.tools.ProgressDialogUtil;

/**
 * UI框架基类
 * 
 * larry create on 2015-5-2
 */
public abstract class BaseActivity extends Activity {
	protected TextView headerTitle;
	private TextView headerLeft;
	protected TextView headerRight;
	
	public ProgressDialogUtil mPd;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(getContentView());
		initializedHeader();
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	abstract protected int getContentView();

	private void initializedHeader() {
		headerTitle = (TextView) findViewById(R.id.header_title);
		headerLeft = (TextView) findViewById(R.id.header_left);
		headerRight = (TextView) findViewById(R.id.header_right);
		if (headerTitle != null) {
			headerTitle.setTypeface(App.getInstance().getCommnonTypeface());
			headerTitle.setText(Html.fromHtml(getHeaderBarTitle()));
			headerTitle.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					onClickTitleEvent();
				}
			});
		}
		if (headerLeft != null) {
			headerLeft.setTypeface(App.getInstance().getCommnonTypeface());
			headerLeft.setText(Html.fromHtml(getHeaderBarLeftIcon()));
			headerLeft.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					onClickLeftEvent();
				}
			});
		}
		if (headerRight != null) {
			headerRight.setTypeface(App.getInstance().getCommnonTypeface());
			headerRight.setText(Html.fromHtml(getHeaderBarRightIcon()));
			headerRight.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					onClickRightEvent();
				}
			});
		}
	}

	protected String getHeaderBarTitle() {
		return "";
	}
	
	protected String getHeaderBarLeftIcon() {
		return "&#xe61b";
	}

	protected String getHeaderBarRightIcon() {
		return "";
	}

	protected boolean onClickLeftEvent() {
		return false;
	}

	protected boolean onClickRightEvent() {
		return false;
	}
	
	protected boolean onClickTitleEvent() {
		return false;
	}
	
	
	public void showWaitDialog() {
		if (mPd == null) {
			mPd = new ProgressDialogUtil(this, "加载中");
		}
		if (!mPd.isShowing()) {
			mPd.showDialog();
		}

	}

	public void showWaitDialog(String title) {
		if (mPd == null) {
			mPd = new ProgressDialogUtil(this, title);
		}
		if (!mPd.isShowing()) {
			mPd.showDialog();
		}

	}

	public void showWaitDialog(boolean isCanceled) {

		if (mPd == null) {
			mPd = new ProgressDialogUtil(this, "加载中");
			mPd.setCanceledOnTouchOutside(isCanceled);
		}
		if (!mPd.isShowing()) {
			mPd.showDialog();
		}

	}

	public void hideWaitDialog() {

		if (mPd != null && mPd.isShowing()) {
			mPd.hideDialog();
		}
	}
}
