package com.asuspro.base;

import android.app.Application;
import android.content.Context;
import android.graphics.Typeface;

import com.asuspro.config.Config;
import com.asuspro.tools.SharePreferenceUtil;
import com.asuspro.tools.Tools;

/**
 * 应用基类 larry create on 2015-5-3
 */
public class App extends Application {
	
	private static App instance;
	private static SharePreferenceUtil preferences;
	private Typeface mCommonFace;
	
	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();
		instance = this;
		Tools.initDeviceInfo(this);
		setPreferences(new SharePreferenceUtil(getApplicationContext()));
	}

	public static App getInstance() {
		return instance;
	}

	public static Context getAppContext() {
		return getInstance().getApplicationContext();
	}

	/**
	 * 通用字体类型
	 * 
	 * @return
	 */
	public Typeface getCommnonTypeface() {
		if (mCommonFace == null) {
			mCommonFace = Typeface.createFromAsset(getAssets(),
					Config.TYPEFACE_COMMON_PATH);
		}
		return mCommonFace;
	}
	
	public static SharePreferenceUtil getPreferences() {
		return preferences;
	}

	public static void setPreferences(SharePreferenceUtil preferences) {
		App.preferences = preferences;
	}
	
}
