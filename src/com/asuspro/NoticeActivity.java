package com.asuspro;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.asuspro.adapter.ClassifyAdapter;
import com.asuspro.adapter.NoticeAdapter;
import com.asuspro.base.App;
import com.asuspro.base.BaseActivity;
import com.asuspro.manager.HttpProxy;
import com.asuspro.manager.HttpProxy.AsyncResponseListener;
import com.asuspro.manager.JsonParser;
import com.asuspro.model.AdvertisementInfo;
import com.asuspro.model.BaseResult;
import com.asuspro.model.CategoryBean;
import com.asuspro.model.NoticeInfo;
import com.asuspro.tools.ToastFactory;

/**
 * 政策通知 larry create on 2015-5-9
 */
public class NoticeActivity extends BaseActivity {
	private TextView notice_title;
	private ListView mListView;
	private RelativeLayout notice_classify_bg;
	private GridView notice_grid;
	
	private List<CategoryBean> classifys = new ArrayList<CategoryBean>();
	private List<NoticeInfo> notices = new ArrayList<NoticeInfo>();
	
	private NoticeAdapter mAdapter;
	private ClassifyAdapter classifyAdapter;
	
	private int pcid = 108;// 保持不变的，接受的上一级的数据
	private int cid = 108;

	@Override
	protected int getContentView() {
		// TODO Auto-generated method stub
		return R.layout.activity_notice;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		pcid = getIntent().getIntExtra("cid", 108);
		cid = pcid;
		
		initView();
		
		setListener();
		
//		initData();
		getColumnListDataFromNet();
		
		getArticleListDataFromNet();
	}
	
	private void initView() {
		notice_title = (TextView) findViewById(R.id.notice_title);
		mListView = (ListView) findViewById(R.id.mListView);
		notice_classify_bg = (RelativeLayout) findViewById(R.id.notice_classify_bg);
		notice_grid = (GridView) findViewById(R.id.notice_grid);
		
		notice_title.setTypeface(App.getInstance().getCommnonTypeface());
		notice_title.setText(Html.fromHtml("全部分类 &#xe602"));
		
		if(pcid == 109) {
			headerTitle.setText("技术分享");
		} else {
			headerTitle.setText("政策通知");
		}
		
		classifyAdapter = new ClassifyAdapter(this);
		classifyAdapter.setData(classifys);
		notice_grid.setAdapter(classifyAdapter);
		
		mAdapter = new NoticeAdapter(this);
		mAdapter.setData(notices);
		mListView.setAdapter(mAdapter);
	}
	
	private void initColumnListData(List<CategoryBean> classifys) {
		this.classifys = classifys;
		classifyAdapter.setData(classifys);
		classifyAdapter.notifyDataSetChanged();
	}
	
	private void initArticleListData(List<NoticeInfo> notices) {
		this.notices = notices;
		mAdapter.setData(notices);
		mAdapter.notifyDataSetChanged();
	}

	private void setListener() {
		notice_title.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(notice_classify_bg.getVisibility() == View.VISIBLE) {
					notice_classify_bg.setVisibility(View.GONE);
					notice_title.setText(Html.fromHtml("全部分类 &#xe602"));
				} else {
					notice_classify_bg.setVisibility(View.VISIBLE);
					notice_title.setText(Html.fromHtml("全部分类 &#xe61c"));
				}
			}
		});
		
		notice_classify_bg.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				notice_classify_bg.setVisibility(View.GONE);
				notice_title.setText(Html.fromHtml("全部分类 &#xe602"));
			}
		});
		notice_grid.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				notice_classify_bg.setVisibility(View.GONE);
				if(position == 0) {
					notice_title.setText(Html.fromHtml("全部分类 &#xe602"));
					cid = pcid;
				} else {
					notice_title.setText(Html.fromHtml(classifys.get(position).getColumn_name() + " &#xe602"));
					cid = (int) classifys.get(position).getColumn_id();
				}
				getArticleListDataFromNet();
			}
		});
		mListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				Intent i = new Intent();
				i.setClass(NoticeActivity.this, NoticeDetailActivity.class);
				i.putExtra("id", notices.get(position).getId());
				i.putExtra("cid", pcid);
				startActivity(i);
			}
		});
	}
	
	//获取分类名目
	private void getColumnListDataFromNet() {
		HttpProxy.requestGetColumnList(pcid, new AsyncResponseListener() {
			
			@Override
			public void onSuccess(String data) {
				// TODO Auto-generated method stub
				Log.d("larry", "--getColumnListDataFromNet--" + data);
				BaseResult result = JsonParser.parserCategory(data);
				if(result.getResultCode() == 0) {
					try {
						List<CategoryBean> classifys = (List<CategoryBean>) result.getResultContent();
						initColumnListData(classifys);
//						image.setImageUrl(info.getImage().getOriginalURL(), null , true);
					} catch (Exception e) {
						e.printStackTrace();
					}
				} else {
					ToastFactory.showToast(NoticeActivity.this, result.getResultMsg(), Toast.LENGTH_SHORT);
				}
			}
			
			@Override
			public void onFailure(String error) {
				// TODO Auto-generated method stub
				ToastFactory.showToast(NoticeActivity.this, error, Toast.LENGTH_SHORT);
			}
		});
	}

	// 获取列表数据
	private void getArticleListDataFromNet() {
		showWaitDialog();
		HttpProxy.requestArticleList(cid, 0, new AsyncResponseListener() {
			
			@Override
			public void onSuccess(String data) {
				// TODO Auto-generated method stub
				Log.d("larry", "--getArticleListDataFromNet--" + data);
				hideWaitDialog();
				BaseResult result = JsonParser.parserNotice(data);
				if(result.getResultCode() == 0) {
					try {
						List<NoticeInfo> notices = (List<NoticeInfo>) result.getResultContent();
						initArticleListData(notices);
					} catch (Exception e) {
						notices.clear();
						initArticleListData(notices);
						e.printStackTrace();
					}
				} else {
					ToastFactory.showToast(NoticeActivity.this, result.getResultMsg(), Toast.LENGTH_SHORT);
				}
			}
			
			@Override
			public void onFailure(String error) {
				// TODO Auto-generated method stub
				hideWaitDialog();
				ToastFactory.showToast(NoticeActivity.this, error, Toast.LENGTH_SHORT);
			}
		});
		
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	protected boolean onClickLeftEvent() {
		// TODO Auto-generated method stub
		finish();
		return super.onClickLeftEvent();
	}

	@Override
	protected String getHeaderBarRightIcon() {
		// TODO Auto-generated method stub
		return "";
	}

	@Override
	protected String getHeaderBarTitle() {
		// TODO Auto-generated method stub
		return "政策通知";
	}

}
